﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void UnityEngine.WebGLInput::set_captureAllKeyboardInput(System.Boolean)
extern void WebGLInput_set_captureAllKeyboardInput_m3F7C251D98F104095110F83C3EAE25D75DC19802 (void);
static Il2CppMethodPointer s_methodPointers[1] = 
{
	WebGLInput_set_captureAllKeyboardInput_m3F7C251D98F104095110F83C3EAE25D75DC19802,
};
static const int32_t s_InvokerIndices[1] = 
{
	2347,
};
extern const CustomAttributesCacheGenerator g_UnityEngine_WebGLModule_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_WebGLModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_WebGLModule_CodeGenModule = 
{
	"UnityEngine.WebGLModule.dll",
	1,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_UnityEngine_WebGLModule_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
