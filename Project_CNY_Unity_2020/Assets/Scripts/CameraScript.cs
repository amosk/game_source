﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    private int ScreenWidth;
    private int ScreenHeight;
    private Vector2 resTarget;
    private Vector2 resViewport;
    private Vector2 resNormalized;
    private Vector2 size;
    private bool bIsMobile;

    private void Awake()
    {
    }

    // Update is called once per frame
    void Update()
    {
        resTarget = new Vector2(1080f, 1920f);
        resViewport = new Vector2(Screen.width, Screen.height);
        resNormalized = resTarget / resViewport; // target res in viewport space
        size = resNormalized / Mathf.Max(resNormalized.x, resNormalized.y);
        Camera.main.rect = new Rect(default, size) { center = new Vector2(0.5f, 0.5f) };
    }
}
