﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleScript : MonoBehaviour
{
    public float EnvelopeSpecial_SpawnPercent = 100;

    [System.Serializable]
    public struct ObstaclesPrefab
    {
        public GameManagerScript.ELevels Level;
        public GameObject[] Obstacles;
    }

    public ObstaclesPrefab[] TopObstaclesPrefab;
    public ObstaclesPrefab[] BottomObstaclesPrefab;
    public GameObject Envelope_Prefab;
    public GameObject EnvelopeSpecial_Prefab;
    private GameObject TopObstaclePrefab;
    private GameObject BottomObstaclePrefab;
    public GameObject GatePrefab;

    public GameObject TopObject;
    public GameObject BottomObject;
    public GameObject Envelope;

    public float BottomStaticObstacleOffset = 0.0f;
    public float TopStaticObstacleMinOffset = 0.0f;
    public float TopStaticObstacleMaxOffset = 0.0f;
    public float GateOffset = -0.05f;

    private Vector3 TopObject_Position;
    private Vector3 BottomObject_Position;
    private Vector3 Envelope_Position;

    GameObject Player;
    PlayerBehavior playerBehaviorScript;

    ObstacleSpawner obstacleSpawnerScript;

    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.Find("Player");
        playerBehaviorScript = Player.GetComponent<PlayerBehavior>();

        GameObject ObstacleSpawner = GameObject.Find("ObstacleSpawner");
        obstacleSpawnerScript = ObstacleSpawner.GetComponent<ObstacleSpawner>();

        InitializeObstacle();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        UpdateMovement();
    }

    private void UpdateMovement()
    {
        bool bIsFlying = playerBehaviorScript.GetCurrentJumpState() == PlayerBehavior.EJumpState.Fly;

        if(playerBehaviorScript.GetGameStarted() && playerBehaviorScript.GetPlayerIsAlive())
        {
            transform.position -= new Vector3(bIsFlying ? playerBehaviorScript.FlyingMovementSpeed : playerBehaviorScript.MovementSpeed, 
                0.0f, 0.0f);
        }   
    }

    public void ReconstructObstacle()
    {
        InitializeObstacle();
    }

    private void InitializeObstacle()
    {
        bool bIsGate = obstacleSpawnerScript.SpawnedObstacle_Amounts == 999;

        if (TopObject)
        {
            Destroy(TopObject);
        }

        TopObject_Position = transform.Find("Top_Object_Position").transform.position;

        if (!bIsGate)
        {
            GameObject[] TopObstacles = TopObstaclesPrefab[(int)GameManagerScript.Instance.CurrentLevel].Obstacles;
            TopObstaclePrefab = TopObstacles[Random.Range(0, TopObstacles.Length - 1)];

            TopObject = Instantiate(TopObstaclePrefab, TopObject_Position, Quaternion.identity);
        }

        TopObject.transform.parent = transform.Find("Top_Object_Position");

        if (BottomObject)
        {
            Destroy(BottomObject);
        }

        if (!bIsGate)
        {
            GameObject[] BottomObstacles = BottomObstaclesPrefab[(int)GameManagerScript.Instance.CurrentLevel].Obstacles;
            BottomObstaclePrefab = BottomObstacles[Random.Range(0, BottomObstacles.Length - 1)];
        }

        BottomObject_Position = transform.Find("Bottom_Object_Position").transform.position;
        BottomObject = Instantiate(bIsGate ? GatePrefab : BottomObstaclePrefab, BottomObject_Position, bIsGate ? Quaternion.identity : Quaternion.Euler(new Vector3 (0.0f, 0.0f, 180.0f)));
        BottomObject.transform.parent = transform.Find("Bottom_Object_Position").transform;

        if (Envelope)
        {
            Destroy(Envelope);
        }
 
        Envelope_Position = transform.Find("Envelope_Position").transform.position;
        float EnvelopeRandomValue = Random.Range(0.0f, 100.0f);
        Envelope = Instantiate(EnvelopeRandomValue < EnvelopeSpecial_SpawnPercent ? EnvelopeSpecial_Prefab : Envelope_Prefab, Envelope_Position, Quaternion.identity);
        Envelope.transform.parent = this.transform;

        if (bIsGate)
        {
            BottomObject.transform.rotation = Quaternion.identity;
            BottomObject.transform.position = new Vector3(transform.position.x, BottomStaticObstacleOffset + GateOffset +
                (GatePrefab.gameObject.GetComponent<SpriteRenderer>().bounds.size.y * 0.5f), transform.position.z);
        }
        else if (BottomObstaclePrefab != null)
        {
            if (BottomObstaclePrefab.gameObject.name.Contains("Coral") || BottomObstaclePrefab.gameObject.name.Contains("Seagrass"))
            {
                BottomObject.transform.rotation = Quaternion.identity;
                BottomObject.transform.position = new Vector3(BottomObject.transform.position.x, BottomStaticObstacleOffset +
                    (BottomObstaclePrefab.gameObject.GetComponent<SpriteRenderer>().bounds.size.y * 0.5f), BottomObject.transform.position.z);

                TopObject.transform.position = new Vector3(TopObject.transform.position.x, 
                    Mathf.Clamp(TopObject.transform.position.y, TopStaticObstacleMinOffset, TopStaticObstacleMaxOffset),
                    TopObject.transform.position.z);
            }
            else if (BottomObstaclePrefab.gameObject.name.Contains("Cliff"))
            {
                BottomObject.transform.rotation = Quaternion.identity;

                if (TopObstaclePrefab.gameObject.name.Contains("Cliff"))
                {
                    TopObject.transform.rotation = Quaternion.Euler(new Vector3(0.0f, 0.0f, 180.0f));
                }
            }
            else if (TopObstaclePrefab.gameObject.name.Contains("Cliff"))
            {
                TopObject.transform.rotation = Quaternion.Euler(new Vector3(0.0f, 0.0f, 180.0f));

                if (BottomObstaclePrefab.gameObject.name.Contains("Cliff"))
                {
                    BottomObject.transform.rotation = Quaternion.identity;
                }
            }
        }
    }
}
