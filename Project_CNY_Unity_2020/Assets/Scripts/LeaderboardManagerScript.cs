﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.IO;
using System;
using System.Text;

[System.Serializable]
public class ScoreData
{
    public int USER_ID;
    public int SCORE;
    public int SCORE_SPECIAL;
    public string DURATION;
}


[System.Serializable]
public class LeaderboardData
{
    public int USER_ID;
    public string FIRST_NAME;
    public string LAST_NAME;
    public string EMAIL;
    public string MOBILE;
    public int SCORE;
    public int SCORE_SPECIAL;
    public string DURATION;
    public int RANK;

    public LeaderboardData()
    {
        USER_ID = 0;
        FIRST_NAME = "";
        LAST_NAME = "";
        EMAIL = "";
        MOBILE = "";
        SCORE = 0;
        SCORE_SPECIAL = 0;
        DURATION = "";
        RANK = 0;
    }
}

public class LeaderboardManagerScript : MonoBehaviour, IGameLoop
{
    private string LeaderboardURL = "https://api.eminent.st/app/leaderboard/";

    public List<LeaderboardData> LeaderboardInfos;

    public LeaderboardData CurrentUserLeaderboardInfo;

    public Canvas LeaderboardUI;

    [SerializeField] private ScoreData _scoreData = new ScoreData();

    private static LeaderboardManagerScript _instance;

    public static LeaderboardManagerScript Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    private void Start()
    {
        LeaderboardInfos = new List<LeaderboardData>();
        StartCoroutine(GetLeaderboardScore());
    }

    public void StartGame()
    {
        if (GameManagerScript.Instance.UserID != -1)
        {
            StartCoroutine(GetUserScore(GameManagerScript.Instance.UserID));
        }
    }

    public void UpdateUserScoreToLeaderboard()
    {
        _scoreData.USER_ID = GameManagerScript.Instance.UserID;
        _scoreData.SCORE = ScoreManagerScript.Instance.CurrentScore;
        _scoreData.SCORE_SPECIAL = ScoreManagerScript.Instance.CurrentScore_Special;
        TimeSpan timeSpan = TimeSpan.FromSeconds(ScoreManagerScript.Instance.CurrentDuration);
        _scoreData.DURATION = string.Format("{0:D2}:{1:D2}:{2:D2}", timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds);

        string score = JsonUtility.ToJson(_scoreData);

        StartCoroutine(PostUserScore(GameManagerScript.Instance.UserID, score));
    }

    IEnumerator GetUserScore(int userIndex)
    {
        // Get User Score Info
        string scoreURL = LeaderboardURL + "userHighScore/" + GameManagerScript.Instance.UserID.ToString();

        UnityWebRequest userScoreRequest = UnityWebRequest.Get(scoreURL);

        yield return userScoreRequest.SendWebRequest();

        if (userScoreRequest.result == UnityWebRequest.Result.ConnectionError || userScoreRequest.result == UnityWebRequest.Result.ProtocolError)
        {
            Debug.LogError(userScoreRequest.error);
            yield break;
        }

        SimpleJSON.JSONNode userLeaderboardInfo = SimpleJSON.JSON.Parse(userScoreRequest.downloadHandler.text);
        SimpleJSON.JSONArray userLeaderboardArray = userLeaderboardInfo["RESULTS"].AsArray;

        CurrentUserLeaderboardInfo.USER_ID = userLeaderboardArray[0]["USER_ID"];
        CurrentUserLeaderboardInfo.FIRST_NAME = userLeaderboardArray[0]["FIRST_NAME"];
        CurrentUserLeaderboardInfo.LAST_NAME = userLeaderboardArray[0]["LAST_NAME"];
        CurrentUserLeaderboardInfo.EMAIL = userLeaderboardArray[0]["EMAIL"];
        CurrentUserLeaderboardInfo.MOBILE = userLeaderboardArray[0]["MOBILE"];
        CurrentUserLeaderboardInfo.SCORE = userLeaderboardArray[0]["SCORE"];
        CurrentUserLeaderboardInfo.SCORE_SPECIAL = userLeaderboardArray[0]["SCORE_SPECIAL"];
        CurrentUserLeaderboardInfo.DURATION = userLeaderboardArray[0]["DURATION"];
        CurrentUserLeaderboardInfo.RANK = userLeaderboardArray[0]["RANK"];
    }

    IEnumerator PostUserScore(int userIndex, string Context)
    {
        string scoreURL = LeaderboardURL + "newRecord";

        UnityWebRequest updateScoreRequest = HTTPRequestHelper.Instance.GeneratePostCallRequest(scoreURL, Context);

        yield return updateScoreRequest.SendWebRequest();

        if (updateScoreRequest.error != null)
        {
            Debug.LogError(updateScoreRequest.error);
            yield break;
        }
        else
        {
            StartCoroutine(GetUserScore(GameManagerScript.Instance.UserID));
            StartCoroutine(RefreshLeaderboard(0.1f));
        }
    }

    IEnumerator RefreshLeaderboard(float WaitTime)
    {
        yield return new WaitForSeconds(WaitTime);

        StartCoroutine(GetLeaderboardScore());
    }
    IEnumerator GetLeaderboardScore()
    {
        // Get Global Leaderboard Info
        LeaderboardInfos.Clear();

        string scoreURL = LeaderboardURL + "globalHighScore";

        UnityWebRequest leaderboardRequest = UnityWebRequest.Get(scoreURL);

        yield return leaderboardRequest.SendWebRequest();

        if (leaderboardRequest.result == UnityWebRequest.Result.ConnectionError || leaderboardRequest.result == UnityWebRequest.Result.ProtocolError)
        {
            Debug.LogError(leaderboardRequest.error);
            yield break;
        }

        SimpleJSON.JSONNode leaderboardInfo = SimpleJSON.JSON.Parse(leaderboardRequest.downloadHandler.text);
        SimpleJSON.JSONArray leaderboardArray = leaderboardInfo["RESULTS"].AsArray;

        for(int i = 0; i <= leaderboardArray.Count - 1; i++)
        {
            LeaderboardData TempLeaderboardData = new LeaderboardData();
            TempLeaderboardData.USER_ID = leaderboardArray[i]["USER_ID"];
            TempLeaderboardData.FIRST_NAME = leaderboardArray[i]["FIRST_NAME"];
            TempLeaderboardData.LAST_NAME = leaderboardArray[i]["LAST_NAME"];
            TempLeaderboardData.EMAIL = leaderboardArray[i]["EMAIL"];
            TempLeaderboardData.MOBILE = leaderboardArray[i]["MOBILE"];
            TempLeaderboardData.SCORE = leaderboardArray[i]["SCORE"];
            TempLeaderboardData.SCORE_SPECIAL = leaderboardArray[i]["SCORE_SPECIAL"];
            TempLeaderboardData.DURATION = leaderboardArray[i]["DURATION"];
            TempLeaderboardData.RANK = leaderboardArray[i]["RANK"];

            LeaderboardInfos.Add(TempLeaderboardData);
        }

        LeaderboardUI.GetComponent<LeaderboardDisplayScript>().GenerateLeaderboard();
    }
}
