﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundaryScript : MonoBehaviour
{
    GameObject Player;
    PlayerBehavior playerBehaviorScript;

    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.Find("Player");
        playerBehaviorScript = Player.GetComponent<PlayerBehavior>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        playerBehaviorScript.bInBoundary = false;
        Debug.Log("Entered Boundary!");
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        playerBehaviorScript.bInBoundary = true;
        Debug.Log("Exit Boundary!");
    }
}
