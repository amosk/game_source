﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup_SpecialScript : MonoBehaviour
{
    GameObject Player;
    PlayerBehavior PlayerBehaviorScript;
    public AudioClip PickupSFX;

    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.Find("Player");
        PlayerBehaviorScript = Player.GetComponent<PlayerBehavior>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Destroy(gameObject);
        PlayerBehaviorScript.ExecuteFlyAction();
        PlayerBehaviorScript.PlayPickUpAudio(PickupSFX);
        ScoreManagerScript.Instance.AddScore(true);
    }
}
