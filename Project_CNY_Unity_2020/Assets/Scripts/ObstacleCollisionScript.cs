﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleCollisionScript : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<PlayerBehavior>())
        {
            if (other.gameObject.GetComponent<PlayerBehavior>().GetCurrentJumpState() != PlayerBehavior.EJumpState.Fly)
            {
                other.gameObject.GetComponent<PlayerBehavior>().PerformDeath();
            }
        }
    }
}
