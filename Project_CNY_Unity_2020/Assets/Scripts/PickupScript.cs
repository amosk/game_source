﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupScript : MonoBehaviour
{
    private BoxCollider Collider;
    GameObject Player;
    PlayerBehavior PlayerBehaviorScript;
    public AudioClip PickupSFX;

    // Start is called before the first frame update
    void Start()
    {
        Collider = GetComponent<BoxCollider>();
        Player = GameObject.Find("Player");
        PlayerBehaviorScript = Player.GetComponent<PlayerBehavior>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<PlayerBehavior>())
        {
            Destroy(gameObject);

            PlayerBehaviorScript.PlayPickUpAudio(PickupSFX);
            ScoreManagerScript.Instance.AddScore(false);
        }
    }
}
