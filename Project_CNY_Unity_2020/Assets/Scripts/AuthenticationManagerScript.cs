﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Text;
using UnityEngine.UI;

[System.Serializable]
public class RegisterUserData
{
    public string FIRST_NAME;
    public string LAST_NAME;
    public string PASSWORD;
    public string CONFIRMED_PASSWORD;
    public string EMAIL;
    public string MOBILE;
    public int PRIZE;
    public string PRIZE_REASON;
}

[System.Serializable]
public class LoginUserData
{
    public string EMAIL;
    public string PASSWORD;
}

[System.Serializable]
public class UpdatePasswordData
{
    public string EMAIL;
    public string MOBILE;
    public string NEW_PASSWORD;
    public string CONFIRMED_PASSWORD;
}

public class AuthenticationManagerScript : MonoBehaviour
{
    private string AuthenticationURL = "https://api.eminent.st/app/auth/";

    public string FirstName;
    public string LastName;
    public string Email;
    public string Mobile;
    public string Message;

    public Canvas LoginScreen;
    public Canvas RegisterScreen;
    public Canvas UpdatePasswordScreen;
    public Canvas MainTitleScreen;
    public Canvas LeaderboardScreen;

    public List<GameObject> FormErrors;

    public Text LoginMessage;
    public Text RegisterUserMessage;
    public Text UpdatePasswordMessage;

    [SerializeField] private RegisterUserData _registerUserData = new RegisterUserData();
    [SerializeField] private LoginUserData _loginUserData = new LoginUserData();
    [SerializeField] private UpdatePasswordData _updatePasswordData = new UpdatePasswordData();

    private static AuthenticationManagerScript _instance;

    public static AuthenticationManagerScript Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    private void Start()
    {
        GameManagerScript.Instance.EndGame();


        if(!GameManagerScript.Instance.bReturnToMainScreen)
        {
            StartCoroutine(InitializeGame(0.01f));
        }

        Screen.orientation = ScreenOrientation.Portrait;
    }

    public void PlayAsGuest()
    {
        GameManagerScript.Instance.UserID = -1;
        LoginScreen.enabled = false;

        GameManagerScript.Instance.StartGame();
    }

    public void LoginUser(string email, string password)
    {
        _loginUserData.EMAIL = email;
        _loginUserData.PASSWORD = password;

        string loginUserInfo = JsonUtility.ToJson(_loginUserData);

        StartCoroutine(PostLogin(loginUserInfo));
    }

    public void RegisterUser(string first_name, string last_name, string password, string confirmed_password, string email, string phone_number, int prize, string prize_reason)
    {
        _registerUserData.FIRST_NAME = first_name;
        _registerUserData.LAST_NAME = last_name;
        _registerUserData.PASSWORD = password;
        _registerUserData.CONFIRMED_PASSWORD = confirmed_password;
        _registerUserData.EMAIL = email;
        _registerUserData.MOBILE = phone_number;
        _registerUserData.PRIZE = prize;
        _registerUserData.PRIZE_REASON = prize_reason;

        string registerUserInfo = JsonUtility.ToJson(_registerUserData);

        StartCoroutine(PostRegister(registerUserInfo));
    }

    public void UpdateUserPassword(string email, string mobile_number, string new_password, string confirmed_password)
    {
        _updatePasswordData.EMAIL = email;
        _updatePasswordData.MOBILE = mobile_number;
        _updatePasswordData.NEW_PASSWORD = new_password;
        _updatePasswordData.CONFIRMED_PASSWORD = confirmed_password;

        string updatePasswordInfo = JsonUtility.ToJson(_updatePasswordData);

        StartCoroutine(PostUpdatePassword(updatePasswordInfo));
    }

    IEnumerator PostLogin(string Context)
    {
        string loginURL = AuthenticationURL + "login";

        UnityWebRequest loginRequest = HTTPRequestHelper.Instance.GeneratePostCallRequest(loginURL, Context);

        Debug.Log(Context);

        yield return loginRequest.SendWebRequest();

        if (loginRequest.error != null)
        {
            SimpleJSON.JSONNode loginUserInfo = SimpleJSON.JSON.Parse(loginRequest.downloadHandler.text);
            LoginMessage.color = Color.red;

            if(loginUserInfo["MESSAGE"] != "")
            {
                LoginMessage.text = loginUserInfo["MESSAGE"];
                Debug.LogError(loginRequest.error);
            }
            else
            {
                LoginMessage.text = loginRequest.error;
                Debug.LogError(loginRequest.error);
            }

            yield break;
        } 
        else
        {
            SimpleJSON.JSONNode loginUserInfo = SimpleJSON.JSON.Parse("Received: " + loginRequest.downloadHandler.text);

            GameManagerScript.Instance.UserID = loginUserInfo["ID"];
            FirstName = loginUserInfo["FIRST_NAME"];
            LastName = loginUserInfo["LAST_NAME"];
            Email = loginUserInfo["EMAIL"];
            Mobile = loginUserInfo["MOBILE"];

            Debug.Log("Deactivate");

            Debug.Log(LoginScreen);
            StartCoroutine(InitializeGame(0.01f));
        }
    }

    public IEnumerator InitializeGame(float WaitTime)
    {
        yield return new WaitForSeconds(WaitTime);

        LoginScreen.enabled = false;
        MainTitleScreen.enabled = false;
        LeaderboardScreen.gameObject.SetActive(false);

        GameManagerScript.Instance.StartGame();
    }

    IEnumerator PostRegister(string Context)
    {
        string registerURL = AuthenticationURL + "register";

        UnityWebRequest registerRequest = HTTPRequestHelper.Instance.GeneratePostCallRequest(registerURL, Context);

        yield return registerRequest.SendWebRequest();

        if (registerRequest.error != null)
        {
            SimpleJSON.JSONNode registerUserInfo = SimpleJSON.JSON.Parse("Received: " + registerRequest.downloadHandler.text);
            RegisterUserMessage.color = Color.red;

            if (registerUserInfo["MESSAGE"] != "")
            {
                RegisterUserMessage.text = registerUserInfo["MESSAGE"];
                Debug.LogError(registerUserInfo["MESSAGE"]);
            }
            else
            {
                RegisterUserMessage.text = registerRequest.error;
                Debug.LogError(registerRequest.error);
            }
            yield break;
        } 
        else
        {
            SimpleJSON.JSONNode registerUserInfo = SimpleJSON.JSON.Parse("Received: " + registerRequest.downloadHandler.text);
            RegisterUserMessage.text = registerUserInfo["MESSAGE"];
            Debug.Log(registerUserInfo["MESSAGE"]);
            RegisterUserMessage.color = Color.green;
        }
    }

    IEnumerator PostUpdatePassword(string Context)
    {
        string updatePasswordURL = AuthenticationURL + "updatePassword";

        UnityWebRequest updatePasswordRequest = HTTPRequestHelper.Instance.GeneratePostCallRequest(updatePasswordURL, Context);

        yield return updatePasswordRequest.SendWebRequest();

        if (updatePasswordRequest.error != null)
        {
            SimpleJSON.JSONNode updatePasswordInfo = SimpleJSON.JSON.Parse(updatePasswordRequest.downloadHandler.text);
            UpdatePasswordMessage.color = Color.red;

            if (updatePasswordInfo["MESSAGE"] != "")
            {
                UpdatePasswordMessage.text = updatePasswordInfo["MESSAGE"];
                Debug.LogError(updatePasswordRequest.error);
            }
            else
            {
                UpdatePasswordMessage.text = updatePasswordRequest.error;
                Debug.LogError(updatePasswordRequest.error);
            }
           
            yield break;
        }
        else
        {
            SimpleJSON.JSONNode updatePasswordInfo = SimpleJSON.JSON.Parse(updatePasswordRequest.downloadHandler.text);
            UpdatePasswordMessage.text = updatePasswordInfo["MESSAGE"];
            Debug.Log(updatePasswordInfo["MESSAGE"]);
            UpdatePasswordMessage.color = Color.green;
        }
    }
}
