﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Text;
using System.Runtime.InteropServices;
using System;

public class HTTPRequestHelper : MonoBehaviour
{
    private static HTTPRequestHelper _instance;

    public static HTTPRequestHelper Instance { get { return _instance; } }


    [DllImport("__Internal")]
    private static extern bool IsMobileBrowser();

    [DllImport("__Internal")]
    private static extern void openLink(string _url);

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        #if !UNITY_EDITOR && UNITY_WEBGL
            WebGLInput.captureAllKeyboardInput = true;
        #endif
    }

    public UnityWebRequest GeneratePostCallRequest(string requestURL, string Context)
    {
        var request = new UnityWebRequest(requestURL, "POST");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(Context);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");

        return request;
    }

    public bool CheckIfHTTPRunningOnMobile()
    {
#if UNITY_WEBGL
        try
        {
            return IsMobileBrowser();
        }
        catch(Exception error)
        {
            Debug.LogException(error, this);
            return false;
        }
#endif
    }

    public void OpenPage(string url)
    {
        openLink(url);
    }
}
