﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxScrollingScript : MonoBehaviour
{
    private float Distance, StartPos, Length;
    public float ParallaxSpeed;
    public float ParallaxOffset = 0.0f;
    public List<GameObject> BackgroundPlanes;

    GameObject Player;
    PlayerBehavior playerBehaviorScript;

    // Start is called before the first frame update
    void Start()
    {
        StartPos = transform.position.x;

        Player = GameObject.Find("Player");
        playerBehaviorScript = Player.GetComponent<PlayerBehavior>();

        if (ParallaxSpeed > 0)
        {
            for (int i = transform.childCount - 1; i >= 0; i--)
            {
                BackgroundPlanes.Add(transform.GetChild(i).gameObject);
            }
        }
        else
        {
            for (int i = 0; i <= transform.childCount - 1; i++)
            {
                BackgroundPlanes.Add(transform.GetChild(i).gameObject);
            }
        }


        if (BackgroundPlanes.Count > 0)
        {
            Length = BackgroundPlanes[0].GetComponent<SpriteRenderer>().bounds.size.x;

            Distance = ParallaxSpeed > 0.0f ? BackgroundPlanes[0].transform.position.x
                : BackgroundPlanes[0].transform.position.x - (Length * 0.5f);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(playerBehaviorScript.GetPlayerIsAlive())
        {
            foreach (GameObject backgroundPlane in BackgroundPlanes.ToArray())
            {
                backgroundPlane.transform.position = new Vector3(backgroundPlane.transform.position.x + ParallaxSpeed,
                    backgroundPlane.transform.position.y, backgroundPlane.transform.position.z);

                if (ParallaxSpeed > 0)
                {
                    if (backgroundPlane.transform.position.x >= Distance)
                    {
                        backgroundPlane.transform.position = new Vector3(BackgroundPlanes[0].transform.position.x - Length - ParallaxSpeed - ParallaxOffset, 
                            backgroundPlane.transform.position.y, backgroundPlane.transform.position.z);
                        BackgroundPlanes.Remove(backgroundPlane);
                        BackgroundPlanes.Insert(0, backgroundPlane);
                    }
                }
                else
                {
                    if (backgroundPlane.transform.position.x <= Distance)
                    {
                        backgroundPlane.transform.position = new Vector3(BackgroundPlanes[BackgroundPlanes.Count - 1].transform.position.x + Length + ParallaxSpeed + ParallaxOffset, 
                            backgroundPlane.transform.position.y, backgroundPlane.transform.position.z);
                        BackgroundPlanes.Remove(backgroundPlane);
                        BackgroundPlanes.Add(backgroundPlane);
                    }
                }

            }
        }
    }
}
