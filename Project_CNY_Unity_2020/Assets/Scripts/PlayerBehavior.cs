﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehavior : MonoBehaviour, IGameLoop
{
    private bool bGravityEnabled = false;
    private bool bAbleToJump = true;
    private bool bIsAlive = true;
    private bool bEnableGravityAcceleration = false;
    private bool bIsStarted = false;
    private bool bWonGame = false;
    private bool bGameEnded = false;
    private Vector3 MovementDirection;
    public enum EJumpState {Fall, Jump, Fly};
    private EJumpState CurrentJumpState;
    private EJumpState PreviousJumpState;
    public float CurrentGravity = 0.0004f;
    public float Gravity_Increment = 0.0005f;
    public float MaxGravity = 0.0025f;

    Vector3 CurrentVelocity;
    public float JumpVelocity = 0.0065f;
    public float JumpTimer = 0.30f;
    public float MovementSpeed = 0.005f;
    public float FlyingMovementSpeed = 0.010f;

    //Fly Variables
    private bool bHoldJump = false;
    public bool bInBoundary = true;

    public float CurrentFlyVelocity = 0.0f;
    public float MinimumFlyVelocity = -0.01f;
    public float MaximumFlyVelocity = 0.01f;
    public float FlyVelocity_Increment = 0.0005f;
    private bool bFlying = false;
    private float CurrentFlyTime = 0.0f;
    public float MaxFlyTime = 5.0f;

    public float AnimationSpeed = 2.0f;

    // Winning Action
    private Vector3 Winning_StartingLocation = Vector3.zero;
    private Vector3 Winning_EndLocation = new Vector3(0.0f, 0.36f, 0.0f);
    private float WinningMove_Alpha = 0.0f;

    private bool bOpenLeaderboard = false;

    // Animation States
    const string PLAYER_EVOLVE = "Evolve";
    const string PLAYER_SWIM = "Swim";
    const string PLAYER_SWIM_02 = "Swim_02";
    const string FADE_IN_OUT = "FadeInOut";

    int PLAYER_EVOLVE_ID;
    int PLAYER_SWIM_ID;
    int PLAYER_SWIM_02_ID;
    int FADE_IN_OUT_ID;

    private int CurrentStateID;
    private int CrossFadeCurrentStateID;

    // SFX
    public AudioClip JumpSFX;
    public AudioClip DeathSFX;

    public Canvas ScoreScreenUI;
    public Canvas WinningScreenUI;
    public Canvas CrossfadeUI;
    public Canvas TapToContinueUI;

    private Vector3 StartingPosition;
    private Quaternion StartingRotation;
    private Quaternion TargetRotation;

    private Rigidbody2D PlayerRigidbody;
    private Animator AnimatorComp;
    private Animator CrossFadeAnimatorComp;

    private IEnumerator ResetJumpCoroutine;
    private IEnumerator StopJumpCoroutine;

    // Start is called before the first frame update
    void Awake()
    {
        PlayerRigidbody = GetComponent<Rigidbody2D>();
        AnimatorComp = GetComponent<Animator>();
        CrossFadeAnimatorComp = CrossfadeUI.GetComponent<Animator>();

        PLAYER_EVOLVE_ID = Animator.StringToHash(PLAYER_EVOLVE);
        PLAYER_SWIM_ID = Animator.StringToHash(PLAYER_SWIM);
        PLAYER_SWIM_02_ID = Animator.StringToHash(PLAYER_SWIM_02);
        FADE_IN_OUT_ID = Animator.StringToHash(FADE_IN_OUT);
    }

    private void Start()
    {
        MovementDirection = new Vector3(0.0f, -1.0f, 0.0f);
        CurrentJumpState = EJumpState.Jump;
        PreviousJumpState = CurrentJumpState;

        StartingPosition = transform.position;

        bFlying = false;
        CurrentFlyTime = 0.0f;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (bGameEnded) return;

        UpdateMovement();

        if (!bWonGame && IsGravityEnabled())
        {
            RotateBasedOnVelocity();
        }
    }

    void Update()
    {
        HandleInput();
    }

    public void StartGame()
    {
        bIsStarted = true;
        AnimatorComp.enabled = true;
        AnimatorComp.speed = AnimationSpeed;
        ScoreScreenUI.gameObject.SetActive(true);

        StartCoroutine(EnableGravityOverTime(1.5f));
    }

    void HandleInput()
    {
        if (bIsStarted)
        {
            if (bIsAlive && bInBoundary)
            {
                if (Input.GetButtonDown("Jump"))
                {
                    bHoldJump = true;

                    if (CurrentJumpState == EJumpState.Fly)
                        CurrentFlyVelocity = 0.0f;
                    else
                        PerformJump();
                }

                if (Input.GetButtonUp("Jump"))
                {
                    bHoldJump = false;

                    if (CurrentJumpState == EJumpState.Fly)
                        CurrentFlyVelocity = 0.0f;
                }

                if (Input.touchCount > 0)
                {
                    Touch touch = Input.GetTouch(0);

                    if (touch.phase == TouchPhase.Began)
                    {
                        bHoldJump = true;

                        if (CurrentJumpState == EJumpState.Fly)
                            CurrentFlyVelocity = 0.0f;
                        else
                            PerformJump();
                    }

                    if (touch.phase == TouchPhase.Ended)
                    {
                        bHoldJump = false;

                        if (CurrentJumpState == EJumpState.Fly)
                            CurrentFlyVelocity = 0.0f;
                    }
                }
            }

            if (bOpenLeaderboard)
            {
                if (Input.GetButtonDown("Jump"))
                {
                    StartCoroutine(OpenLeaderboardAfterTime(0.5f));
                }

                if (Input.touchCount > 0)
                {
                    Touch touch = Input.GetTouch(0);

                    if (touch.phase == TouchPhase.Began)
                    {
                        StartCoroutine(OpenLeaderboardAfterTime(0.5f));
                    }
                }
            }
        }
    }


    void ToggleGravity(bool bToggleGravity)
    {
        bGravityEnabled = bToggleGravity;
    }

    bool IsGravityEnabled()
    {
        return bGravityEnabled;
    } 

    void ResetToFallingState()
    {
        MovementDirection = new Vector3(0.0f, -1.0f, 0.0f);
        CurrentJumpState = EJumpState.Fall;
        bEnableGravityAcceleration = true;

        ResetJumpCoroutine = ResetJumpTimer(0.05f);
        StartCoroutine(ResetJumpCoroutine);
    }

    void ResetJump()
    {
        bAbleToJump = true;
    }

    void ResetGravity()
    {
        bEnableGravityAcceleration = false;
        CurrentGravity = 0.0004f;
    }

    void UpdateGravityAcceleration()
    {
        CurrentGravity = Mathf.Clamp(CurrentGravity + Gravity_Increment, 0.0f, MaxGravity);
    }

    void UpdateMovement()
    {
        if (bWonGame)
        {
            transform.position = Vector3.Lerp(Winning_StartingLocation, Winning_EndLocation, WinningMove_Alpha);
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0.0f, 0.0f, 0.0f), 0.05f);

            WinningMove_Alpha += 0.01f;

            if (WinningMove_Alpha > 1.0f)
            {
                WinningMove_Alpha = 0.0f;
                bGameEnded = true;

                ChangeAnimationState(PLAYER_EVOLVE_ID);
                CrossFade_ChangeAnimationState(FADE_IN_OUT_ID);
                StartCoroutine(AbleToProceedLeaderboardAfterTime(7.0f));
            }
        }
        else
        {
            if (CurrentJumpState == EJumpState.Fly)
            {
                UpdateFlyMovement();
            }

            if (IsGravityEnabled())
            {
                switch (CurrentJumpState)
                {
                    case EJumpState.Jump:
                        CurrentVelocity.y += JumpVelocity;
                        break;
                    case EJumpState.Fall:
                        CurrentVelocity.y += CurrentGravity;
                        break;
                    case EJumpState.Fly:
                        CurrentVelocity.y = CurrentFlyVelocity;
                        break;
                }

                transform.position += Vector3.Scale(MovementDirection, CurrentVelocity);

                CurrentVelocity = Vector3.zero;

                if (bEnableGravityAcceleration)
                {
                    UpdateGravityAcceleration();
                }
            }
        }
    }

    void RotateBasedOnVelocity()
    {
        if(CurrentJumpState != PreviousJumpState)
        {
            PreviousJumpState = CurrentJumpState;

            switch (CurrentJumpState)
            {
                case EJumpState.Jump:
                    TargetRotation.eulerAngles = new Vector3(0.0f, 0.0f, 35.0f);
                    break;
                case EJumpState.Fall:
                    TargetRotation.eulerAngles = new Vector3(0.0f, 0.0f, -15.0f);
                    break;
                default:
                    TargetRotation.eulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
                    break;
            }
        }

        transform.rotation = Quaternion.Slerp(transform.rotation, TargetRotation, CurrentJumpState == EJumpState.Jump ?  5.0f : 0.05f);
    }

    private void PerformJump()
    {
        if (bAbleToJump)
        {
            if (!IsGravityEnabled())
            {
                ToggleGravity(true);
            }

            bAbleToJump = false;
            MovementDirection = new Vector3(0.0f, 1.0f, 0.0f);
            CurrentJumpState = EJumpState.Jump;

            ResetGravity();

            StopJumpCoroutine = StopJumpTimer(JumpTimer);
            StartCoroutine(StopJumpCoroutine);

            AudioManagerScript.Instance.PlaySFX(JumpSFX);
        }
    }
    public void PerformDeath()
    {
        if (bIsAlive)
        {
            LeaderboardManagerScript.Instance.UpdateUserScoreToLeaderboard();
            bIsAlive = false;

            PlayerRigidbody.bodyType = RigidbodyType2D.Dynamic;

            StartCoroutine(OpenLeaderboardAfterTime(1.0f));

            AudioManagerScript.Instance.PlaySFX(DeathSFX);

            AudioManagerScript.Instance.StopBGM();

            StopCoroutine(ResetJumpCoroutine);
            StopCoroutine(StopJumpCoroutine);
        }
    }

    public void PerformWinningAction()
    {
        if (bIsAlive)
        {
            LeaderboardManagerScript.Instance.UpdateUserScoreToLeaderboard();
            bIsAlive = false;

            bWonGame = true;
            Winning_StartingLocation = transform.position;
            WinningScreenUI.GetComponent<WinningScreenFadeUIScript>().FadeInWinningScreen();
        }
    }

    private IEnumerator StopJumpTimer(float StopJumpTime)
    {
        yield return new WaitForSeconds(StopJumpTime);
        ResetToFallingState();

        StartCoroutine(StopJumpCoroutine);
    }
    private IEnumerator ResetJumpTimer(float ResetJumpTime)
    {
        yield return new WaitForSeconds(ResetJumpTime);
        ResetJump();
    }

    private IEnumerator EnableGravityOverTime(float WaitTime)
    {
        yield return new WaitForSeconds(WaitTime);

        if (!IsGravityEnabled())
        {
            PerformJump();
        }
    }

    protected void UpdateFlyMovement()
    {
        if(bFlying)
        {
            if (bHoldJump && bInBoundary)
            {
                CurrentFlyVelocity = Mathf.Clamp(CurrentFlyVelocity + FlyVelocity_Increment, MinimumFlyVelocity, MaximumFlyVelocity);
            }
            else
            {
                CurrentFlyVelocity = Mathf.Clamp(CurrentFlyVelocity - FlyVelocity_Increment, MinimumFlyVelocity, MaximumFlyVelocity);
            }

            CurrentFlyTime += Time.deltaTime;

            if(CurrentFlyTime >= MaxFlyTime)
            {
                RevertFlyAction();

                AudioManagerScript.Instance.PlayMainBGM();

                if (!IsGravityEnabled())
                {
                    PerformJump();
                }
            }
        }
        
    }
    public void ExecuteFlyAction()
    {
        CurrentFlyTime = 0.0f;
        CurrentFlyVelocity = 0.0f;
        MovementDirection = new Vector3(0.0f, 1.0f, 0.0f);
        CurrentJumpState = EJumpState.Fly;

        bFlying = true;

        if(IsGravityEnabled())
        {
            StopCoroutine(ResetJumpCoroutine);
            StopCoroutine(StopJumpCoroutine);
        }

        AudioManagerScript.Instance.PlayPowerUpBGM();
    }
    private void RevertFlyAction()
    {
        CurrentFlyTime = 0.0f;
        CurrentFlyVelocity = 0.0f;
        bFlying = false;
        ResetGravity();
        ResetToFallingState();
    }

    public void PlayPickUpAudio(AudioClip _audioClip)
    {
        AudioManagerScript.Instance.PlaySFX(_audioClip);
    }

    protected void ChangeAnimationState(int _newStateID)
    {
        if (CurrentStateID == _newStateID) return;

        AnimatorComp.Play(_newStateID);

        CurrentStateID = _newStateID;
    }

    private void CrossFade_ChangeAnimationState(int _newStateID)
    {
        if(CrossFadeCurrentStateID == _newStateID) return;

        CrossFadeAnimatorComp.Play(_newStateID);

        CrossFadeCurrentStateID = _newStateID;
    }

    public void SecondFormTransformation()
    {
        ChangeAnimationState(PLAYER_SWIM_02_ID);
    }

    public void LockPlayerMovement(bool bLock)
    {
        if(bLock)
        {
            StopCoroutine(ResetJumpCoroutine);
            StopCoroutine(StopJumpCoroutine);

            ToggleGravity(false);
            transform.rotation = Quaternion.Euler(Vector3.zero);
            transform.position = StartingPosition;
            ResetGravity();
            ResetJump();

            bIsStarted = false;
        }
        else
        {
            bIsStarted = true;
        }
    }

    public bool GetGameStarted()
    {
        return bIsStarted;
    }

    public bool GetPlayerIsAlive()
    {
        return bIsAlive;
    }

    public EJumpState GetCurrentJumpState()
    {
        return CurrentJumpState;
    }

    IEnumerator AbleToProceedLeaderboardAfterTime(float WaitTime)
    {
        yield return new WaitForSeconds(WaitTime);
        bOpenLeaderboard = true;
        TapToContinueUI.gameObject.SetActive(true);
        CrossfadeUI.gameObject.SetActive(false);
    }

    IEnumerator OpenLeaderboardAfterTime(float WaitTime)
    {
        yield return new WaitForSeconds(WaitTime);
        TapToContinueUI.gameObject.SetActive(false);
        LeaderboardManagerScript.Instance.LeaderboardUI.gameObject.SetActive(true);
    }
}
