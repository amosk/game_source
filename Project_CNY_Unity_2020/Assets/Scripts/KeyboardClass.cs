﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Runtime.InteropServices;
using System;

public class KeyboardClass : MonoBehaviour, ISelectHandler {

	public string PopUpMessage;
	public int StringLimit = 50;
	public bool bOnlyAcceptIntValue = false;
	public bool bValidateEmail = false;
	public bool bValidateMobileNumber = false;
	private bool bIsMobile = false;

	[DllImport("__Internal")]
	private static extern void focusHandleAction (string _popupmessage, string _name, int _strlimit, bool _bOnlyInt, bool _bValidateEmail, bool _bValidateMobileNumber);

    private void Start()
    {
        bIsMobile = HTTPRequestHelper.Instance.CheckIfHTTPRunningOnMobile();
	}

    public void ReceiveInputData(string value) {
		gameObject.GetComponent<InputField> ().text = value;
    }

	public void OnSelect(BaseEventData data) {
		#if UNITY_WEBGL
		try
		{
			if(bIsMobile)
            {
                focusHandleAction(PopUpMessage, gameObject.name, StringLimit, bOnlyAcceptIntValue, bValidateEmail, bValidateMobileNumber);
            }
        }
        catch (Exception error)
		{
			Debug.LogException(error, this);
		}
		#endif
	}
}
