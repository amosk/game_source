﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManagerScript : MonoBehaviour, IGameLoop
{
    // Audio players components.
    private AudioSource EffectsSource;
    private AudioSource MusicSource;

    public AudioClip MainBGM;
    public AudioClip PowerUpBGM;

    // Random pitch adjustment range.
    public float LowPitchRange = .95f;
    public float HighPitchRange = 1.05f;

    private bool bPlayingPowerUp = false;

    private float MainBGMTime = 0.0f;

    public static AudioManagerScript Instance { get { return _instance; } }

    private static AudioManagerScript _instance;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        EffectsSource = null;
        MusicSource = null;
    }

    private void Start()
    {
        EffectsSource = gameObject.transform.Find("SFX").GetComponent<AudioSource>();
        MusicSource = gameObject.transform.Find("Music").GetComponent<AudioSource>();

        AdjustVolume(GameManagerScript.Instance.MusicVolume, GameManagerScript.Instance.SFXVolume);
    }

    // Start is called before the first frame update
    public void StartGame()
    {
        PlayMainBGM();
    }

    public void PlayMainBGM()
    {
        if (MusicSource)
        {
            MusicSource.Stop();
            MusicSource.clip = MainBGM;
            MusicSource.Play();
            MusicSource.time = MainBGMTime;

            bPlayingPowerUp = false;

            MainBGMTime = 0.0f;
        }
    }

    public void PlayPowerUpBGM()
    {
        if(MusicSource)
        {
            if (!bPlayingPowerUp)
            {
                MainBGMTime = MusicSource.time;
            }

            MusicSource.Stop();
            MusicSource.clip = PowerUpBGM;
            MusicSource.Play();

            bPlayingPowerUp = true;
        }
    }

    public void StopBGM()
    {
        if(MusicSource)
        {
            MusicSource.Stop();
        }
    }

    // Play a single clip through the sound effects source.
    public void PlaySFX(AudioClip clip)
    {
        if (EffectsSource)
        {
            EffectsSource.PlayOneShot(clip);
        }
    }

    public void AdjustVolume(float musicVolume, float effectVolume)
    {
        MusicSource.volume = musicVolume;
        GameManagerScript.Instance.MusicVolume = musicVolume;
        EffectsSource.volume = effectVolume;
        GameManagerScript.Instance.SFXVolume = effectVolume;
    }

    public bool IsMute()
    {
        if (MusicSource.volume == 0.0f || EffectsSource.volume == 0.0f)
        {
            return true;
        }

        return false;
    }
}
