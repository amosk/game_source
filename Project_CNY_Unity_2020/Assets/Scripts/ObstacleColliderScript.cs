﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleColliderScript : MonoBehaviour
{
    ObstacleSpawner obstacleSpawnerScript;
    // Start is called before the first frame update
    void Start()
    {
        GameObject ObstacleSpawner = GameObject.Find("ObstacleSpawner");
        obstacleSpawnerScript = ObstacleSpawner.GetComponent<ObstacleSpawner>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.parent != null && other.transform.parent.parent != null)
        {
            if (obstacleSpawnerScript.SpawnedObstacle_Amounts <= obstacleSpawnerScript.MaximumSpawn_Count)
            {
                GameObject CurrentObstacle = other.transform.parent.parent.gameObject;
                obstacleSpawnerScript.SpawnedObstacles.Remove(CurrentObstacle);
                float NewObstacleOffset = obstacleSpawnerScript.SpawnedObstacles[obstacleSpawnerScript.SpawnedObstacles.Count - 1].transform.position.x + obstacleSpawnerScript.Obstacle_Gap;
                float NewObstacleHeight = Random.Range(obstacleSpawnerScript.Obstacle_MinimumHeight, obstacleSpawnerScript.Obstacle_MaximumHeight);
                CurrentObstacle.transform.position = new Vector3(NewObstacleOffset, NewObstacleHeight, 0.0f);
                obstacleSpawnerScript.SpawnedObstacles.Add(CurrentObstacle);
                CurrentObstacle.GetComponent<ObstacleScript>().ReconstructObstacle();
                obstacleSpawnerScript.SpawnedObstacle_Amounts++;
            }
        }
    }
}
