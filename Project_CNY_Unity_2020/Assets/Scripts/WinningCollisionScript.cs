﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinningCollisionScript : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<PlayerBehavior>())
        {
            other.gameObject.GetComponent<PlayerBehavior>().PerformWinningAction();
        }
    }
}
