﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour
{
    public GameObject ObstaclePrefab;

    public int Obstacle_Amounts = 20;
    public float Obstacle_Gap = 0.8f;
    public float Obstacle_StartingOffset = 0.0f;
    public float Obstacle_MinimumHeight = -0.27f;
    public float Obstacle_MaximumHeight = 0.2f;

    public float MaximumSpawn_Count = 999;
    public float SpawnedObstacle_Amounts = 0;

    public  List<GameObject> SpawnedObstacles;
    
    // Start is called before the first frame update
    void Start()
    {
        PrepopulateObstacles();
    }

    private void PrepopulateObstacles()
    {
        for (int i = 0; i <= Obstacle_Amounts - 1; i++)
        {
            Vector3 ObstaclePos = new Vector3(
                (i * Obstacle_Gap) + Obstacle_StartingOffset,
                Random.Range(Obstacle_MinimumHeight, Obstacle_MaximumHeight),
                0.0f);
            GameObject Obstacle = Instantiate(ObstaclePrefab, ObstaclePos, Quaternion.identity);
            SpawnedObstacles.Add(Obstacle);

            SpawnedObstacle_Amounts++;
        }
    }

    public void HideObstacles()
    {
        for (int i = 0; i <= Obstacle_Amounts - 1; i++)
        {
            SpawnedObstacles[i].gameObject.SetActive(false);
        }
    }

    public void RegenerateObstacles()
    {
        for (int i = 0; i <= Obstacle_Amounts - 1; i++)
        {
            Vector3 ObstaclePos = new Vector3(
                (i * Obstacle_Gap) + Obstacle_StartingOffset,
                Random.Range(Obstacle_MinimumHeight, Obstacle_MaximumHeight),
                0.0f);

            SpawnedObstacles[i].gameObject.SetActive(true);
            SpawnedObstacles[i].transform.position = ObstaclePos;
            SpawnedObstacles[i].GetComponent<ObstacleScript>().ReconstructObstacle();
        }
    }
}

