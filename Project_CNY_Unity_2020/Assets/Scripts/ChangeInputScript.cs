﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class ChangeInputScript : MonoBehaviour
{
    EventSystem CurrentSystem;
    public Selectable FirstInput;
    public Button SubmitButton;
    private bool bIsMobile = false;

    // Start is called before the first frame update
    void Start()
    {
        bIsMobile = HTTPRequestHelper.Instance.CheckIfHTTPRunningOnMobile();

        if (!bIsMobile)
        {
            AssignUINavigation();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(!bIsMobile)
        {
            if ((Input.GetKeyDown(KeyCode.Tab) && Input.GetKey(KeyCode.LeftShift)))
            {
                Selectable previous = CurrentSystem.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnUp();

                if (previous != null)
                {
                    previous.Select();
                }
            }
            else if (Input.GetKeyDown(KeyCode.Tab))
            {
                Selectable next = CurrentSystem.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnDown();

                if (next != null)
                {
                    next.Select();
                }
            }
            else if (Input.GetKeyDown(KeyCode.Return))
            {
                SubmitButton.onClick.Invoke();
            }
        }
    }

    public void AssignUINavigation()
    {
        bIsMobile = HTTPRequestHelper.Instance.CheckIfHTTPRunningOnMobile();

        if (!bIsMobile)
        {
            CurrentSystem = EventSystem.current;
            FirstInput.Select();
        }
    }
}
