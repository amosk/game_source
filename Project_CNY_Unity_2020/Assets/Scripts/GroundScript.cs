﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundScript : MonoBehaviour
{
    GameObject Player;
    PlayerBehavior playerBehaviorScript;

    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.Find("Player");
        playerBehaviorScript = Player.GetComponent<PlayerBehavior>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<PlayerBehavior>())
            playerBehaviorScript.PerformDeath();
    }
}
