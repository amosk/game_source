﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManagerScript : MonoBehaviour
{
    public int CurrentScore = 0;
    public int CurrentScore_Special = 0;
    public float CurrentDuration = 0.0f;

    private static ScoreManagerScript _instance;

    public static ScoreManagerScript Instance { get { return _instance; } }

    public Canvas CrossFadeUI;
    private CrossfadeUIScript CrossFadeUIComp;
    ObstacleSpawner obstacleSpawnerScript;

    public int ScoreTransitionThreshold1 = 300;
    public int ScoreTransitionThreshold2 = 600;

    [System.Serializable]
    public struct ParallaxBackgrounds
    {
        public GameManagerScript.ELevels Level;
        public GameObject ParallaxBackground;
    }
    public ParallaxBackgrounds[] ParallaxBackgroundsPrefab;

    GameObject Player;
    PlayerBehavior playerBehaviorScript;

    private void Awake()
    {
        if(_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        if(CrossFadeUI)
        {
            CrossFadeUIComp = CrossFadeUI.GetComponent<CrossfadeUIScript>();
        }
    }

    private void Start()
    {
        CurrentScore = 0;

        Player = GameObject.Find("Player");
        playerBehaviorScript = Player.GetComponent<PlayerBehavior>();

        GameObject ObstacleSpawner = GameObject.Find("ObstacleSpawner");
        obstacleSpawnerScript = ObstacleSpawner.GetComponent<ObstacleSpawner>();

        GameManagerScript.Instance.CurrentLevel = (GameManagerScript.ELevels) 0;
    }

    private void Update()
    {
        CurrentDuration += Time.deltaTime;
    }

    public void AddScore(bool bIsScoreSpecial)
    {
        CurrentScore += 1;
        CurrentScore_Special += bIsScoreSpecial ? 1 : 0;

        if (CurrentScore == ScoreTransitionThreshold1 || CurrentScore == ScoreTransitionThreshold2)
        {
            if (CrossFadeUIComp)
            {
                GameManagerScript.Instance.ProceedToNextLevel();
                CrossFadeUIComp.TriggerTransition();
                obstacleSpawnerScript.HideObstacles();
                playerBehaviorScript.LockPlayerMovement(true);

                StartCoroutine(RegenerateObstacle(1.5f));
            }
        }
    }

    private IEnumerator RegenerateObstacle(float WaitTime)
    {
        yield return new WaitForSeconds(WaitTime);

        obstacleSpawnerScript.RegenerateObstacles();
        playerBehaviorScript.LockPlayerMovement(false);

        if (CurrentScore == ScoreTransitionThreshold2)
        {
            // Player transform
            playerBehaviorScript.SecondFormTransformation();
        }

        for (int i = 0; i < (int)GameManagerScript.ELevels.Mountain; i++)
        {
            if (i == (int)GameManagerScript.Instance.CurrentLevel)
            {
                ParallaxBackgroundsPrefab[i].ParallaxBackground.gameObject.SetActive(true);
            }
            else
            {
                ParallaxBackgroundsPrefab[i].ParallaxBackground.gameObject.SetActive(false);
            }
        }
        
    }
}
