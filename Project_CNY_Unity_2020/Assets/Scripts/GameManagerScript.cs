﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManagerScript : MonoBehaviour
{
    public int UserID = -1;
    public bool bStarted = false;

    public enum ELevels { River, River_Mountain, Mountain};
    public ELevels CurrentLevel = ELevels.River;

    public float MusicVolume = 0.7f;
    public float SFXVolume = 0.5f;

    public bool bReturnToMainScreen = true;

    private static GameManagerScript _instance;

    public static GameManagerScript Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        DontDestroyOnLoad(this);
    }
    public void StartGame()
    {
        if(!bStarted)
        {
            List<IGameLoop> gameLoopInterfaces = FindInterfaces.Find<IGameLoop>();

            foreach (IGameLoop gameLoopInterface in gameLoopInterfaces)
            {
                gameLoopInterface.StartGame();
            }

            bStarted = true;
        }
    }

    public void EndGame()
    {
        bStarted = false;
    }

    public void ProceedToNextLevel()
    {
        CurrentLevel = (ELevels)Mathf.Clamp((int)CurrentLevel + 1, 0, (int)ELevels.Mountain);
    }
}

public static class FindInterfaces
{
    public static List<T> Find <T>()
    {
        List<T> interfaces = new List<T>();

        GameObject[] rootGameObjects = SceneManager.GetActiveScene().GetRootGameObjects();
        foreach(var rootGameObject in rootGameObjects)
        {
            T[] childrenInterfaces = rootGameObject.GetComponentsInChildren<T>();
            foreach(var childInterface in childrenInterfaces)
            {
                interfaces.Add(childInterface);
            }
        }
        return interfaces;
    }
}

