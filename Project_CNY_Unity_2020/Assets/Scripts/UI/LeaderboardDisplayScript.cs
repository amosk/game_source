﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LeaderboardDisplayScript : MonoBehaviour
{
    public GameObject leaderboardObjectPrefab;
    public Transform leaderboardContent;
    public Transform leaderboardUserContent;
    public Canvas MainTitleUI;
    public Button PlayButton;

    private void Update()
    {
        if (GameManagerScript.Instance.bStarted)
        {
            PlayButton.gameObject.SetActive(true);
        }
        else
        {
            PlayButton.gameObject.SetActive(false);
        }
    }

    public void GenerateLeaderboard()
    {
        foreach (Transform child in leaderboardContent.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        foreach (LeaderboardData leaderboardInfo in LeaderboardManagerScript.Instance.LeaderboardInfos)
        {
            GameObject leaderboardEntry = Instantiate(leaderboardObjectPrefab, leaderboardContent);
            Text[] leaderboardTexts = leaderboardEntry.GetComponentsInChildren<Text>();
            leaderboardTexts[0].text = leaderboardInfo.RANK.ToString();
            leaderboardTexts[1].text = leaderboardInfo.EMAIL;
            leaderboardTexts[2].text = leaderboardInfo.SCORE.ToString();
        }

        foreach (Transform child in leaderboardUserContent.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        if (GameManagerScript.Instance.UserID != -1)
        {
            LeaderboardData userLeaderboardInfo = LeaderboardManagerScript.Instance.CurrentUserLeaderboardInfo;

            GameObject leaderboardEntry = Instantiate(leaderboardObjectPrefab, leaderboardUserContent);
            Text[] leaderboardTexts = leaderboardEntry.GetComponentsInChildren<Text>();
            leaderboardTexts[0].text = userLeaderboardInfo.RANK.ToString();
            leaderboardTexts[1].text = userLeaderboardInfo.EMAIL;
            leaderboardTexts[2].text = userLeaderboardInfo.SCORE.ToString();
        }
    }

    public void CloseLeaderboard()
    {
        if (GameManagerScript.Instance.bStarted)
        {
            GameManagerScript.Instance.bReturnToMainScreen = true;

            SceneManager.LoadScene("Game");
        }
        else
        {
            this.gameObject.SetActive(false);
            MainTitleUI.gameObject.SetActive(true);
        }
    }

    public void CloseLeaderboardAndRestart()
    {
        GameManagerScript.Instance.bReturnToMainScreen = false;

        SceneManager.LoadScene("Game");
    }
}
