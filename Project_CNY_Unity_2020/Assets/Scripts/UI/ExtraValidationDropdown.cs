﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExtraValidationDropdown : MonoBehaviour
{
    public Text ErrorMessage;
    private Dropdown DropdownComp;
    public string ErrorMessageText;

    // Start is called before the first frame update
    void Start()
    {
        DropdownComp = GetComponent<Dropdown>();
    }

    public void DetectDropdownIsSelected()
    {
        if(DropdownComp.value == 0)
        {
            ErrorMessage.text = "Select a " + ErrorMessageText;

            if (!AuthenticationManagerScript.Instance.FormErrors.Contains(gameObject))
            {
                AuthenticationManagerScript.Instance.FormErrors.Add(gameObject);
            }
        }
        else
        {
            ErrorMessage.text = "";

            if (AuthenticationManagerScript.Instance.FormErrors.Contains(gameObject))
            {
                AuthenticationManagerScript.Instance.FormErrors.Remove(gameObject);
            }
        }
    }
}
