﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExtraValidationCheckBox : MonoBehaviour
{
    private Toggle ToggleComp;

    // Start is called before the first frame update
    void Start()
    {
        ToggleComp = GetComponent<Toggle>();
    }

    public void DetectToggleIsSelected()
    {
        if (!ToggleComp.isOn)
        {
            if (!AuthenticationManagerScript.Instance.FormErrors.Contains(gameObject))
            {
                AuthenticationManagerScript.Instance.FormErrors.Add(gameObject);
            }
        }
        else
        {
            if (AuthenticationManagerScript.Instance.FormErrors.Contains(gameObject))
            {
                AuthenticationManagerScript.Instance.FormErrors.Remove(gameObject);
            }
        }
    }
}
