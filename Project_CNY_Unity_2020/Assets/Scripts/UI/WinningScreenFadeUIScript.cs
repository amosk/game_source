using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinningScreenFadeUIScript : MonoBehaviour
{
    Animator AnimatorComp;

    // Animation States
    const string SCREEN_WINNINGSCREENFADE = "WinningScreenfade";

    int SCREEN_WINNINGSCREENFADE_ID;

    private int CurrentStateID;

    private void Awake()
    {
        AnimatorComp = GetComponent<Animator>();

        SCREEN_WINNINGSCREENFADE_ID = Animator.StringToHash(SCREEN_WINNINGSCREENFADE);
    }

    public void FadeInWinningScreen()
    {
        ChangeAnimationState(SCREEN_WINNINGSCREENFADE_ID);
    }

    protected void ChangeAnimationState(int _newStateID)
    {
        if (CurrentStateID == _newStateID) return;

        AnimatorComp.Play(_newStateID);

        CurrentStateID = _newStateID;
    }
}
