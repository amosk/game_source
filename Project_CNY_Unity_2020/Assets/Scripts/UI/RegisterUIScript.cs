﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RegisterUIScript : MonoBehaviour
{
    public Button RegisterButton;
    public InputField IF_FirstName;
    public InputField IF_LastName;
    public InputField IF_Password;
    public InputField IF_ConfirmedPassword;
    public InputField IF_Email;
    public InputField IF_Mobile;
    public Dropdown DD_Prize;
    public InputField IF_PrizeReason;
    public Toggle TO_TC;
    public Text RegisterUserMessage;

    // Start is called before the first frame update
    void Start()
    {
        RegisterButton.onClick.AddListener(delegate { OnRegisterButtonClick(); });
    }

    void OnRegisterButtonClick()
    {
        if (AuthenticationManagerScript.Instance.FormErrors.Count > 1)
        {
            RegisterUserMessage.text = "Invalid Information.";
            RegisterUserMessage.color = Color.red;
        }
        else if (AuthenticationManagerScript.Instance.FormErrors.Count == 1)
        {
            if(!AuthenticationManagerScript.Instance.FormErrors.Contains(TO_TC.gameObject))
            {
                RegisterUserMessage.text = "Invalid Information.";
            } 
            else
            {
                RegisterUserMessage.text = "You didn't agree to the Terms and Conditions.";
            }
            RegisterUserMessage.color = Color.red;
        }
        else
        {
            AuthenticationManagerScript.Instance.RegisterUser(IF_FirstName.text, IF_LastName.text, IF_Password.text, IF_ConfirmedPassword.text, 
                IF_Email.text, IF_Mobile.text, DD_Prize.value, IF_PrizeReason.text);
        }
    }

    public void UIConstruct()
    {
        GetComponent<ChangeInputScript>().AssignUINavigation();
        RegisterUserMessage.text = "";
        RegisterUserMessage.color = Color.white;
        IF_FirstName.text = "";
        IF_LastName.text = "";
        IF_Password.text = "";
        IF_ConfirmedPassword.text = "";
        IF_Email.text = "";
        IF_Mobile.text = "";
        DD_Prize.value = 0;
        TO_TC.isOn = false;
        IF_PrizeReason.text = "";

        IF_FirstName.gameObject.transform.Find("T_FirstName_Error").gameObject.GetComponent<Text>().text = "";
        IF_LastName.gameObject.transform.Find("T_LastName_Error").gameObject.GetComponent<Text>().text = "";
        IF_Password.gameObject.transform.Find("T_Password_Error").gameObject.GetComponent<Text>().text = "";
        IF_ConfirmedPassword.gameObject.transform.Find("T_ConfirmPassword_Error").gameObject.GetComponent<Text>().text = "";
        IF_Email.gameObject.transform.Find("T_Email_Error").gameObject.GetComponent<Text>().text = "";
        IF_Mobile.gameObject.transform.Find("T_Mobile_Error").gameObject.GetComponent<Text>().text = "";
        DD_Prize.gameObject.transform.Find("T_PCO_Error").gameObject.GetComponent<Text>().text = "";
        IF_PrizeReason.gameObject.transform.Find("T_Reason_Error").gameObject.GetComponent<Text>().text = "";
    }
}
