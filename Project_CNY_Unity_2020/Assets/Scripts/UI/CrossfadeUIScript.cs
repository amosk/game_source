﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossfadeUIScript : MonoBehaviour
{
    Animator AnimatorComp;

    // Animation States
    const string SCREEN_CROSSFADE = "Crossfade";

    int SCREEN_CROSSFADE_ID;

    private int CurrentStateID;

    private void Awake()
    {
        AnimatorComp = GetComponent<Animator>();

        SCREEN_CROSSFADE_ID = Animator.StringToHash(SCREEN_CROSSFADE);
    }

    public void TriggerTransition()
    {
        ChangeAnimationState(SCREEN_CROSSFADE_ID);
    }

    protected void ChangeAnimationState(int _newStateID)
    {
        if (CurrentStateID == _newStateID) return;

        AnimatorComp.Play(_newStateID);

        CurrentStateID = _newStateID;
    }
}
