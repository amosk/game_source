﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoginUIScript : MonoBehaviour
{
    public Button LoginButton;
    public InputField IF_Email;
    public InputField IF_Password;
    public Text LoginMessage;

    public void OnLoginButtonClick()
    {
        StartCoroutine(LoginButtonPress(0.01f));
    }

    private IEnumerator LoginButtonPress(float WaitTime)
    {
        yield return new WaitForSeconds(WaitTime);

        if (AuthenticationManagerScript.Instance.FormErrors.Count > 0)
        {
            LoginMessage.text = "Invalid Information.";
            LoginMessage.color = Color.red;
        }
        else
        {
            AuthenticationManagerScript.Instance.LoginUser(IF_Email.text, IF_Password.text);
        }
    }

    public void UIConstruct()
    {
        GetComponent<ChangeInputScript>().AssignUINavigation();
        LoginMessage.text = "";
        LoginMessage.color = Color.white;
        IF_Email.text = "";
        IF_Password.text = "";

        IF_Email.gameObject.transform.Find("T_Email_Error").gameObject.GetComponent<Text>().text = "";
        IF_Password.gameObject.transform.Find("T_Password_Error").gameObject.GetComponent<Text>().text = "";
    }
}
