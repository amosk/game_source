﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class ExtraValidationInputField : MonoBehaviour
{
    public Text ErrorMessage;
    private InputField InputFieldComp;

    public int StringLimit;

    public bool bOnlyInt;
    public bool bValidateEmail;

    public InputField PasswordInputField;
    public bool bConfirmedPassword;
    public Text PasswordErrorMessage;

    private void Start()
    {
        InputFieldComp = GetComponent<InputField>();
    }
    public void ValidateInputField()
    {
        if (InputFieldComp.text.Length > StringLimit)
        {
            ErrorMessage.text = "Input field exceeded limit of: " + StringLimit;

            if (!AuthenticationManagerScript.Instance.FormErrors.Contains(gameObject))
            {
                AuthenticationManagerScript.Instance.FormErrors.Add(gameObject);
            }
        }
        else if (bOnlyInt && !ValidateNumericNumber(InputFieldComp.text) && InputFieldComp.text.Length > 0)
        {
            ErrorMessage.text = "Must be only digit numbers.";

            if (!AuthenticationManagerScript.Instance.FormErrors.Contains(gameObject))
            {
                AuthenticationManagerScript.Instance.FormErrors.Add(gameObject);
            }
        }
        else if (bValidateEmail && !ValidateEmail(InputFieldComp.text) && InputFieldComp.text.Length > 0)
        {
            ErrorMessage.text = "Invalid email format";

            if (!AuthenticationManagerScript.Instance.FormErrors.Contains(gameObject))
            {
                AuthenticationManagerScript.Instance.FormErrors.Add(gameObject);
            }
        }
        else if (bConfirmedPassword && !string.Equals(PasswordInputField.text, InputFieldComp.text) && InputFieldComp.text.Length > 0)
        {
            PasswordErrorMessage.text = "Password mismatched";

            if (!AuthenticationManagerScript.Instance.FormErrors.Contains(gameObject))
            {
                AuthenticationManagerScript.Instance.FormErrors.Add(gameObject);
            }
        }
        else
        {
            ErrorMessage.text = "";

            if (AuthenticationManagerScript.Instance.FormErrors.Contains(gameObject))
            {
                AuthenticationManagerScript.Instance.FormErrors.Remove(gameObject);
            }

            if(PasswordInputField)
            {
                if (AuthenticationManagerScript.Instance.FormErrors.Contains(PasswordInputField.gameObject))
                {
                    AuthenticationManagerScript.Instance.FormErrors.Remove(PasswordInputField.gameObject);
                }
            }
        }
    }

    public void DetectInputFieldIsFill()
    {
        if (InputFieldComp.text.Length <= 0)
        {
            ErrorMessage.text = "Field is required";

            if (!AuthenticationManagerScript.Instance.FormErrors.Contains(gameObject))
            {
                AuthenticationManagerScript.Instance.FormErrors.Add(gameObject);
            }
        }
        else
        {
            if(!AuthenticationManagerScript.Instance.FormErrors.Contains(gameObject))
                ErrorMessage.text = "";
        }
    }

    bool ValidateNumericNumber(string _input)
    {
        decimal result;
        if (decimal.TryParse(_input, out result))
        {
            return true;
        }

        return false;
    }
    bool ValidateEmail(string _email)
    {
        try
        {
            var address = new System.Net.Mail.MailAddress(_email);
            return address.Address == _email;
        }
        catch
        {
            return false;
        }
    }
}
