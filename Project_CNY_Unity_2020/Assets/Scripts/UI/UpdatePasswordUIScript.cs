﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdatePasswordUIScript : MonoBehaviour
{
    public Button UpdatePasswordButton;
    public InputField IF_Email;
    public InputField IF_Mobile;
    public InputField IF_NewPassword;
    public InputField IF_ConfirmedPassword;
    public Text UpdatePasswordMessage;

    // Start is called before the first frame update
    void Start()
    {
        UpdatePasswordButton.onClick.AddListener(delegate { OnUpdatePasswordButtonClick(); });
    }

    void OnUpdatePasswordButtonClick()
    {
        if (AuthenticationManagerScript.Instance.FormErrors.Count > 0)
        {
            UpdatePasswordMessage.text = "Invalid Information.";
            UpdatePasswordMessage.color = Color.red;
        }
        else
        {
            AuthenticationManagerScript.Instance.UpdateUserPassword(IF_Email.text, IF_Mobile.text, IF_NewPassword.text, IF_ConfirmedPassword.text);
        }
    }

    public void UIConstruct()
    {
        GetComponent<ChangeInputScript>().AssignUINavigation();
        UpdatePasswordMessage.text = "";
        UpdatePasswordMessage.color = Color.white;
        IF_Email.text = "";
        IF_Mobile.text = "";
        IF_NewPassword.text = "";
        IF_ConfirmedPassword.text = "";

        IF_Email.gameObject.transform.Find("T_Email_Error").gameObject.GetComponent<Text>().text = "";
        IF_Mobile.gameObject.transform.Find("T_Mobile_Error").gameObject.GetComponent<Text>().text = "";
        IF_NewPassword.gameObject.transform.Find("T_NewPassword_Error").gameObject.GetComponent<Text>().text = "";
        IF_ConfirmedPassword.gameObject.transform.Find("T_ConfirmPassword_Error").gameObject.GetComponent<Text>().text = "";
    }
}
