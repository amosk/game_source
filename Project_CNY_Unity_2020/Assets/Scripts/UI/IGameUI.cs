using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGameUI 
{
    void SetInputFieldUninteractable();

    void SetInputFieldInteractable();
}
