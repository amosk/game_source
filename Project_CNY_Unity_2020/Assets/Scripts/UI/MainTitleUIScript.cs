using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainTitleUIScript : MonoBehaviour
{
    public Button LoginButton;
    public Sprite LoginButtonImage;
    public Sprite PlayButtonImage;
    public Button MuteButton;
    public Sprite MuteButtonImage;
    public Sprite UnmuteButtonImage;

    private LoginUIScript LoginUIScript;

    private void Start()
    {
        if(GameManagerScript.Instance.MusicVolume == 0.0f || GameManagerScript.Instance.SFXVolume == 0.0f)
        {
            MuteButton.GetComponent<Image>().sprite = MuteButtonImage;
        } 
        else
        {
            MuteButton.GetComponent<Image>().sprite = UnmuteButtonImage;
        }

        if(GameManagerScript.Instance.UserID != -1)
        {
            LoginButton.GetComponent<Image>().sprite = PlayButtonImage;
        }
        else
        {
            LoginButton.GetComponent<Image>().sprite = LoginButtonImage;
        }

        LoginUIScript = AuthenticationManagerScript.Instance.LoginScreen.GetComponent<LoginUIScript>();
    }
    public void OnPlayAsGuestButtonClick()
    {
        AuthenticationManagerScript.Instance.PlayAsGuest();
    }

    public void OnMuteButtonClick()
    {
        if(AudioManagerScript.Instance.IsMute())
        {
            AudioManagerScript.Instance.AdjustVolume(0.7f, 0.5f);
            MuteButton.GetComponent<Image>().sprite = UnmuteButtonImage;
        }
        else
        {
            AudioManagerScript.Instance.AdjustVolume(0.0f, 0.0f);
            MuteButton.GetComponent<Image>().sprite = MuteButtonImage;
        }
    }

    public void OnLoginButtonClick()
    {
        if(GameManagerScript.Instance.UserID != -1)
        {
            StartCoroutine(AuthenticationManagerScript.Instance.InitializeGame(0.01f));
        }
        else
        {
            AuthenticationManagerScript.Instance.LoginScreen.gameObject.SetActive(true);
            LoginUIScript.UIConstruct();
        }
    }
}
