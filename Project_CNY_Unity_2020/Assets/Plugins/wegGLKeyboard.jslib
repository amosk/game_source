﻿mergeInto(LibraryManager.library, {
    IsMobileBrowser: function () {
        if (/iPhone|iPad|iPod|Android/i.test(navigator.userAgent))
        {
            return true;
        }
        else
        {
            return false;
        }
    },
    focusHandleAction: function (_popupmessage, _name, _strlimit, _bOnlyInt, _bValidateEmail, _bValidateMobileNumber) {
        if (/iPhone|iPad|iPod|Android/i.test(navigator.userAgent)) {
            BootboxPrompt(Pointer_stringify(_popupmessage), Pointer_stringify(_name), _strlimit, Number(_bOnlyInt), Number(_bValidateEmail), Number(_bValidateMobileNumber));
        }
    },
    openLink: function (_url) {
        url = Pointer_stringify(_url);
        window.open(url, '_blank');
    }
});