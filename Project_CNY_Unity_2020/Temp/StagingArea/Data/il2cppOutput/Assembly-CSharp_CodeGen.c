﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 AudioManagerScript AudioManagerScript::get_Instance()
extern void AudioManagerScript_get_Instance_mF84C38805AC2140B48B85F3F771685428EA80C29 (void);
// 0x00000002 System.Void AudioManagerScript::Awake()
extern void AudioManagerScript_Awake_m71D69FF1C019AED9716FA8C9FB672A1FE55D46A4 (void);
// 0x00000003 System.Void AudioManagerScript::Start()
extern void AudioManagerScript_Start_mCA8DAB4C8058C91527AA90844292DC065CEF1A63 (void);
// 0x00000004 System.Void AudioManagerScript::StartGame()
extern void AudioManagerScript_StartGame_m0B09F28E063D9E77698C8D3FB39C356089DAC3DD (void);
// 0x00000005 System.Void AudioManagerScript::PlayMainBGM()
extern void AudioManagerScript_PlayMainBGM_m0FEC40E0AAD4DC6269FCC37D04A073341C3B302D (void);
// 0x00000006 System.Void AudioManagerScript::PlayPowerUpBGM()
extern void AudioManagerScript_PlayPowerUpBGM_mDFCDB27A659A55D26186C8B21A8799E5E10D9814 (void);
// 0x00000007 System.Void AudioManagerScript::StopBGM()
extern void AudioManagerScript_StopBGM_mA79AEEEC27D6A3D3550070817EA7522C331B1CDE (void);
// 0x00000008 System.Void AudioManagerScript::PlaySFX(UnityEngine.AudioClip)
extern void AudioManagerScript_PlaySFX_m8F546B0F6D46B7F85374EC1CCB7E326FC53DEA0F (void);
// 0x00000009 System.Void AudioManagerScript::AdjustVolume(System.Single,System.Single)
extern void AudioManagerScript_AdjustVolume_m13990DFC8DA97742FA2E018E78E589E38A61A728 (void);
// 0x0000000A System.Boolean AudioManagerScript::IsMute()
extern void AudioManagerScript_IsMute_mE0EBFD6AC34F617593F0C6413EF469136FED19DF (void);
// 0x0000000B System.Void AudioManagerScript::.ctor()
extern void AudioManagerScript__ctor_mB7EBDB54102B4DD2ED9186C4F2198DFEA9B1ADDF (void);
// 0x0000000C System.Void RegisterUserData::.ctor()
extern void RegisterUserData__ctor_mC80344269847224DA5072B1EB17627683D189389 (void);
// 0x0000000D System.Void LoginUserData::.ctor()
extern void LoginUserData__ctor_mAD4C8DA646071FFC216DE6EFF402AA77DF95F64B (void);
// 0x0000000E System.Void UpdatePasswordData::.ctor()
extern void UpdatePasswordData__ctor_m497FBE33C880730D6DA5872C858E6EEBA4254714 (void);
// 0x0000000F AuthenticationManagerScript AuthenticationManagerScript::get_Instance()
extern void AuthenticationManagerScript_get_Instance_m143D98B4FF8D71EC28A6BAA3D528768AB50C5C30 (void);
// 0x00000010 System.Void AuthenticationManagerScript::Awake()
extern void AuthenticationManagerScript_Awake_mF02810ED29BBA711488E146BDB69CA9B667CC00F (void);
// 0x00000011 System.Void AuthenticationManagerScript::Start()
extern void AuthenticationManagerScript_Start_m4F950D7A928F5E82340D08BA8C00ECBE718D8A64 (void);
// 0x00000012 System.Void AuthenticationManagerScript::PlayAsGuest()
extern void AuthenticationManagerScript_PlayAsGuest_m310BB1401C420E496E6EC4B77D272E46D8FDE2FF (void);
// 0x00000013 System.Void AuthenticationManagerScript::LoginUser(System.String,System.String)
extern void AuthenticationManagerScript_LoginUser_mAB663B37FC1C49389B69928B33F77FFFA0FB1DC2 (void);
// 0x00000014 System.Void AuthenticationManagerScript::RegisterUser(System.String,System.String,System.String,System.String,System.String,System.String,System.Int32,System.String)
extern void AuthenticationManagerScript_RegisterUser_m1DEF60DE3B6256702FD6DEBE13F4FA3795850BD7 (void);
// 0x00000015 System.Void AuthenticationManagerScript::UpdateUserPassword(System.String,System.String,System.String,System.String)
extern void AuthenticationManagerScript_UpdateUserPassword_mAB8FAB94F898EC0945BF753FAAA6F74833B1CFD1 (void);
// 0x00000016 System.Collections.IEnumerator AuthenticationManagerScript::PostLogin(System.String)
extern void AuthenticationManagerScript_PostLogin_mEA9EBA2DB1BC45CD94E4998FBE8499C8D62D5ED3 (void);
// 0x00000017 System.Collections.IEnumerator AuthenticationManagerScript::InitializeGame(System.Single)
extern void AuthenticationManagerScript_InitializeGame_m0680BC7A2C483C7E51CE0CC72EAD2C7C04DEF908 (void);
// 0x00000018 System.Collections.IEnumerator AuthenticationManagerScript::PostRegister(System.String)
extern void AuthenticationManagerScript_PostRegister_m87C6C4318FB74E2BC0464AAD636B548C9C5979CB (void);
// 0x00000019 System.Collections.IEnumerator AuthenticationManagerScript::PostUpdatePassword(System.String)
extern void AuthenticationManagerScript_PostUpdatePassword_mDE1B1EE42E88A7BA7B5307D35BAC7F95F35458F6 (void);
// 0x0000001A System.Void AuthenticationManagerScript::.ctor()
extern void AuthenticationManagerScript__ctor_m1E1C589C6EDDACCD3DBE10CBB0C894905DFDCF6C (void);
// 0x0000001B System.Void AuthenticationManagerScript/<PostLogin>d__27::.ctor(System.Int32)
extern void U3CPostLoginU3Ed__27__ctor_mEB1BEFE21BAA925B96785C30234697D008F64011 (void);
// 0x0000001C System.Void AuthenticationManagerScript/<PostLogin>d__27::System.IDisposable.Dispose()
extern void U3CPostLoginU3Ed__27_System_IDisposable_Dispose_m2FE2383282696EDB935EEDDBD9834C5AFF8A3DAE (void);
// 0x0000001D System.Boolean AuthenticationManagerScript/<PostLogin>d__27::MoveNext()
extern void U3CPostLoginU3Ed__27_MoveNext_mCC040C0D09EF83758E8D4CCC54FC3F93A27805E9 (void);
// 0x0000001E System.Object AuthenticationManagerScript/<PostLogin>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPostLoginU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m58E7651E66624F084279ABF17D4A246276428CFE (void);
// 0x0000001F System.Void AuthenticationManagerScript/<PostLogin>d__27::System.Collections.IEnumerator.Reset()
extern void U3CPostLoginU3Ed__27_System_Collections_IEnumerator_Reset_mADA6D4E611B36BAA2BA3CF7AFA0C6F9B1BAB0B7F (void);
// 0x00000020 System.Object AuthenticationManagerScript/<PostLogin>d__27::System.Collections.IEnumerator.get_Current()
extern void U3CPostLoginU3Ed__27_System_Collections_IEnumerator_get_Current_m29813583295C7B6D170E9FCFA005D06633BD3F50 (void);
// 0x00000021 System.Void AuthenticationManagerScript/<InitializeGame>d__28::.ctor(System.Int32)
extern void U3CInitializeGameU3Ed__28__ctor_m6483DF96F3B8D5010E57BACE7379CD97F6552A8D (void);
// 0x00000022 System.Void AuthenticationManagerScript/<InitializeGame>d__28::System.IDisposable.Dispose()
extern void U3CInitializeGameU3Ed__28_System_IDisposable_Dispose_m66B0054BDB30119B1B18604BDCB93C623E19D178 (void);
// 0x00000023 System.Boolean AuthenticationManagerScript/<InitializeGame>d__28::MoveNext()
extern void U3CInitializeGameU3Ed__28_MoveNext_mD19D5D71516CAF467A23AF25B71A39728434465E (void);
// 0x00000024 System.Object AuthenticationManagerScript/<InitializeGame>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInitializeGameU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7BEC21D79B0B1519085C033FBBE4D4AF59E5B284 (void);
// 0x00000025 System.Void AuthenticationManagerScript/<InitializeGame>d__28::System.Collections.IEnumerator.Reset()
extern void U3CInitializeGameU3Ed__28_System_Collections_IEnumerator_Reset_m6C14396FDB21283CF4BF45B8EC4EF72848F056D8 (void);
// 0x00000026 System.Object AuthenticationManagerScript/<InitializeGame>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CInitializeGameU3Ed__28_System_Collections_IEnumerator_get_Current_m9FEE2D712034174CD69CE35841120F2210301FED (void);
// 0x00000027 System.Void AuthenticationManagerScript/<PostRegister>d__29::.ctor(System.Int32)
extern void U3CPostRegisterU3Ed__29__ctor_m931F4CA283C7F730F020CB30ADAEB0CD2ADD0F77 (void);
// 0x00000028 System.Void AuthenticationManagerScript/<PostRegister>d__29::System.IDisposable.Dispose()
extern void U3CPostRegisterU3Ed__29_System_IDisposable_Dispose_mB44158C17BA7B6C83846257E355EAAF2E6454C88 (void);
// 0x00000029 System.Boolean AuthenticationManagerScript/<PostRegister>d__29::MoveNext()
extern void U3CPostRegisterU3Ed__29_MoveNext_mAD2158B0552AB32BED58502EB21EC6D253AD201D (void);
// 0x0000002A System.Object AuthenticationManagerScript/<PostRegister>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPostRegisterU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6449593DFCD7207A0D1D366835D70272ED696DCE (void);
// 0x0000002B System.Void AuthenticationManagerScript/<PostRegister>d__29::System.Collections.IEnumerator.Reset()
extern void U3CPostRegisterU3Ed__29_System_Collections_IEnumerator_Reset_m25DC991E98C158D77FB873D7478F34797AF3B253 (void);
// 0x0000002C System.Object AuthenticationManagerScript/<PostRegister>d__29::System.Collections.IEnumerator.get_Current()
extern void U3CPostRegisterU3Ed__29_System_Collections_IEnumerator_get_Current_mCB8DBA443AC25037298A19CBB31BE910ED38F63A (void);
// 0x0000002D System.Void AuthenticationManagerScript/<PostUpdatePassword>d__30::.ctor(System.Int32)
extern void U3CPostUpdatePasswordU3Ed__30__ctor_m909F6D547362802555788CF69D5907C4CA4BA188 (void);
// 0x0000002E System.Void AuthenticationManagerScript/<PostUpdatePassword>d__30::System.IDisposable.Dispose()
extern void U3CPostUpdatePasswordU3Ed__30_System_IDisposable_Dispose_m89831D42936AAF1DB69688E54ED481E96C1D0844 (void);
// 0x0000002F System.Boolean AuthenticationManagerScript/<PostUpdatePassword>d__30::MoveNext()
extern void U3CPostUpdatePasswordU3Ed__30_MoveNext_m41ABA5865833A31CFB7DBE8C479D0DCFF878AF22 (void);
// 0x00000030 System.Object AuthenticationManagerScript/<PostUpdatePassword>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPostUpdatePasswordU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0CB3D36D4861D2E2547B0732E6E75A49BF0D9518 (void);
// 0x00000031 System.Void AuthenticationManagerScript/<PostUpdatePassword>d__30::System.Collections.IEnumerator.Reset()
extern void U3CPostUpdatePasswordU3Ed__30_System_Collections_IEnumerator_Reset_m39D8102580AB48A2D53BEE3662B43B7035864C14 (void);
// 0x00000032 System.Object AuthenticationManagerScript/<PostUpdatePassword>d__30::System.Collections.IEnumerator.get_Current()
extern void U3CPostUpdatePasswordU3Ed__30_System_Collections_IEnumerator_get_Current_m78D4C232B44854225B661FF119D83E1F104DC408 (void);
// 0x00000033 System.Void BoundaryScript::Start()
extern void BoundaryScript_Start_mFEB6B58DAFAAF3C7BB2D6F86DE67DCBA9ABC4C3B (void);
// 0x00000034 System.Void BoundaryScript::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void BoundaryScript_OnTriggerEnter2D_mFD2D0E911CB71C6EFCADD7507CBD3EE883FE740B (void);
// 0x00000035 System.Void BoundaryScript::OnTriggerExit2D(UnityEngine.Collider2D)
extern void BoundaryScript_OnTriggerExit2D_m19C6AD58F61A4714DF8572036F130AF29F9ED15A (void);
// 0x00000036 System.Void BoundaryScript::.ctor()
extern void BoundaryScript__ctor_mC7189D2C91CDB69B88B16FC996732B7BEAFFB606 (void);
// 0x00000037 System.Void CameraScript::Awake()
extern void CameraScript_Awake_m421586ECC5F1A5029EC370048C8FDC492BA13C65 (void);
// 0x00000038 System.Void CameraScript::Update()
extern void CameraScript_Update_mFD3343EC9B5F59B8D44E7151FA49C7CE88B1936A (void);
// 0x00000039 System.Void CameraScript::.ctor()
extern void CameraScript__ctor_m2F555CA91AB185743DD59C5C093A7F6EE339B863 (void);
// 0x0000003A System.Void ChangeInputScript::Start()
extern void ChangeInputScript_Start_mE15210E853535EDEE35639D49D597D622DD9B4CB (void);
// 0x0000003B System.Void ChangeInputScript::Update()
extern void ChangeInputScript_Update_mC9622E774A1F637A01ED9E6B67618EC1206DC182 (void);
// 0x0000003C System.Void ChangeInputScript::AssignUINavigation()
extern void ChangeInputScript_AssignUINavigation_m6A1F251BE53B3DEB82AD1653E36AA38530B95C84 (void);
// 0x0000003D System.Void ChangeInputScript::.ctor()
extern void ChangeInputScript__ctor_mE58EFB8844CFE341F0086A05D47F955A7D4A7A1C (void);
// 0x0000003E GameManagerScript GameManagerScript::get_Instance()
extern void GameManagerScript_get_Instance_mF64FDD782CB37098D0DB861D1AC3CBE7FCD3115D (void);
// 0x0000003F System.Void GameManagerScript::Awake()
extern void GameManagerScript_Awake_m8296FABBF41E390DBF0BB4071457A3153854FE01 (void);
// 0x00000040 System.Void GameManagerScript::StartGame()
extern void GameManagerScript_StartGame_mA573B61AA3C1CCC72F3BE73F21C03E94DD9C7805 (void);
// 0x00000041 System.Void GameManagerScript::EndGame()
extern void GameManagerScript_EndGame_m9F15B54656771FCCDE22769527C1F47DCF427D57 (void);
// 0x00000042 System.Void GameManagerScript::ProceedToNextLevel()
extern void GameManagerScript_ProceedToNextLevel_m502AEFA619361B552F5DE7013AA15EBDC8ADBD7A (void);
// 0x00000043 System.Void GameManagerScript::.ctor()
extern void GameManagerScript__ctor_m95960A513907B211C35582552310D03099AB2339 (void);
// 0x00000044 System.Collections.Generic.List`1<T> FindInterfaces::Find()
// 0x00000045 System.Void GroundScript::Start()
extern void GroundScript_Start_mD827FC506EFE4C3BF9A9863368C45393AFA2AAAF (void);
// 0x00000046 System.Void GroundScript::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void GroundScript_OnTriggerEnter2D_mC88505F4A43FF1BB782E0EC2F72431AB80090AE4 (void);
// 0x00000047 System.Void GroundScript::.ctor()
extern void GroundScript__ctor_m417B8B73D039528B2CA5A35062BFAB97CEC26CF5 (void);
// 0x00000048 HTTPRequestHelper HTTPRequestHelper::get_Instance()
extern void HTTPRequestHelper_get_Instance_m7CE06EDA42A62C4E5FFBE8515102EA0E3990DE6B (void);
// 0x00000049 System.Boolean HTTPRequestHelper::IsMobileBrowser()
extern void HTTPRequestHelper_IsMobileBrowser_m0FC74B25FADCD8A41B3DDE22A22EFA3B2A7F15FD (void);
// 0x0000004A System.Void HTTPRequestHelper::openLink(System.String)
extern void HTTPRequestHelper_openLink_m3FC9E363AA28F6355374FE5700F3FDE2F3AFE3A9 (void);
// 0x0000004B System.Void HTTPRequestHelper::Awake()
extern void HTTPRequestHelper_Awake_m72A3550942537C24D98EDF8D8661E7A6F9243C4B (void);
// 0x0000004C UnityEngine.Networking.UnityWebRequest HTTPRequestHelper::GeneratePostCallRequest(System.String,System.String)
extern void HTTPRequestHelper_GeneratePostCallRequest_mD529C721402C43043FC1C713ECB3C4F6B85CB840 (void);
// 0x0000004D System.Boolean HTTPRequestHelper::CheckIfHTTPRunningOnMobile()
extern void HTTPRequestHelper_CheckIfHTTPRunningOnMobile_mD1EF5784E0C3554B68970CC52BD8A76004D31526 (void);
// 0x0000004E System.Void HTTPRequestHelper::OpenPage(System.String)
extern void HTTPRequestHelper_OpenPage_m0F8D16C67A2FD89C7E993E40B724667AF4C5BE88 (void);
// 0x0000004F System.Void HTTPRequestHelper::.ctor()
extern void HTTPRequestHelper__ctor_m0084D378615C4BC40B2B5BBC8D3FC1E4626685DB (void);
// 0x00000050 System.Void IGameLoop::StartGame()
// 0x00000051 System.Void KeyboardClass::focusHandleAction(System.String,System.String,System.Int32,System.Boolean,System.Boolean,System.Boolean)
extern void KeyboardClass_focusHandleAction_m4C2FEA3D18106774674F0CD51BE59E18CE3A4F5F (void);
// 0x00000052 System.Void KeyboardClass::Start()
extern void KeyboardClass_Start_mB54D10821CC81ABFF646CB111FE5AF2FA46DE503 (void);
// 0x00000053 System.Void KeyboardClass::ReceiveInputData(System.String)
extern void KeyboardClass_ReceiveInputData_mE43EC88640EDE0433E96ABD4DF739C0289E17AEF (void);
// 0x00000054 System.Void KeyboardClass::OnSelect(UnityEngine.EventSystems.BaseEventData)
extern void KeyboardClass_OnSelect_m0B5DBE9AD485B2B5F8CA106DE2DA885FCBE8FBA5 (void);
// 0x00000055 System.Void KeyboardClass::.ctor()
extern void KeyboardClass__ctor_m121B5DE9BF5D8774A8EE455719EFF155B13CF2F0 (void);
// 0x00000056 System.Void ScoreData::.ctor()
extern void ScoreData__ctor_mD8749C727CA2FFBC106EC087E2F5C9B8D94959C1 (void);
// 0x00000057 System.Void LeaderboardData::.ctor()
extern void LeaderboardData__ctor_m79824B3B65BE1603FEB490F271C2127D44FEAEFA (void);
// 0x00000058 LeaderboardManagerScript LeaderboardManagerScript::get_Instance()
extern void LeaderboardManagerScript_get_Instance_mA52F4A95BBD5C5570DFDBED8EEC0880CA89AFD92 (void);
// 0x00000059 System.Void LeaderboardManagerScript::Awake()
extern void LeaderboardManagerScript_Awake_mC1EC1D1900D13C9B3306E57626710377B7559F11 (void);
// 0x0000005A System.Void LeaderboardManagerScript::Start()
extern void LeaderboardManagerScript_Start_mE2426D550669DC9E564A5008CD902BD60474BC29 (void);
// 0x0000005B System.Void LeaderboardManagerScript::StartGame()
extern void LeaderboardManagerScript_StartGame_mDDD503DD642AB3708195CB17862B702CA000D5BB (void);
// 0x0000005C System.Void LeaderboardManagerScript::UpdateUserScoreToLeaderboard()
extern void LeaderboardManagerScript_UpdateUserScoreToLeaderboard_m49F3A9E1E4F73A02BA54C820042F45A1F134705F (void);
// 0x0000005D System.Collections.IEnumerator LeaderboardManagerScript::GetUserScore(System.Int32)
extern void LeaderboardManagerScript_GetUserScore_m8756E65C0A74F98E324B876FB5EB196A6ADC795A (void);
// 0x0000005E System.Collections.IEnumerator LeaderboardManagerScript::PostUserScore(System.Int32,System.String)
extern void LeaderboardManagerScript_PostUserScore_m2A579351E7CC3CFE5D7C2F2776852C8CC1AC9447 (void);
// 0x0000005F System.Collections.IEnumerator LeaderboardManagerScript::RefreshLeaderboard(System.Single)
extern void LeaderboardManagerScript_RefreshLeaderboard_m77306CDAE263AAD96AE6C3DCE3CD073A1171C1AE (void);
// 0x00000060 System.Collections.IEnumerator LeaderboardManagerScript::GetLeaderboardScore()
extern void LeaderboardManagerScript_GetLeaderboardScore_mAB14D2288DED2975614F0E520031FC65E44DFF6A (void);
// 0x00000061 System.Void LeaderboardManagerScript::.ctor()
extern void LeaderboardManagerScript__ctor_m9A98737963AB5886FFCC53561D044D6FD9BBBB01 (void);
// 0x00000062 System.Void LeaderboardManagerScript/<GetUserScore>d__12::.ctor(System.Int32)
extern void U3CGetUserScoreU3Ed__12__ctor_m6DF1C76F95FD6F8AF39A06037BED6343E305C2FA (void);
// 0x00000063 System.Void LeaderboardManagerScript/<GetUserScore>d__12::System.IDisposable.Dispose()
extern void U3CGetUserScoreU3Ed__12_System_IDisposable_Dispose_m0B2AEC18400147F12B383DAF905A66A3B8D3DC2A (void);
// 0x00000064 System.Boolean LeaderboardManagerScript/<GetUserScore>d__12::MoveNext()
extern void U3CGetUserScoreU3Ed__12_MoveNext_m9DD6297A941BBA0245201A32B612E0336631EB2F (void);
// 0x00000065 System.Object LeaderboardManagerScript/<GetUserScore>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetUserScoreU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1609C0434FBD0F2540E29692CA5145F44797CC82 (void);
// 0x00000066 System.Void LeaderboardManagerScript/<GetUserScore>d__12::System.Collections.IEnumerator.Reset()
extern void U3CGetUserScoreU3Ed__12_System_Collections_IEnumerator_Reset_m2B00C50A75F503B3E3765CAF36F43001ADF88352 (void);
// 0x00000067 System.Object LeaderboardManagerScript/<GetUserScore>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CGetUserScoreU3Ed__12_System_Collections_IEnumerator_get_Current_m6BBD7CCC844BAD6D354B395F87EF37D6263C3244 (void);
// 0x00000068 System.Void LeaderboardManagerScript/<PostUserScore>d__13::.ctor(System.Int32)
extern void U3CPostUserScoreU3Ed__13__ctor_m42C6A446F6AB7D73A5DA80DA9AAA9C08741DD4E4 (void);
// 0x00000069 System.Void LeaderboardManagerScript/<PostUserScore>d__13::System.IDisposable.Dispose()
extern void U3CPostUserScoreU3Ed__13_System_IDisposable_Dispose_m6791F873B65EA43765AD1F4B6DE6C2F442AF4A75 (void);
// 0x0000006A System.Boolean LeaderboardManagerScript/<PostUserScore>d__13::MoveNext()
extern void U3CPostUserScoreU3Ed__13_MoveNext_mC1B27DB7543A958120CD99D82134724686A60A67 (void);
// 0x0000006B System.Object LeaderboardManagerScript/<PostUserScore>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPostUserScoreU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF02B4C4EF83A6A968D93F20EFF5C4D49BDE7C856 (void);
// 0x0000006C System.Void LeaderboardManagerScript/<PostUserScore>d__13::System.Collections.IEnumerator.Reset()
extern void U3CPostUserScoreU3Ed__13_System_Collections_IEnumerator_Reset_m8C33BD5DDCF2DFB8C0F1322DE16D5BACE21434F1 (void);
// 0x0000006D System.Object LeaderboardManagerScript/<PostUserScore>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CPostUserScoreU3Ed__13_System_Collections_IEnumerator_get_Current_m76FD9530EA41DA3E24A815FAFB2A546BDA2E5398 (void);
// 0x0000006E System.Void LeaderboardManagerScript/<RefreshLeaderboard>d__14::.ctor(System.Int32)
extern void U3CRefreshLeaderboardU3Ed__14__ctor_m3BE38DCD3898B69384135BF8BF3BBECB37CF2BBB (void);
// 0x0000006F System.Void LeaderboardManagerScript/<RefreshLeaderboard>d__14::System.IDisposable.Dispose()
extern void U3CRefreshLeaderboardU3Ed__14_System_IDisposable_Dispose_m6DEB89FB0B3983C07F931D6AA157A597A74C4942 (void);
// 0x00000070 System.Boolean LeaderboardManagerScript/<RefreshLeaderboard>d__14::MoveNext()
extern void U3CRefreshLeaderboardU3Ed__14_MoveNext_m40BAFB71DE7800A0CB9FC97152B4FCF20390AB96 (void);
// 0x00000071 System.Object LeaderboardManagerScript/<RefreshLeaderboard>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRefreshLeaderboardU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1C0DA2AEA8CB3577F31398A832DD180B719B4D70 (void);
// 0x00000072 System.Void LeaderboardManagerScript/<RefreshLeaderboard>d__14::System.Collections.IEnumerator.Reset()
extern void U3CRefreshLeaderboardU3Ed__14_System_Collections_IEnumerator_Reset_mE8F2E463A16B08A0DE19F4F2C5E439A6BBA3F4C8 (void);
// 0x00000073 System.Object LeaderboardManagerScript/<RefreshLeaderboard>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CRefreshLeaderboardU3Ed__14_System_Collections_IEnumerator_get_Current_mD145646783752DD18CB8862C45B302D896861CFD (void);
// 0x00000074 System.Void LeaderboardManagerScript/<GetLeaderboardScore>d__15::.ctor(System.Int32)
extern void U3CGetLeaderboardScoreU3Ed__15__ctor_mF4EDA003AED4462A1EF6DDCE4424A4C2EF856FE6 (void);
// 0x00000075 System.Void LeaderboardManagerScript/<GetLeaderboardScore>d__15::System.IDisposable.Dispose()
extern void U3CGetLeaderboardScoreU3Ed__15_System_IDisposable_Dispose_mA8ABD6A836C443F686F9659B756BD47433198F5C (void);
// 0x00000076 System.Boolean LeaderboardManagerScript/<GetLeaderboardScore>d__15::MoveNext()
extern void U3CGetLeaderboardScoreU3Ed__15_MoveNext_m72C5829E2163DE1617E0174E04FC40FC0717564A (void);
// 0x00000077 System.Object LeaderboardManagerScript/<GetLeaderboardScore>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetLeaderboardScoreU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m668B067B6B3BF7507722752F90FD5DEF954FFB99 (void);
// 0x00000078 System.Void LeaderboardManagerScript/<GetLeaderboardScore>d__15::System.Collections.IEnumerator.Reset()
extern void U3CGetLeaderboardScoreU3Ed__15_System_Collections_IEnumerator_Reset_m450981936121B358A075B602BBA9DB04F71E9548 (void);
// 0x00000079 System.Object LeaderboardManagerScript/<GetLeaderboardScore>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CGetLeaderboardScoreU3Ed__15_System_Collections_IEnumerator_get_Current_mF42DAE75A8866E8A9673687025E5FCFCD5ED5D1C (void);
// 0x0000007A System.Void LevelTransitionManagerScript::Start()
extern void LevelTransitionManagerScript_Start_m47636205F4023F58DD08D974A21EB9DB5FC0E541 (void);
// 0x0000007B System.Void LevelTransitionManagerScript::Update()
extern void LevelTransitionManagerScript_Update_m31273BC73DE49D2BD5E6DED43BEEBE8C57A3ECCB (void);
// 0x0000007C System.Void LevelTransitionManagerScript::.ctor()
extern void LevelTransitionManagerScript__ctor_m63749269D84570374F0E1EE90EF9899DA379C240 (void);
// 0x0000007D System.Void ObstacleColliderScript::Start()
extern void ObstacleColliderScript_Start_m5E40C70E550453F4E6CD99337A65B1AE7A0625BC (void);
// 0x0000007E System.Void ObstacleColliderScript::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void ObstacleColliderScript_OnTriggerEnter2D_m41B4E7BEB9C6E2628D080709A8A07F0B4DCCA724 (void);
// 0x0000007F System.Void ObstacleColliderScript::.ctor()
extern void ObstacleColliderScript__ctor_m111BDCACC86E8D08C565D2D5F7665E24262ED0BF (void);
// 0x00000080 System.Void ObstacleCollisionScript::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void ObstacleCollisionScript_OnTriggerEnter2D_m662BA9D129D5A26B200DEA36AE4C3AC5CAC4DEA9 (void);
// 0x00000081 System.Void ObstacleCollisionScript::.ctor()
extern void ObstacleCollisionScript__ctor_mD5D61F4C6DF291161540C5B44C8C4DB9DADCE667 (void);
// 0x00000082 System.Void ObstacleScript::Start()
extern void ObstacleScript_Start_mDD9F1FA599FC21E2B2E521A879887CBF519822F0 (void);
// 0x00000083 System.Void ObstacleScript::FixedUpdate()
extern void ObstacleScript_FixedUpdate_m043F033F600C6C981FFFF48B33762A867DEC1CF4 (void);
// 0x00000084 System.Void ObstacleScript::UpdateMovement()
extern void ObstacleScript_UpdateMovement_mEEC8D707174AD5EB80A8FF2FBB8BF6740CCEBB0D (void);
// 0x00000085 System.Void ObstacleScript::ReconstructObstacle()
extern void ObstacleScript_ReconstructObstacle_m400581C4E57CD65A9045EB57DA8CB40AF94896E6 (void);
// 0x00000086 System.Void ObstacleScript::InitializeObstacle()
extern void ObstacleScript_InitializeObstacle_m2A19A7CE3DC0AEAA52554A38029295A34805A9CA (void);
// 0x00000087 System.Void ObstacleScript::.ctor()
extern void ObstacleScript__ctor_m7F8EE833BA7BB72CE13F43E858BA12E69C2A9929 (void);
// 0x00000088 System.Void ObstacleSpawner::Start()
extern void ObstacleSpawner_Start_mBD8A272A8EEAD54E0988D0AE0D7663548DA0C745 (void);
// 0x00000089 System.Void ObstacleSpawner::PrepopulateObstacles()
extern void ObstacleSpawner_PrepopulateObstacles_m35D1C3256AD8CF82564405DB03151FF9DCC8AB7E (void);
// 0x0000008A System.Void ObstacleSpawner::HideObstacles()
extern void ObstacleSpawner_HideObstacles_m71005575000323F882FB24AD810FB27448005251 (void);
// 0x0000008B System.Void ObstacleSpawner::RegenerateObstacles()
extern void ObstacleSpawner_RegenerateObstacles_mE93586B053854CDBD80D95AEFE703ADC06F3857E (void);
// 0x0000008C System.Void ObstacleSpawner::.ctor()
extern void ObstacleSpawner__ctor_m347D7FB8C7FAEB149F02F9A147EE7E51A28F57ED (void);
// 0x0000008D System.Void ParallaxScrollingScript::Start()
extern void ParallaxScrollingScript_Start_m196F4A10E8A333276A674CD6E977F17E7629F489 (void);
// 0x0000008E System.Void ParallaxScrollingScript::FixedUpdate()
extern void ParallaxScrollingScript_FixedUpdate_m28CCAEDF7C66F15788C0076D83EEE04EE16C96A9 (void);
// 0x0000008F System.Void ParallaxScrollingScript::.ctor()
extern void ParallaxScrollingScript__ctor_mBB5016E34515CC945DFD89E0480F48571982E080 (void);
// 0x00000090 System.Void PickupScript::Start()
extern void PickupScript_Start_m66E57B159A384FD98786299C3F6491839EE9DBA1 (void);
// 0x00000091 System.Void PickupScript::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void PickupScript_OnTriggerEnter2D_m9C9FAFC6A648981CB0FDBD94A2452A50E3B31517 (void);
// 0x00000092 System.Void PickupScript::.ctor()
extern void PickupScript__ctor_m6A7D241A4ED917F6125E78E9ABA819BEF1B28C08 (void);
// 0x00000093 System.Void Pickup_SpecialScript::Start()
extern void Pickup_SpecialScript_Start_mD1DE94500DDB9B1DD1FE09644976555AFBBB15CA (void);
// 0x00000094 System.Void Pickup_SpecialScript::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Pickup_SpecialScript_OnTriggerEnter2D_mF24197DE1F10CE0CDA59BD5DE03267DE8F711870 (void);
// 0x00000095 System.Void Pickup_SpecialScript::.ctor()
extern void Pickup_SpecialScript__ctor_mDC73D2E5F8DBAB3E42DF89790B6A252D13E4EB6B (void);
// 0x00000096 System.Void PlayerBehavior::Awake()
extern void PlayerBehavior_Awake_m903075302F0C67B356F133380EDA83FCB714A0C0 (void);
// 0x00000097 System.Void PlayerBehavior::Start()
extern void PlayerBehavior_Start_m0A1F3B0C6099E8205960D3E492BB506E6BAE8EF2 (void);
// 0x00000098 System.Void PlayerBehavior::FixedUpdate()
extern void PlayerBehavior_FixedUpdate_m534D89C6A5C09E3581800557EA5F859195F78967 (void);
// 0x00000099 System.Void PlayerBehavior::Update()
extern void PlayerBehavior_Update_m2D11A23661FBCC51809DA46B442F4424788BBD7A (void);
// 0x0000009A System.Void PlayerBehavior::StartGame()
extern void PlayerBehavior_StartGame_mD929DC0B436D6B9C6BBD129F7F2B20C4DB4A8DD8 (void);
// 0x0000009B System.Void PlayerBehavior::HandleInput()
extern void PlayerBehavior_HandleInput_m28EA7C8587EB1B02362A1FE3BDADC8F8EA41095F (void);
// 0x0000009C System.Void PlayerBehavior::ToggleGravity(System.Boolean)
extern void PlayerBehavior_ToggleGravity_m3EABDDD05D1796FFD8E0668C502590B34FEE956B (void);
// 0x0000009D System.Boolean PlayerBehavior::IsGravityEnabled()
extern void PlayerBehavior_IsGravityEnabled_mCA6CA598F2307BCE41AB146DC47D32E3AD2AFE7B (void);
// 0x0000009E System.Void PlayerBehavior::ResetToFallingState()
extern void PlayerBehavior_ResetToFallingState_m612679EE3801CF7CF2D465DCAE2F725BF76A2360 (void);
// 0x0000009F System.Void PlayerBehavior::ResetJump()
extern void PlayerBehavior_ResetJump_mE473F24CDE47E582BA3DE49AEA232225323C5F1A (void);
// 0x000000A0 System.Void PlayerBehavior::ResetGravity()
extern void PlayerBehavior_ResetGravity_m01226E648248720782802B9A56A6E0169362794A (void);
// 0x000000A1 System.Void PlayerBehavior::UpdateGravityAcceleration()
extern void PlayerBehavior_UpdateGravityAcceleration_m50D75138B10F716B38ABB9488C7EF0DC7AA836F2 (void);
// 0x000000A2 System.Void PlayerBehavior::UpdateMovement()
extern void PlayerBehavior_UpdateMovement_m92C5873F8E5F005696C991A24D234464C21F93C7 (void);
// 0x000000A3 System.Void PlayerBehavior::RotateBasedOnVelocity()
extern void PlayerBehavior_RotateBasedOnVelocity_m006E0DE5DC5F7A5095AA177493327AD7B0D4F51A (void);
// 0x000000A4 System.Void PlayerBehavior::PerformJump()
extern void PlayerBehavior_PerformJump_mC677747719CDBAB5B724E4FBD86D2D148EB244ED (void);
// 0x000000A5 System.Void PlayerBehavior::PerformDeath()
extern void PlayerBehavior_PerformDeath_mB5027ED6FE256CDBADF206CFB4DCAF24359ED445 (void);
// 0x000000A6 System.Void PlayerBehavior::PerformWinningAction()
extern void PlayerBehavior_PerformWinningAction_m905B35B425962798AE958D812EE7965C711AD36C (void);
// 0x000000A7 System.Collections.IEnumerator PlayerBehavior::StopJumpTimer(System.Single)
extern void PlayerBehavior_StopJumpTimer_mAFE61DEDE57D69FF756BCE23E17F6434691F8638 (void);
// 0x000000A8 System.Collections.IEnumerator PlayerBehavior::ResetJumpTimer(System.Single)
extern void PlayerBehavior_ResetJumpTimer_m185F6ED4236EEB216DC85888A50C2560FF914D3B (void);
// 0x000000A9 System.Collections.IEnumerator PlayerBehavior::EnableGravityOverTime(System.Single)
extern void PlayerBehavior_EnableGravityOverTime_mD4996D3AB212898BB086648619213689676BDA50 (void);
// 0x000000AA System.Void PlayerBehavior::UpdateFlyMovement()
extern void PlayerBehavior_UpdateFlyMovement_m5A4CEDBDBD42E791828B0D0198B005C094E59E2C (void);
// 0x000000AB System.Void PlayerBehavior::ExecuteFlyAction()
extern void PlayerBehavior_ExecuteFlyAction_m8A406A7AADB16012307E844E2D5B583330707E45 (void);
// 0x000000AC System.Void PlayerBehavior::RevertFlyAction()
extern void PlayerBehavior_RevertFlyAction_m775962B05635245637D37B4F6101A0F57C598D33 (void);
// 0x000000AD System.Void PlayerBehavior::PlayPickUpAudio(UnityEngine.AudioClip)
extern void PlayerBehavior_PlayPickUpAudio_m6BB93858BC23E90FA1B4B1E1B7E4CEA939F27811 (void);
// 0x000000AE System.Void PlayerBehavior::ChangeAnimationState(System.Int32)
extern void PlayerBehavior_ChangeAnimationState_m5DE1C31FB8130F879C61DAC5EFA05BDE446DD590 (void);
// 0x000000AF System.Void PlayerBehavior::CrossFade_ChangeAnimationState(System.Int32)
extern void PlayerBehavior_CrossFade_ChangeAnimationState_mD446BF74F49BA225E968603D87C39FA01D598CCB (void);
// 0x000000B0 System.Void PlayerBehavior::SecondFormTransformation()
extern void PlayerBehavior_SecondFormTransformation_mDFA98DDFD7D3C39FAE702DE13CBBE0CF1DE4913B (void);
// 0x000000B1 System.Void PlayerBehavior::LockPlayerMovement(System.Boolean)
extern void PlayerBehavior_LockPlayerMovement_mF4A4DFB68D086793C64FC9E8E8F29DDB6BFEC930 (void);
// 0x000000B2 System.Boolean PlayerBehavior::GetGameStarted()
extern void PlayerBehavior_GetGameStarted_m249CB7DFE875EC4A30C4DA26ABBAE161CCA1A466 (void);
// 0x000000B3 System.Boolean PlayerBehavior::GetPlayerIsAlive()
extern void PlayerBehavior_GetPlayerIsAlive_mC4A534ABE96ACB41F805CF693507ECBFE69A721A (void);
// 0x000000B4 PlayerBehavior/EJumpState PlayerBehavior::GetCurrentJumpState()
extern void PlayerBehavior_GetCurrentJumpState_m59D718BBDAD07005C34891106361D5DA03280EBD (void);
// 0x000000B5 System.Collections.IEnumerator PlayerBehavior::AbleToProceedLeaderboardAfterTime(System.Single)
extern void PlayerBehavior_AbleToProceedLeaderboardAfterTime_mA4AA9F5151C07EF9F99AFE27CE2F590CF45FDBB5 (void);
// 0x000000B6 System.Collections.IEnumerator PlayerBehavior::OpenLeaderboardAfterTime(System.Single)
extern void PlayerBehavior_OpenLeaderboardAfterTime_mB2932C80FDD5D9E1299064A2E57DBCEE848E69C7 (void);
// 0x000000B7 System.Void PlayerBehavior::.ctor()
extern void PlayerBehavior__ctor_m85557340768EF11DD81ADAC93770FA77450CF872 (void);
// 0x000000B8 System.Void PlayerBehavior/<StopJumpTimer>d__74::.ctor(System.Int32)
extern void U3CStopJumpTimerU3Ed__74__ctor_m41E2698C1D7FFA19FB5ED48CDE29E7AAD7CD28B8 (void);
// 0x000000B9 System.Void PlayerBehavior/<StopJumpTimer>d__74::System.IDisposable.Dispose()
extern void U3CStopJumpTimerU3Ed__74_System_IDisposable_Dispose_m67AD08EC1B8C83720DAE14A4F7B934CF625176CC (void);
// 0x000000BA System.Boolean PlayerBehavior/<StopJumpTimer>d__74::MoveNext()
extern void U3CStopJumpTimerU3Ed__74_MoveNext_m064E17D5A12CEE924AB579BE877F265CA1DC8DE1 (void);
// 0x000000BB System.Object PlayerBehavior/<StopJumpTimer>d__74::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStopJumpTimerU3Ed__74_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m081E92295C4CEC6F83563A877AB1DCAF46DC8DA4 (void);
// 0x000000BC System.Void PlayerBehavior/<StopJumpTimer>d__74::System.Collections.IEnumerator.Reset()
extern void U3CStopJumpTimerU3Ed__74_System_Collections_IEnumerator_Reset_mD055DB840465665B871A22F89D90E605D7A97096 (void);
// 0x000000BD System.Object PlayerBehavior/<StopJumpTimer>d__74::System.Collections.IEnumerator.get_Current()
extern void U3CStopJumpTimerU3Ed__74_System_Collections_IEnumerator_get_Current_mD16CC96DB783F8037E654D94D49E6708C51EC914 (void);
// 0x000000BE System.Void PlayerBehavior/<ResetJumpTimer>d__75::.ctor(System.Int32)
extern void U3CResetJumpTimerU3Ed__75__ctor_mA27CAFE95460E965FDCCC3A1813716299237C8E4 (void);
// 0x000000BF System.Void PlayerBehavior/<ResetJumpTimer>d__75::System.IDisposable.Dispose()
extern void U3CResetJumpTimerU3Ed__75_System_IDisposable_Dispose_mD84D349A6DF9DD4BD173257B9AB9BAA218C0ADB4 (void);
// 0x000000C0 System.Boolean PlayerBehavior/<ResetJumpTimer>d__75::MoveNext()
extern void U3CResetJumpTimerU3Ed__75_MoveNext_m868F61272FCD457EC01D1798C1608D5F36AE527A (void);
// 0x000000C1 System.Object PlayerBehavior/<ResetJumpTimer>d__75::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CResetJumpTimerU3Ed__75_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7ECDA3855FBBAACCEE6F700AE41B3CDD2EDC486B (void);
// 0x000000C2 System.Void PlayerBehavior/<ResetJumpTimer>d__75::System.Collections.IEnumerator.Reset()
extern void U3CResetJumpTimerU3Ed__75_System_Collections_IEnumerator_Reset_m9061B620684FA7AA104DB6F03EF4F9EC3096D45F (void);
// 0x000000C3 System.Object PlayerBehavior/<ResetJumpTimer>d__75::System.Collections.IEnumerator.get_Current()
extern void U3CResetJumpTimerU3Ed__75_System_Collections_IEnumerator_get_Current_m978E4CC3F36731396CE35B9C179AE59FD37DC63E (void);
// 0x000000C4 System.Void PlayerBehavior/<EnableGravityOverTime>d__76::.ctor(System.Int32)
extern void U3CEnableGravityOverTimeU3Ed__76__ctor_m2B9DADB5BF8B2D0536C2D164F93FA9867BA796EA (void);
// 0x000000C5 System.Void PlayerBehavior/<EnableGravityOverTime>d__76::System.IDisposable.Dispose()
extern void U3CEnableGravityOverTimeU3Ed__76_System_IDisposable_Dispose_mA5FDF3F865DA8C4E0905CE6C6025BE69D4070354 (void);
// 0x000000C6 System.Boolean PlayerBehavior/<EnableGravityOverTime>d__76::MoveNext()
extern void U3CEnableGravityOverTimeU3Ed__76_MoveNext_mC6E55BDCAA37A0BDD95CBFEDF5611AA25A414658 (void);
// 0x000000C7 System.Object PlayerBehavior/<EnableGravityOverTime>d__76::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEnableGravityOverTimeU3Ed__76_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m92C0C9DB67D90D2F2C749287610A1B6226FA542C (void);
// 0x000000C8 System.Void PlayerBehavior/<EnableGravityOverTime>d__76::System.Collections.IEnumerator.Reset()
extern void U3CEnableGravityOverTimeU3Ed__76_System_Collections_IEnumerator_Reset_m3932804EE7C65C73D7E8CFC1826D47DCA8C837C2 (void);
// 0x000000C9 System.Object PlayerBehavior/<EnableGravityOverTime>d__76::System.Collections.IEnumerator.get_Current()
extern void U3CEnableGravityOverTimeU3Ed__76_System_Collections_IEnumerator_get_Current_mAEBADC72DF8EEEFCF94DDE16A776F7D5D8D88794 (void);
// 0x000000CA System.Void PlayerBehavior/<AbleToProceedLeaderboardAfterTime>d__88::.ctor(System.Int32)
extern void U3CAbleToProceedLeaderboardAfterTimeU3Ed__88__ctor_m40C062923AB126A9E0D45BB3AAA3F17D50DB2530 (void);
// 0x000000CB System.Void PlayerBehavior/<AbleToProceedLeaderboardAfterTime>d__88::System.IDisposable.Dispose()
extern void U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_System_IDisposable_Dispose_m373AEAE00494910A9D8D3D849DAA13C2B99048A9 (void);
// 0x000000CC System.Boolean PlayerBehavior/<AbleToProceedLeaderboardAfterTime>d__88::MoveNext()
extern void U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_MoveNext_m7DC5D8760237ECB2894D187F5810D8B3768B7E97 (void);
// 0x000000CD System.Object PlayerBehavior/<AbleToProceedLeaderboardAfterTime>d__88::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1066A678444ADE3726429AD33279DF0180B722D5 (void);
// 0x000000CE System.Void PlayerBehavior/<AbleToProceedLeaderboardAfterTime>d__88::System.Collections.IEnumerator.Reset()
extern void U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_System_Collections_IEnumerator_Reset_m9119FA446EEE0532BC12BD62858486173A9E51CA (void);
// 0x000000CF System.Object PlayerBehavior/<AbleToProceedLeaderboardAfterTime>d__88::System.Collections.IEnumerator.get_Current()
extern void U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_System_Collections_IEnumerator_get_Current_m5E07809987A0D316422B77E8F8755D199FA282D7 (void);
// 0x000000D0 System.Void PlayerBehavior/<OpenLeaderboardAfterTime>d__89::.ctor(System.Int32)
extern void U3COpenLeaderboardAfterTimeU3Ed__89__ctor_m2A4E052679FFFC11969D70AAB464E5B78F3490A2 (void);
// 0x000000D1 System.Void PlayerBehavior/<OpenLeaderboardAfterTime>d__89::System.IDisposable.Dispose()
extern void U3COpenLeaderboardAfterTimeU3Ed__89_System_IDisposable_Dispose_mD00875A1A0AE2962BBC8A1E3E19DDF62CBE73CAD (void);
// 0x000000D2 System.Boolean PlayerBehavior/<OpenLeaderboardAfterTime>d__89::MoveNext()
extern void U3COpenLeaderboardAfterTimeU3Ed__89_MoveNext_m329DB4B90EB7342841202899B85AAD48BE5CC4EF (void);
// 0x000000D3 System.Object PlayerBehavior/<OpenLeaderboardAfterTime>d__89::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COpenLeaderboardAfterTimeU3Ed__89_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDFD555A751B8003880B730B6AFD29E8A691841F8 (void);
// 0x000000D4 System.Void PlayerBehavior/<OpenLeaderboardAfterTime>d__89::System.Collections.IEnumerator.Reset()
extern void U3COpenLeaderboardAfterTimeU3Ed__89_System_Collections_IEnumerator_Reset_mC656EA4FBAE012267F228A786F865FA4185A3335 (void);
// 0x000000D5 System.Object PlayerBehavior/<OpenLeaderboardAfterTime>d__89::System.Collections.IEnumerator.get_Current()
extern void U3COpenLeaderboardAfterTimeU3Ed__89_System_Collections_IEnumerator_get_Current_m1C900A1EE59D97FA2DA2A48153B3CE7D14DE2D57 (void);
// 0x000000D6 ScoreManagerScript ScoreManagerScript::get_Instance()
extern void ScoreManagerScript_get_Instance_m393A32F09CC80B058D9C17CCA8D8756783E27A37 (void);
// 0x000000D7 System.Void ScoreManagerScript::Awake()
extern void ScoreManagerScript_Awake_m31B40EE0B3C9E853DFDE077E8628A13E451D7FCE (void);
// 0x000000D8 System.Void ScoreManagerScript::Start()
extern void ScoreManagerScript_Start_mE9C9CF6F01BF4B494F8726D1014F0399375172AF (void);
// 0x000000D9 System.Void ScoreManagerScript::Update()
extern void ScoreManagerScript_Update_m61DCCDF89CB448E6A62126FF862FE507A6000A3E (void);
// 0x000000DA System.Void ScoreManagerScript::AddScore(System.Boolean)
extern void ScoreManagerScript_AddScore_m2A2DCFCE0C7274B40C062161C64F435EF8814F55 (void);
// 0x000000DB System.Collections.IEnumerator ScoreManagerScript::RegenerateObstacle(System.Single)
extern void ScoreManagerScript_RegenerateObstacle_m5BC87374E37350071A1C6973E47051B6F613D117 (void);
// 0x000000DC System.Void ScoreManagerScript::.ctor()
extern void ScoreManagerScript__ctor_mC6D2E5B61E9DFE594F34C983943E01798610ED30 (void);
// 0x000000DD System.Void ScoreManagerScript/<RegenerateObstacle>d__19::.ctor(System.Int32)
extern void U3CRegenerateObstacleU3Ed__19__ctor_m60453B6244F7386B560123E985C11370CBEDECBB (void);
// 0x000000DE System.Void ScoreManagerScript/<RegenerateObstacle>d__19::System.IDisposable.Dispose()
extern void U3CRegenerateObstacleU3Ed__19_System_IDisposable_Dispose_mB38500D93A8F554EA8E1909EC0173AE9DD00FCE0 (void);
// 0x000000DF System.Boolean ScoreManagerScript/<RegenerateObstacle>d__19::MoveNext()
extern void U3CRegenerateObstacleU3Ed__19_MoveNext_mB07451936B9CAF55FA75FEED916BA048DB85C098 (void);
// 0x000000E0 System.Object ScoreManagerScript/<RegenerateObstacle>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRegenerateObstacleU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m895A09897E7E9C09EE91BEC95D83C82A53DF79E4 (void);
// 0x000000E1 System.Void ScoreManagerScript/<RegenerateObstacle>d__19::System.Collections.IEnumerator.Reset()
extern void U3CRegenerateObstacleU3Ed__19_System_Collections_IEnumerator_Reset_m7E60D59453D7C13F7010D83AC8979A2E5DC475B9 (void);
// 0x000000E2 System.Object ScoreManagerScript/<RegenerateObstacle>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CRegenerateObstacleU3Ed__19_System_Collections_IEnumerator_get_Current_mCA250A006DE45F560B6CE478835B866C20DBE7D6 (void);
// 0x000000E3 System.Void CrossfadeUIScript::Awake()
extern void CrossfadeUIScript_Awake_m33237D5647DDC12FCB777BEE4C0D6FE7677AE259 (void);
// 0x000000E4 System.Void CrossfadeUIScript::TriggerTransition()
extern void CrossfadeUIScript_TriggerTransition_m8C3863B86770A36192134B09B5014F47B72BA67A (void);
// 0x000000E5 System.Void CrossfadeUIScript::ChangeAnimationState(System.Int32)
extern void CrossfadeUIScript_ChangeAnimationState_mA1B07728BBE4498E4EE1ED10D253BF7CC71F4FDC (void);
// 0x000000E6 System.Void CrossfadeUIScript::.ctor()
extern void CrossfadeUIScript__ctor_m985700FAEFE8BA20B5459BC8A19008887152868B (void);
// 0x000000E7 System.Void ExtraValidationCheckBox::Start()
extern void ExtraValidationCheckBox_Start_m0F102F70D134BE2D33628B9FC0B8A18157BC5CCC (void);
// 0x000000E8 System.Void ExtraValidationCheckBox::DetectToggleIsSelected()
extern void ExtraValidationCheckBox_DetectToggleIsSelected_m1EEA2AF6BFC64B72C8158EFB079C181D798FDB45 (void);
// 0x000000E9 System.Void ExtraValidationCheckBox::.ctor()
extern void ExtraValidationCheckBox__ctor_mA99A77C5802805A1EAC458E082D3962A2A94B996 (void);
// 0x000000EA System.Void ExtraValidationDropdown::Start()
extern void ExtraValidationDropdown_Start_m48A17259A853C881AEEFF71DE17CFF2574A9877F (void);
// 0x000000EB System.Void ExtraValidationDropdown::DetectDropdownIsSelected()
extern void ExtraValidationDropdown_DetectDropdownIsSelected_mBD9E83A627DC3F4635F1443CDA8512F6D0D74110 (void);
// 0x000000EC System.Void ExtraValidationDropdown::.ctor()
extern void ExtraValidationDropdown__ctor_mAF11315E4BA56E38A0ED91957BEEF33E33F377CC (void);
// 0x000000ED System.Void ExtraValidationInputField::Start()
extern void ExtraValidationInputField_Start_m7EBE4F38E5401D6DF83A57732A182DBA9D92C9D0 (void);
// 0x000000EE System.Void ExtraValidationInputField::ValidateInputField()
extern void ExtraValidationInputField_ValidateInputField_m8575A6D2B4AEBF4879243007ACCB4B8F8B250D0B (void);
// 0x000000EF System.Void ExtraValidationInputField::DetectInputFieldIsFill()
extern void ExtraValidationInputField_DetectInputFieldIsFill_m414E2461D5132D71FD38752E75F42DFE08BD6632 (void);
// 0x000000F0 System.Boolean ExtraValidationInputField::ValidateNumericNumber(System.String)
extern void ExtraValidationInputField_ValidateNumericNumber_m1CCD9C23BF2FD301A920C31EED8E50ACA28855CE (void);
// 0x000000F1 System.Boolean ExtraValidationInputField::ValidateEmail(System.String)
extern void ExtraValidationInputField_ValidateEmail_mD4184B848E1217D438988732C55A5534F4E0B7EB (void);
// 0x000000F2 System.Void ExtraValidationInputField::.ctor()
extern void ExtraValidationInputField__ctor_mCB2F80A18E6CD4A613AA004C185990E02BCCAB25 (void);
// 0x000000F3 System.Void IGameUI::SetInputFieldUninteractable()
// 0x000000F4 System.Void IGameUI::SetInputFieldInteractable()
// 0x000000F5 System.Void LeaderboardDisplayScript::Update()
extern void LeaderboardDisplayScript_Update_m00FCB0831EBBE4E5D221EF93B6C16C000CDF9376 (void);
// 0x000000F6 System.Void LeaderboardDisplayScript::GenerateLeaderboard()
extern void LeaderboardDisplayScript_GenerateLeaderboard_mA93E5C4E016A09F373C65711B972015BAD073E48 (void);
// 0x000000F7 System.Void LeaderboardDisplayScript::CloseLeaderboard()
extern void LeaderboardDisplayScript_CloseLeaderboard_m9B586742A0E3B5AEAEAAFA5F8B3F104EFDE3FD2A (void);
// 0x000000F8 System.Void LeaderboardDisplayScript::CloseLeaderboardAndRestart()
extern void LeaderboardDisplayScript_CloseLeaderboardAndRestart_m865B7FCE848AC5B9FE9DE034480AE62814DE92AA (void);
// 0x000000F9 System.Void LeaderboardDisplayScript::.ctor()
extern void LeaderboardDisplayScript__ctor_mDB5183DD7A6E596C1B887ED43E496EE8C830C4B2 (void);
// 0x000000FA System.Void LoginUIScript::OnLoginButtonClick()
extern void LoginUIScript_OnLoginButtonClick_m7B579BBAE90F1EE8BCAA633EEF499C875B9DE58E (void);
// 0x000000FB System.Collections.IEnumerator LoginUIScript::LoginButtonPress(System.Single)
extern void LoginUIScript_LoginButtonPress_m0CE571C7B9164C53A9606738B7BC8FA2482CAA3E (void);
// 0x000000FC System.Void LoginUIScript::UIConstruct()
extern void LoginUIScript_UIConstruct_m8746B0F4400E0DAF2544FF989E75112FF608EB8A (void);
// 0x000000FD System.Void LoginUIScript::.ctor()
extern void LoginUIScript__ctor_m7FD6FEDE637A93B5D8E243F7A96E23D42B5BF03B (void);
// 0x000000FE System.Void LoginUIScript/<LoginButtonPress>d__5::.ctor(System.Int32)
extern void U3CLoginButtonPressU3Ed__5__ctor_m935D324B8C35004111454210399F8E62DEE9F2EB (void);
// 0x000000FF System.Void LoginUIScript/<LoginButtonPress>d__5::System.IDisposable.Dispose()
extern void U3CLoginButtonPressU3Ed__5_System_IDisposable_Dispose_mBDEB7D0D4575B10290E226D82EBC8EF10CFD99F0 (void);
// 0x00000100 System.Boolean LoginUIScript/<LoginButtonPress>d__5::MoveNext()
extern void U3CLoginButtonPressU3Ed__5_MoveNext_mA392CD45BB03008E84C3BF8F51C594A39405ED47 (void);
// 0x00000101 System.Object LoginUIScript/<LoginButtonPress>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoginButtonPressU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m82C6CC51A0997A8D3CD01A602CC35F562ACEF118 (void);
// 0x00000102 System.Void LoginUIScript/<LoginButtonPress>d__5::System.Collections.IEnumerator.Reset()
extern void U3CLoginButtonPressU3Ed__5_System_Collections_IEnumerator_Reset_m3024966D9BF1F0E05CE3A175381E492052284959 (void);
// 0x00000103 System.Object LoginUIScript/<LoginButtonPress>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CLoginButtonPressU3Ed__5_System_Collections_IEnumerator_get_Current_mAB81582C564DB4C9DD1A7EEB8EA4AF8BCF1A73E0 (void);
// 0x00000104 System.Void MainTitleUIScript::Start()
extern void MainTitleUIScript_Start_m9E4B8679626C9C6F9557E1B49F8D25B650B02018 (void);
// 0x00000105 System.Void MainTitleUIScript::OnPlayAsGuestButtonClick()
extern void MainTitleUIScript_OnPlayAsGuestButtonClick_mA623D5298BBBAD6824538ECC0F814744E6F414B9 (void);
// 0x00000106 System.Void MainTitleUIScript::OnMuteButtonClick()
extern void MainTitleUIScript_OnMuteButtonClick_mF538AD8A57C5D46215B4AEE84D1CB421FC2939C5 (void);
// 0x00000107 System.Void MainTitleUIScript::OnLoginButtonClick()
extern void MainTitleUIScript_OnLoginButtonClick_m4F660564F43EAF95B114F2EDA355E86D606FFC1F (void);
// 0x00000108 System.Void MainTitleUIScript::.ctor()
extern void MainTitleUIScript__ctor_mFFA7E92D65AEA2AA744E07D96D9257ABC829ABF0 (void);
// 0x00000109 System.Void RegisterUIScript::Start()
extern void RegisterUIScript_Start_m4E3EE5DAAC1F71889B4E62B08054BDC06ADBFBFF (void);
// 0x0000010A System.Void RegisterUIScript::OnRegisterButtonClick()
extern void RegisterUIScript_OnRegisterButtonClick_mEBC8C696504346FD262A7BA918B08B3FD3919105 (void);
// 0x0000010B System.Void RegisterUIScript::UIConstruct()
extern void RegisterUIScript_UIConstruct_mA300E7AE59A5971254B2D0357590E03822F1C4FE (void);
// 0x0000010C System.Void RegisterUIScript::.ctor()
extern void RegisterUIScript__ctor_m3E6E86B8303B3834EDAB38BD13FF871A69ABE4AF (void);
// 0x0000010D System.Void RegisterUIScript::<Start>b__11_0()
extern void RegisterUIScript_U3CStartU3Eb__11_0_mA65A23DD36E49ED96B0962932AC2A81EDC81693B (void);
// 0x0000010E System.Void ScoreDisplayScript::Start()
extern void ScoreDisplayScript_Start_mFD12910929721600204A1C4608CC4B59C746CA12 (void);
// 0x0000010F System.Void ScoreDisplayScript::Update()
extern void ScoreDisplayScript_Update_m5CAB913D5E6B99EB03180AC78FA29F4D181B67AF (void);
// 0x00000110 System.Void ScoreDisplayScript::.ctor()
extern void ScoreDisplayScript__ctor_mF034AA41D7D3AC4F193C889E003594989B3DA665 (void);
// 0x00000111 System.Void UpdatePasswordUIScript::Start()
extern void UpdatePasswordUIScript_Start_mAC4437267B2216497D2E6A3A4509EFA8FE6394BD (void);
// 0x00000112 System.Void UpdatePasswordUIScript::OnUpdatePasswordButtonClick()
extern void UpdatePasswordUIScript_OnUpdatePasswordButtonClick_m13F93CAD3D1CC2D456FD1DCADD762EB003FD1ED9 (void);
// 0x00000113 System.Void UpdatePasswordUIScript::UIConstruct()
extern void UpdatePasswordUIScript_UIConstruct_m2FAA6588DEE6C0895E0A40CEED25800F6C1030F0 (void);
// 0x00000114 System.Void UpdatePasswordUIScript::.ctor()
extern void UpdatePasswordUIScript__ctor_m20FBDE893E685A769FFD4DC905A3F49E5E91A3BA (void);
// 0x00000115 System.Void UpdatePasswordUIScript::<Start>b__6_0()
extern void UpdatePasswordUIScript_U3CStartU3Eb__6_0_m29B697067F37BA21498266921EF007CB013D8DF3 (void);
// 0x00000116 System.Void WinningScreenFadeUIScript::Awake()
extern void WinningScreenFadeUIScript_Awake_mDEF9D76F9CBF5CDB0E0252571E48AB24CE558C87 (void);
// 0x00000117 System.Void WinningScreenFadeUIScript::FadeInWinningScreen()
extern void WinningScreenFadeUIScript_FadeInWinningScreen_m259FB62586373BF0631F6DE5F8C0460EBB1F8935 (void);
// 0x00000118 System.Void WinningScreenFadeUIScript::ChangeAnimationState(System.Int32)
extern void WinningScreenFadeUIScript_ChangeAnimationState_m8D0954583A63AE5C45FBC735B8542DFD558DC3F6 (void);
// 0x00000119 System.Void WinningScreenFadeUIScript::.ctor()
extern void WinningScreenFadeUIScript__ctor_m20E7A110CB2F7060F0325B76603BF2596145326C (void);
// 0x0000011A System.Void WinningCollisionScript::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void WinningCollisionScript_OnTriggerEnter2D_m73350BFE073676C9078E26633F4DE50DE66F8B02 (void);
// 0x0000011B System.Void WinningCollisionScript::.ctor()
extern void WinningCollisionScript__ctor_m791F6D742A94595C28A0B627C057E4156F59F394 (void);
// 0x0000011C SimpleJSON.JSONNodeType SimpleJSON.JSONNode::get_Tag()
// 0x0000011D SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.Int32)
extern void JSONNode_get_Item_m9B4DBE85D7742250EF918F4450E3CE020AF7C807 (void);
// 0x0000011E System.Void SimpleJSON.JSONNode::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONNode_set_Item_m62DFE58028E55800BF80EFE6E0CE4CDE6DF00CA7 (void);
// 0x0000011F SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.String)
extern void JSONNode_get_Item_mB39E17FF10DD6C0D91FCF1EF485A73F434B34F1D (void);
// 0x00000120 System.Void SimpleJSON.JSONNode::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONNode_set_Item_m9F8D28C00CA46F8C0BD5E43500AEAFC3A1999E07 (void);
// 0x00000121 System.String SimpleJSON.JSONNode::get_Value()
extern void JSONNode_get_Value_m8F31142225AAE4DDEC7ACB74B7FFB30A6DD672F1 (void);
// 0x00000122 System.Void SimpleJSON.JSONNode::set_Value(System.String)
extern void JSONNode_set_Value_mA046548FC6B7DCBDA4964DB05303EBA0CF2A29CA (void);
// 0x00000123 System.Int32 SimpleJSON.JSONNode::get_Count()
extern void JSONNode_get_Count_m6FD5676FECC6B26A6384D8294C213C4B032EA269 (void);
// 0x00000124 System.Boolean SimpleJSON.JSONNode::get_IsNumber()
extern void JSONNode_get_IsNumber_m682272D381670DCFA66BD9EE3C0CE3CB161E3AAD (void);
// 0x00000125 System.Boolean SimpleJSON.JSONNode::get_IsString()
extern void JSONNode_get_IsString_mE0D6737C47977364E9F1620CEA98454EAFAEB196 (void);
// 0x00000126 System.Boolean SimpleJSON.JSONNode::get_IsBoolean()
extern void JSONNode_get_IsBoolean_mA575C54AD05577A9F45E0F55355C0B40F60BFA24 (void);
// 0x00000127 System.Boolean SimpleJSON.JSONNode::get_IsNull()
extern void JSONNode_get_IsNull_m85D1B73ABCCE95314768A4E690D941B82D08CD58 (void);
// 0x00000128 System.Boolean SimpleJSON.JSONNode::get_IsArray()
extern void JSONNode_get_IsArray_m54719E46991451D5C8750D8265D0C6F91EEE97D7 (void);
// 0x00000129 System.Boolean SimpleJSON.JSONNode::get_IsObject()
extern void JSONNode_get_IsObject_mC9E7B94CB90443629157D7C2AB5ED83510DD0BD1 (void);
// 0x0000012A System.Boolean SimpleJSON.JSONNode::get_Inline()
extern void JSONNode_get_Inline_m6791C62A74ACCEA90470A7EC177FA3F12F744F09 (void);
// 0x0000012B System.Void SimpleJSON.JSONNode::set_Inline(System.Boolean)
extern void JSONNode_set_Inline_m5C5C9E182CE521A831EBD2D708568297601AD835 (void);
// 0x0000012C System.Void SimpleJSON.JSONNode::Add(System.String,SimpleJSON.JSONNode)
extern void JSONNode_Add_m10F03DC3FD017B22B831388D39FF735D5AFE96A4 (void);
// 0x0000012D System.Void SimpleJSON.JSONNode::Add(SimpleJSON.JSONNode)
extern void JSONNode_Add_m7DCFC7266181DE46B90ECC7C5DD7D5B7618F9DE2 (void);
// 0x0000012E SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(System.String)
extern void JSONNode_Remove_m49D627BAE273E902C8EB8345BDD7B33E945C2E47 (void);
// 0x0000012F SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(System.Int32)
extern void JSONNode_Remove_m3B3259E69D33121E4974FE1ACFAEBED4B034C053 (void);
// 0x00000130 SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(SimpleJSON.JSONNode)
extern void JSONNode_Remove_mD1FE270ABF87BCD7D09AAA22286AB3C315BAA0EA (void);
// 0x00000131 System.Void SimpleJSON.JSONNode::Clear()
extern void JSONNode_Clear_m706F6EBA5C07C64C78FADE96D642EA3A108C1978 (void);
// 0x00000132 SimpleJSON.JSONNode SimpleJSON.JSONNode::Clone()
extern void JSONNode_Clone_mB8EAA2AC7D9354F0BB2BFE8154199D2B5787E5EB (void);
// 0x00000133 System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode::get_Children()
extern void JSONNode_get_Children_mB9A68101166B1C5176311D2EC1ABCC832248FDFF (void);
// 0x00000134 System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode::get_DeepChildren()
extern void JSONNode_get_DeepChildren_m3E3CE542A4856D0BD557B7E121B4CAA999C8110C (void);
// 0x00000135 System.Boolean SimpleJSON.JSONNode::HasKey(System.String)
extern void JSONNode_HasKey_m844625544C6ACE8B18B84D836357CF0CB83D4109 (void);
// 0x00000136 SimpleJSON.JSONNode SimpleJSON.JSONNode::GetValueOrDefault(System.String,SimpleJSON.JSONNode)
extern void JSONNode_GetValueOrDefault_m995F3AFE1A8C9A1F6AA41CB6EB3C9D1514BBCD2E (void);
// 0x00000137 System.String SimpleJSON.JSONNode::ToString()
extern void JSONNode_ToString_m9E23A359C8B7209DEED6B81DA27C8D0619F9CE98 (void);
// 0x00000138 System.String SimpleJSON.JSONNode::ToString(System.Int32)
extern void JSONNode_ToString_m9B8D0351B9B14D65ABF1090E6D07E0C69DFB84C3 (void);
// 0x00000139 System.Void SimpleJSON.JSONNode::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
// 0x0000013A SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONNode::GetEnumerator()
// 0x0000013B System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>> SimpleJSON.JSONNode::get_Linq()
extern void JSONNode_get_Linq_m568F8D242CB58C62C9D8230A0C1F684D70573268 (void);
// 0x0000013C SimpleJSON.JSONNode/KeyEnumerator SimpleJSON.JSONNode::get_Keys()
extern void JSONNode_get_Keys_m6C1FC7F6C35656362327B486F0426273234584E2 (void);
// 0x0000013D SimpleJSON.JSONNode/ValueEnumerator SimpleJSON.JSONNode::get_Values()
extern void JSONNode_get_Values_m1097622D87EFAC54681A9A9796E563DA142D7677 (void);
// 0x0000013E System.Double SimpleJSON.JSONNode::get_AsDouble()
extern void JSONNode_get_AsDouble_m0DE7D5E7712FB59B32C1C570EBBF85D597B98656 (void);
// 0x0000013F System.Void SimpleJSON.JSONNode::set_AsDouble(System.Double)
extern void JSONNode_set_AsDouble_mBA8F81DF080D29E60DDE8235663BAFFA688736D5 (void);
// 0x00000140 System.Int32 SimpleJSON.JSONNode::get_AsInt()
extern void JSONNode_get_AsInt_m743FC99FEF2DA80A8C6CB71137A5FFD28084E0FA (void);
// 0x00000141 System.Void SimpleJSON.JSONNode::set_AsInt(System.Int32)
extern void JSONNode_set_AsInt_m7CA6313AD31E9E08FEB0B6F8B92AD9A3882D9B75 (void);
// 0x00000142 System.Single SimpleJSON.JSONNode::get_AsFloat()
extern void JSONNode_get_AsFloat_m4B2A24C67F4FBF872DEA6360719D854AE1AD1FBB (void);
// 0x00000143 System.Void SimpleJSON.JSONNode::set_AsFloat(System.Single)
extern void JSONNode_set_AsFloat_m624BDD6CAF17D1709BACFDF3554F2AE11FDD22D1 (void);
// 0x00000144 System.Boolean SimpleJSON.JSONNode::get_AsBool()
extern void JSONNode_get_AsBool_m15160B79EBEA51E7A2C7C7A23B3540A60434B36D (void);
// 0x00000145 System.Void SimpleJSON.JSONNode::set_AsBool(System.Boolean)
extern void JSONNode_set_AsBool_m0878AF783E25077E17850DD1B4522B17FF08770F (void);
// 0x00000146 System.Int64 SimpleJSON.JSONNode::get_AsLong()
extern void JSONNode_get_AsLong_mE7DA8DC8A512CA74C7B372E9FF9CAE347E4BA855 (void);
// 0x00000147 System.Void SimpleJSON.JSONNode::set_AsLong(System.Int64)
extern void JSONNode_set_AsLong_m24D91AED5E8EF3285A82FEDDE9D7D151DBEDAFA6 (void);
// 0x00000148 System.UInt64 SimpleJSON.JSONNode::get_AsULong()
extern void JSONNode_get_AsULong_mEEA4E92743A00ECBEE45404E05CA6C0D082389A3 (void);
// 0x00000149 System.Void SimpleJSON.JSONNode::set_AsULong(System.UInt64)
extern void JSONNode_set_AsULong_mE9F8E62D253807F5BE0B2768D4CFB9AA3E232054 (void);
// 0x0000014A SimpleJSON.JSONArray SimpleJSON.JSONNode::get_AsArray()
extern void JSONNode_get_AsArray_m10801C5609C0C024480B49DFA03A4FB16A4E6829 (void);
// 0x0000014B SimpleJSON.JSONObject SimpleJSON.JSONNode::get_AsObject()
extern void JSONNode_get_AsObject_mDE74F42234B130BA44AD17DF9FFC64A2D8DFCD46 (void);
// 0x0000014C SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.String)
extern void JSONNode_op_Implicit_m3938223B519495895A5B4E53D60789BB2D4620F2 (void);
// 0x0000014D System.String SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m110AF33FB2CEF68FF905E93F656AF02222554668 (void);
// 0x0000014E SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Double)
extern void JSONNode_op_Implicit_m66EA9B62BBFD5A306C2B95703443F1960338BA01 (void);
// 0x0000014F System.Double SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m98778303F0AD762A7C4FE928D82352A1F8276EBE (void);
// 0x00000150 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Single)
extern void JSONNode_op_Implicit_m3780006769998A253BE0F8068E68BD88AC692112 (void);
// 0x00000151 System.Single SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mA90C42C0D957F5F284857AE91907D69477331166 (void);
// 0x00000152 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Int32)
extern void JSONNode_op_Implicit_mDAC018F58F31AB6333A9852BD0B038152063B3F2 (void);
// 0x00000153 System.Int32 SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m10A6B0627DE9BC247B0B39554FEA1CD37CA7988C (void);
// 0x00000154 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Int64)
extern void JSONNode_op_Implicit_m45DA7291150C2AD5FF800B3E6BFF747D59D571DC (void);
// 0x00000155 System.Int64 SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mA5840D22D4BEDED7AC00F6F5A36938B4C919BFD3 (void);
// 0x00000156 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.UInt64)
extern void JSONNode_op_Implicit_mCDBF45400C2B893A51A9E5ACD26D479C5B942C82 (void);
// 0x00000157 System.UInt64 SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m4A1F576E9A7CB07C3BECEFF936EC66C01E8FF884 (void);
// 0x00000158 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Boolean)
extern void JSONNode_op_Implicit_mAF077674B97BAF8FD4EB941A63B84D26F6E08EDC (void);
// 0x00000159 System.Boolean SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mE4078D38F919268986EF017D4A9EA55FD8CBA826 (void);
// 0x0000015A SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>)
extern void JSONNode_op_Implicit_m92A46C2D66615E97C21041238C09386EDCF0A266 (void);
// 0x0000015B System.Boolean SimpleJSON.JSONNode::op_Equality(SimpleJSON.JSONNode,System.Object)
extern void JSONNode_op_Equality_mA320F56360F44724C465167318C8FC38F0DCA38F (void);
// 0x0000015C System.Boolean SimpleJSON.JSONNode::op_Inequality(SimpleJSON.JSONNode,System.Object)
extern void JSONNode_op_Inequality_m00AE17CECA317ECC98958F027D0545C831DA24FE (void);
// 0x0000015D System.Boolean SimpleJSON.JSONNode::Equals(System.Object)
extern void JSONNode_Equals_mC1598F5265D88ECAA3040C537A28FC2DC878BBFF (void);
// 0x0000015E System.Int32 SimpleJSON.JSONNode::GetHashCode()
extern void JSONNode_GetHashCode_mF900BB7E50D965906CDBC7210B9097CDE6B89834 (void);
// 0x0000015F System.Text.StringBuilder SimpleJSON.JSONNode::get_EscapeBuilder()
extern void JSONNode_get_EscapeBuilder_m87ACD2EE75FD74E9E7B67BBB66FCDB0643749628 (void);
// 0x00000160 System.String SimpleJSON.JSONNode::Escape(System.String)
extern void JSONNode_Escape_m5C3A578A1281224A7BDF595470FA706CBC597A68 (void);
// 0x00000161 SimpleJSON.JSONNode SimpleJSON.JSONNode::ParseElement(System.String,System.Boolean)
extern void JSONNode_ParseElement_mB69AF0C51A3AD8AF384A016599AEAEEF978533E3 (void);
// 0x00000162 SimpleJSON.JSONNode SimpleJSON.JSONNode::Parse(System.String)
extern void JSONNode_Parse_m747C7ACB3E8F42CB32651EAB1506047F02BF4076 (void);
// 0x00000163 System.Void SimpleJSON.JSONNode::.ctor()
extern void JSONNode__ctor_mF0692814A0795056AE052B05EF63D6B216B5619E (void);
// 0x00000164 System.Void SimpleJSON.JSONNode::.cctor()
extern void JSONNode__cctor_m0F95CA9754AE155CEC873DEFB28A83E8AAC25990 (void);
// 0x00000165 System.Boolean SimpleJSON.JSONNode/Enumerator::get_IsValid()
extern void Enumerator_get_IsValid_m078841FA11B23FAA03C8C0F0F5CFE1C7A33CC267_AdjustorThunk (void);
// 0x00000166 System.Void SimpleJSON.JSONNode/Enumerator::.ctor(System.Collections.Generic.List`1/Enumerator<SimpleJSON.JSONNode>)
extern void Enumerator__ctor_m0A9EDBF3F33AA5FAE87C204FA885D965D16E8A44_AdjustorThunk (void);
// 0x00000167 System.Void SimpleJSON.JSONNode/Enumerator::.ctor(System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJSON.JSONNode>)
extern void Enumerator__ctor_mDE6D3B06DF10240F0C637131119C8B09CE9EC4DC_AdjustorThunk (void);
// 0x00000168 System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONNode/Enumerator::get_Current()
extern void Enumerator_get_Current_m6C700A8C86DAB32F9FE845C3BE1E35D17757C65C_AdjustorThunk (void);
// 0x00000169 System.Boolean SimpleJSON.JSONNode/Enumerator::MoveNext()
extern void Enumerator_MoveNext_m095925BE3D881DF1E015997DBEAFE0851722E53E_AdjustorThunk (void);
// 0x0000016A System.Void SimpleJSON.JSONNode/ValueEnumerator::.ctor(System.Collections.Generic.List`1/Enumerator<SimpleJSON.JSONNode>)
extern void ValueEnumerator__ctor_m1FF9C4191C706E48C3F9A191D0CE02F9BECDCE45_AdjustorThunk (void);
// 0x0000016B System.Void SimpleJSON.JSONNode/ValueEnumerator::.ctor(System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJSON.JSONNode>)
extern void ValueEnumerator__ctor_m8143FFB5AD9AB3936DF1A59E3492C2CF81793AF5_AdjustorThunk (void);
// 0x0000016C System.Void SimpleJSON.JSONNode/ValueEnumerator::.ctor(SimpleJSON.JSONNode/Enumerator)
extern void ValueEnumerator__ctor_mE92051B3948C6A45D4F9265EA6D8329D70649D06_AdjustorThunk (void);
// 0x0000016D SimpleJSON.JSONNode SimpleJSON.JSONNode/ValueEnumerator::get_Current()
extern void ValueEnumerator_get_Current_mAB38FFFD4C1293C61EE73C825D85B7258B61961C_AdjustorThunk (void);
// 0x0000016E System.Boolean SimpleJSON.JSONNode/ValueEnumerator::MoveNext()
extern void ValueEnumerator_MoveNext_mE68E69AFB9D6FA2B41F55B066F3295490EED5589_AdjustorThunk (void);
// 0x0000016F SimpleJSON.JSONNode/ValueEnumerator SimpleJSON.JSONNode/ValueEnumerator::GetEnumerator()
extern void ValueEnumerator_GetEnumerator_mF841522782073D8D5F9782A37B2487BEA63E5E09_AdjustorThunk (void);
// 0x00000170 System.Void SimpleJSON.JSONNode/KeyEnumerator::.ctor(System.Collections.Generic.List`1/Enumerator<SimpleJSON.JSONNode>)
extern void KeyEnumerator__ctor_m6E59374D6FF2E994DFB9BFCA3759CC335CA48D23_AdjustorThunk (void);
// 0x00000171 System.Void SimpleJSON.JSONNode/KeyEnumerator::.ctor(System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJSON.JSONNode>)
extern void KeyEnumerator__ctor_mB67A74FB33429333A208FA269F011122C1AB6395_AdjustorThunk (void);
// 0x00000172 System.Void SimpleJSON.JSONNode/KeyEnumerator::.ctor(SimpleJSON.JSONNode/Enumerator)
extern void KeyEnumerator__ctor_m5F83F3F252DE00DB6AC2AE59FD501F921C56F224_AdjustorThunk (void);
// 0x00000173 System.String SimpleJSON.JSONNode/KeyEnumerator::get_Current()
extern void KeyEnumerator_get_Current_m2F8F06987AA5A1600CFA852F7510BF01A3F4DEB4_AdjustorThunk (void);
// 0x00000174 System.Boolean SimpleJSON.JSONNode/KeyEnumerator::MoveNext()
extern void KeyEnumerator_MoveNext_mFF5FB4ECB623B58733E9B85846A296853462F2F4_AdjustorThunk (void);
// 0x00000175 SimpleJSON.JSONNode/KeyEnumerator SimpleJSON.JSONNode/KeyEnumerator::GetEnumerator()
extern void KeyEnumerator_GetEnumerator_m4534071ECBA3E7165BB0DC642EA9E827A29F5A87_AdjustorThunk (void);
// 0x00000176 System.Void SimpleJSON.JSONNode/LinqEnumerator::.ctor(SimpleJSON.JSONNode)
extern void LinqEnumerator__ctor_m711F6A5F82CC6093E5245A51BFEF95B0355E2FAC (void);
// 0x00000177 System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONNode/LinqEnumerator::get_Current()
extern void LinqEnumerator_get_Current_m7F293F41FED328A040E5BC71C7F4560C3B8C295C (void);
// 0x00000178 System.Object SimpleJSON.JSONNode/LinqEnumerator::System.Collections.IEnumerator.get_Current()
extern void LinqEnumerator_System_Collections_IEnumerator_get_Current_m36203F845AA95DEB756D145E03469EF077B79BF1 (void);
// 0x00000179 System.Boolean SimpleJSON.JSONNode/LinqEnumerator::MoveNext()
extern void LinqEnumerator_MoveNext_m547BA7FD679ED14C3E698763EBEC58333194B9EE (void);
// 0x0000017A System.Void SimpleJSON.JSONNode/LinqEnumerator::Dispose()
extern void LinqEnumerator_Dispose_mF5270BF1DEAB954E8FC65435DF6D6FD5DBE1C21F (void);
// 0x0000017B System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>> SimpleJSON.JSONNode/LinqEnumerator::GetEnumerator()
extern void LinqEnumerator_GetEnumerator_m356D1E4E1EC2D215962F677F3C6AE538E3CF9A20 (void);
// 0x0000017C System.Void SimpleJSON.JSONNode/LinqEnumerator::Reset()
extern void LinqEnumerator_Reset_mCC3E0E49D5343E81C63A2B4A28A7268F628A2522 (void);
// 0x0000017D System.Collections.IEnumerator SimpleJSON.JSONNode/LinqEnumerator::System.Collections.IEnumerable.GetEnumerator()
extern void LinqEnumerator_System_Collections_IEnumerable_GetEnumerator_m713D8655544E9FC2911762CD17D215B5763AE735 (void);
// 0x0000017E System.Void SimpleJSON.JSONNode/<get_Children>d__43::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__43__ctor_m1E3DEE85215E302020E599B5D498D5F257B841E5 (void);
// 0x0000017F System.Void SimpleJSON.JSONNode/<get_Children>d__43::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__43_System_IDisposable_Dispose_m3F00470D6E61DBE9CF64CD523D38F5092505403A (void);
// 0x00000180 System.Boolean SimpleJSON.JSONNode/<get_Children>d__43::MoveNext()
extern void U3Cget_ChildrenU3Ed__43_MoveNext_m44C527BF6FC399EF5A6818F21D2DE6EE987A3514 (void);
// 0x00000181 SimpleJSON.JSONNode SimpleJSON.JSONNode/<get_Children>d__43::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__43_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m2E015F7A52E8208F5DBDD306CA59D6DDA7C5E14B (void);
// 0x00000182 System.Void SimpleJSON.JSONNode/<get_Children>d__43::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__43_System_Collections_IEnumerator_Reset_m651CEB1CC2F1607A9BC74E77C5B74A593E064F08 (void);
// 0x00000183 System.Object SimpleJSON.JSONNode/<get_Children>d__43::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__43_System_Collections_IEnumerator_get_Current_m10EB5342637BBF2E59C8EC9A660D591135EB6ACA (void);
// 0x00000184 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode/<get_Children>d__43::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__43_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m3C96B2C39F4888749B03B276C3525C5F6F65F689 (void);
// 0x00000185 System.Collections.IEnumerator SimpleJSON.JSONNode/<get_Children>d__43::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__43_System_Collections_IEnumerable_GetEnumerator_m348BC5D347B12C3670753E9B49E7A85F98FBD028 (void);
// 0x00000186 System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__45::.ctor(System.Int32)
extern void U3Cget_DeepChildrenU3Ed__45__ctor_mAEF820B12BA0E5ACD7AEAF398E13D86229F33824 (void);
// 0x00000187 System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__45::System.IDisposable.Dispose()
extern void U3Cget_DeepChildrenU3Ed__45_System_IDisposable_Dispose_m402090148B49E68DBBB71155A79B32A9D5EC0B28 (void);
// 0x00000188 System.Boolean SimpleJSON.JSONNode/<get_DeepChildren>d__45::MoveNext()
extern void U3Cget_DeepChildrenU3Ed__45_MoveNext_m7D0B0392EC628E124531B872D453C5627BE36C16 (void);
// 0x00000189 System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__45::<>m__Finally1()
extern void U3Cget_DeepChildrenU3Ed__45_U3CU3Em__Finally1_m7087BAFAFAED9C487312FEFB3224EF8C5AD8C816 (void);
// 0x0000018A System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__45::<>m__Finally2()
extern void U3Cget_DeepChildrenU3Ed__45_U3CU3Em__Finally2_m81B9787080A73A39DD00CC34745F1439DA51099A (void);
// 0x0000018B SimpleJSON.JSONNode SimpleJSON.JSONNode/<get_DeepChildren>d__45::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_DeepChildrenU3Ed__45_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m6F553695AE45AFE3DB3CB3CB041EBA0C989BD418 (void);
// 0x0000018C System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__45::System.Collections.IEnumerator.Reset()
extern void U3Cget_DeepChildrenU3Ed__45_System_Collections_IEnumerator_Reset_m0A483B8285BDDCD8AAE7A06B530E0B8AD49AFBB7 (void);
// 0x0000018D System.Object SimpleJSON.JSONNode/<get_DeepChildren>d__45::System.Collections.IEnumerator.get_Current()
extern void U3Cget_DeepChildrenU3Ed__45_System_Collections_IEnumerator_get_Current_m3405A7516644ED2988C02CD58955E86EF6F4A9C0 (void);
// 0x0000018E System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode/<get_DeepChildren>d__45::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_DeepChildrenU3Ed__45_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m8EC445D3F9C890F98F5EFE538F031CEFD61B518A (void);
// 0x0000018F System.Collections.IEnumerator SimpleJSON.JSONNode/<get_DeepChildren>d__45::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_DeepChildrenU3Ed__45_System_Collections_IEnumerable_GetEnumerator_mDD5EC1B27E01615A2CC852679694910EF826BE7A (void);
// 0x00000190 System.Boolean SimpleJSON.JSONArray::get_Inline()
extern void JSONArray_get_Inline_mCCA0FAE052593C420DF7266D9030F7CBCFF5D43A (void);
// 0x00000191 System.Void SimpleJSON.JSONArray::set_Inline(System.Boolean)
extern void JSONArray_set_Inline_mCDD35E23A81DC4DF48DB54FA164B2D05CA8CDD5C (void);
// 0x00000192 SimpleJSON.JSONNodeType SimpleJSON.JSONArray::get_Tag()
extern void JSONArray_get_Tag_mA6475F4E3B27AE262DFF53A510E5DD9CA9A82212 (void);
// 0x00000193 System.Boolean SimpleJSON.JSONArray::get_IsArray()
extern void JSONArray_get_IsArray_m015BCD163D49042D8827D8B5880EAC8720B93DDD (void);
// 0x00000194 SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONArray::GetEnumerator()
extern void JSONArray_GetEnumerator_m107028482740A363BC3AAC4348F729F49094B8D7 (void);
// 0x00000195 SimpleJSON.JSONNode SimpleJSON.JSONArray::get_Item(System.Int32)
extern void JSONArray_get_Item_mB56A68792D63083DAE88CF576C4E5C9C6D0E1AEC (void);
// 0x00000196 System.Void SimpleJSON.JSONArray::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONArray_set_Item_m8D70249BB8D717AD878AFE87F9F86345CD5DE9EF (void);
// 0x00000197 SimpleJSON.JSONNode SimpleJSON.JSONArray::get_Item(System.String)
extern void JSONArray_get_Item_m5E864AF954B22AD5841E8CEB58AF68DED45C4C8B (void);
// 0x00000198 System.Void SimpleJSON.JSONArray::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONArray_set_Item_mD6DB253A66DC2D34179D9773A1ADF25A11FE7DDE (void);
// 0x00000199 System.Int32 SimpleJSON.JSONArray::get_Count()
extern void JSONArray_get_Count_m8649900F00FBEDB004834FB9D814F848E6C7D72B (void);
// 0x0000019A System.Void SimpleJSON.JSONArray::Add(System.String,SimpleJSON.JSONNode)
extern void JSONArray_Add_m14182F062E171457670DB45BE811A46A78B2D685 (void);
// 0x0000019B SimpleJSON.JSONNode SimpleJSON.JSONArray::Remove(System.Int32)
extern void JSONArray_Remove_m65505E642765169F35A9F21760C7EDC39407B9F9 (void);
// 0x0000019C SimpleJSON.JSONNode SimpleJSON.JSONArray::Remove(SimpleJSON.JSONNode)
extern void JSONArray_Remove_mA79C09C43B22EE2B4965C1D8B3794BAE1957037E (void);
// 0x0000019D System.Void SimpleJSON.JSONArray::Clear()
extern void JSONArray_Clear_m34F9A0A6EA42F82C69B96EC8651181EC221B8329 (void);
// 0x0000019E SimpleJSON.JSONNode SimpleJSON.JSONArray::Clone()
extern void JSONArray_Clone_m61DBE2A5A6231C2F3FD32D1EB44A1693CB6CD172 (void);
// 0x0000019F System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONArray::get_Children()
extern void JSONArray_get_Children_m56D8A696E09F5EEFF1FF9F8BE06011CE5EAFA197 (void);
// 0x000001A0 System.Void SimpleJSON.JSONArray::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONArray_WriteToStringBuilder_m64C54C1D36684A7CE5EAB86E3350AE4B811D40C9 (void);
// 0x000001A1 System.Void SimpleJSON.JSONArray::.ctor()
extern void JSONArray__ctor_m2B6B15254CC3578F4B78A9E5CA656C4D25119512 (void);
// 0x000001A2 System.Void SimpleJSON.JSONArray/<get_Children>d__24::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__24__ctor_mC6643FC9306ABFE30E20A00C08D9738D533DB279 (void);
// 0x000001A3 System.Void SimpleJSON.JSONArray/<get_Children>d__24::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__24_System_IDisposable_Dispose_mDDD5192FEDBCC636E7301392862E8CC8315F155E (void);
// 0x000001A4 System.Boolean SimpleJSON.JSONArray/<get_Children>d__24::MoveNext()
extern void U3Cget_ChildrenU3Ed__24_MoveNext_mD59D17AA8CF3F462A6A65C89885A61EC5F03C409 (void);
// 0x000001A5 System.Void SimpleJSON.JSONArray/<get_Children>d__24::<>m__Finally1()
extern void U3Cget_ChildrenU3Ed__24_U3CU3Em__Finally1_mFEFCD0DE4E0476CA2C2B2897D7BA097B1928B83A (void);
// 0x000001A6 SimpleJSON.JSONNode SimpleJSON.JSONArray/<get_Children>d__24::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__24_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m6143E2C2D943E8398F986B0FD76F13B995D2BE15 (void);
// 0x000001A7 System.Void SimpleJSON.JSONArray/<get_Children>d__24::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__24_System_Collections_IEnumerator_Reset_m838D58EEF0F67271515CB056BAA7E7467754D04A (void);
// 0x000001A8 System.Object SimpleJSON.JSONArray/<get_Children>d__24::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__24_System_Collections_IEnumerator_get_Current_m2B6EE2073CADA240627B5A1419ED1DE249E657E8 (void);
// 0x000001A9 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONArray/<get_Children>d__24::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__24_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mF97261A04403E4BFC8518DB4F85163C057934C26 (void);
// 0x000001AA System.Collections.IEnumerator SimpleJSON.JSONArray/<get_Children>d__24::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__24_System_Collections_IEnumerable_GetEnumerator_m29A61F8B9157EB14CF35EFC4495AF8D7CBDB7C39 (void);
// 0x000001AB System.Boolean SimpleJSON.JSONObject::get_Inline()
extern void JSONObject_get_Inline_mEBB1EA4AB1EE32D80F2DB4C5C4240DD13593750A (void);
// 0x000001AC System.Void SimpleJSON.JSONObject::set_Inline(System.Boolean)
extern void JSONObject_set_Inline_mDDC84E63241E060414987E3B8301F7F1AB2967B4 (void);
// 0x000001AD SimpleJSON.JSONNodeType SimpleJSON.JSONObject::get_Tag()
extern void JSONObject_get_Tag_m0F8454A3B161D9954B2C1B49945DE0E9041907B6 (void);
// 0x000001AE System.Boolean SimpleJSON.JSONObject::get_IsObject()
extern void JSONObject_get_IsObject_m6E6B9273D50B23643A6DFBD17FBEA6BD6143E12C (void);
// 0x000001AF SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONObject::GetEnumerator()
extern void JSONObject_GetEnumerator_mC0AAAB9C27211FED4A7DA97BB1D2DEA1777CABD6 (void);
// 0x000001B0 SimpleJSON.JSONNode SimpleJSON.JSONObject::get_Item(System.String)
extern void JSONObject_get_Item_mF7EF352F2BA035D7EFC5902F2DEB12168E586CBD (void);
// 0x000001B1 System.Void SimpleJSON.JSONObject::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONObject_set_Item_m84B17410261AD3D4C06D551BB169126C42331DD8 (void);
// 0x000001B2 SimpleJSON.JSONNode SimpleJSON.JSONObject::get_Item(System.Int32)
extern void JSONObject_get_Item_m28A01147850D0D538204316C35A6E1A823602C0E (void);
// 0x000001B3 System.Void SimpleJSON.JSONObject::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONObject_set_Item_m17A462CA68BCC6AB9A077BD2D5354C2CD9BAA448 (void);
// 0x000001B4 System.Int32 SimpleJSON.JSONObject::get_Count()
extern void JSONObject_get_Count_m5271E2DB59F0A774A71E1313A43FC47D209DE4BD (void);
// 0x000001B5 System.Void SimpleJSON.JSONObject::Add(System.String,SimpleJSON.JSONNode)
extern void JSONObject_Add_m89A24748B1E3A3BC627C355AD3F946A5541D0D47 (void);
// 0x000001B6 SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(System.String)
extern void JSONObject_Remove_m20C910052474109A5D0B8C63DA98648296C1E846 (void);
// 0x000001B7 SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(System.Int32)
extern void JSONObject_Remove_m6A66604EDD3CBED40F44D85BAD15342B133A2B21 (void);
// 0x000001B8 SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(SimpleJSON.JSONNode)
extern void JSONObject_Remove_m2000AEFD68838D8F04D250DDFF4E5B2B52D2661A (void);
// 0x000001B9 System.Void SimpleJSON.JSONObject::Clear()
extern void JSONObject_Clear_m5332361D04331BEB78ADA3F299212004967EAE05 (void);
// 0x000001BA SimpleJSON.JSONNode SimpleJSON.JSONObject::Clone()
extern void JSONObject_Clone_m61BB490DF0D0A7D29453CFB5EBA356865176978A (void);
// 0x000001BB System.Boolean SimpleJSON.JSONObject::HasKey(System.String)
extern void JSONObject_HasKey_m818D6C5A3544D891213DC9620E4E8F281FB446E3 (void);
// 0x000001BC SimpleJSON.JSONNode SimpleJSON.JSONObject::GetValueOrDefault(System.String,SimpleJSON.JSONNode)
extern void JSONObject_GetValueOrDefault_m0C55823832259A1F2E55088F8204DEAFA1AB03C3 (void);
// 0x000001BD System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONObject::get_Children()
extern void JSONObject_get_Children_mDC37B11CC04E072B9E1DFE56A3B176A1E22F8E6D (void);
// 0x000001BE System.Void SimpleJSON.JSONObject::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONObject_WriteToStringBuilder_m8B9F8C4C58F878A4992C39D21E75CF735DB65F61 (void);
// 0x000001BF System.Void SimpleJSON.JSONObject::.ctor()
extern void JSONObject__ctor_m159A8A4A6AD38E7E4786F8EA8FFB9948DF520F92 (void);
// 0x000001C0 System.Void SimpleJSON.JSONObject/<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_mC0EC999DE1ACB8335F31D6D652C0379FF2D065A6 (void);
// 0x000001C1 System.Boolean SimpleJSON.JSONObject/<>c__DisplayClass21_0::<Remove>b__0(System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>)
extern void U3CU3Ec__DisplayClass21_0_U3CRemoveU3Eb__0_m4DAF41E69ADC8210F10A0E9353204C4C9BB593F0 (void);
// 0x000001C2 System.Void SimpleJSON.JSONObject/<get_Children>d__27::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__27__ctor_m8B8957E246F78B1184815B8C085890B41340711D (void);
// 0x000001C3 System.Void SimpleJSON.JSONObject/<get_Children>d__27::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__27_System_IDisposable_Dispose_m9FF83D38681D2D54D43AC8A1B50B1A3C8CBFC2FD (void);
// 0x000001C4 System.Boolean SimpleJSON.JSONObject/<get_Children>d__27::MoveNext()
extern void U3Cget_ChildrenU3Ed__27_MoveNext_m5A6D6FE80E1EBD6AA95570CDC2DEFDF6AC948AF8 (void);
// 0x000001C5 System.Void SimpleJSON.JSONObject/<get_Children>d__27::<>m__Finally1()
extern void U3Cget_ChildrenU3Ed__27_U3CU3Em__Finally1_m95B7E9611A12888F1C6F07750B36EDCD06FF0A2D (void);
// 0x000001C6 SimpleJSON.JSONNode SimpleJSON.JSONObject/<get_Children>d__27::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__27_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mC4ACE0C684380E7DDC8D7EDE0D76749E2B2EBA5F (void);
// 0x000001C7 System.Void SimpleJSON.JSONObject/<get_Children>d__27::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__27_System_Collections_IEnumerator_Reset_m8F4944630D5FBA4286ED06697CD6932FB73C76EC (void);
// 0x000001C8 System.Object SimpleJSON.JSONObject/<get_Children>d__27::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__27_System_Collections_IEnumerator_get_Current_mECECB942275EF47371EA737AC48C607909027DA3 (void);
// 0x000001C9 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONObject/<get_Children>d__27::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__27_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mBA4BBCF2F261D002F57438B8F03BDF263810D942 (void);
// 0x000001CA System.Collections.IEnumerator SimpleJSON.JSONObject/<get_Children>d__27::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__27_System_Collections_IEnumerable_GetEnumerator_m0EC44BA64BA97F3BA0655F7809FEC0EFE289C8F0 (void);
// 0x000001CB SimpleJSON.JSONNodeType SimpleJSON.JSONString::get_Tag()
extern void JSONString_get_Tag_m8CD40E0F8FEA6E68B02D661527AAB26E581AD925 (void);
// 0x000001CC System.Boolean SimpleJSON.JSONString::get_IsString()
extern void JSONString_get_IsString_mE62F3299F2BDCFA5704FEFBB6D6174C7D18EFEA0 (void);
// 0x000001CD SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONString::GetEnumerator()
extern void JSONString_GetEnumerator_m2DB5AD3DA3FA00B24522A9083E0EC92952D28C14 (void);
// 0x000001CE System.String SimpleJSON.JSONString::get_Value()
extern void JSONString_get_Value_m0A72F580BA1EBD98A7592810069BDF215BC737F0 (void);
// 0x000001CF System.Void SimpleJSON.JSONString::set_Value(System.String)
extern void JSONString_set_Value_m9B7FCFDF82ED801A434AC208F52582A2B0DF2164 (void);
// 0x000001D0 System.Void SimpleJSON.JSONString::.ctor(System.String)
extern void JSONString__ctor_mE09DCD65C60DE37D1F0F79BD6902E464EE1960A3 (void);
// 0x000001D1 SimpleJSON.JSONNode SimpleJSON.JSONString::Clone()
extern void JSONString_Clone_m1B348BFEED419701F25E81BCB6F804B6D72B131D (void);
// 0x000001D2 System.Void SimpleJSON.JSONString::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONString_WriteToStringBuilder_mB338AAA2644A34B03F6DBE6620F8370C64D15627 (void);
// 0x000001D3 System.Boolean SimpleJSON.JSONString::Equals(System.Object)
extern void JSONString_Equals_m9709B63863E6B7E1000EF9E0318B2C499D325B92 (void);
// 0x000001D4 System.Int32 SimpleJSON.JSONString::GetHashCode()
extern void JSONString_GetHashCode_m573FB034DD33C5A300F358A318A0BCA761E35AB1 (void);
// 0x000001D5 System.Void SimpleJSON.JSONString::Clear()
extern void JSONString_Clear_mEABC1369E1751A8B6ACCF8F69DB736B8EDB35446 (void);
// 0x000001D6 SimpleJSON.JSONNodeType SimpleJSON.JSONNumber::get_Tag()
extern void JSONNumber_get_Tag_mC3886860EBAEBED98DF4B58A2E5D2DA1C86D0DF0 (void);
// 0x000001D7 System.Boolean SimpleJSON.JSONNumber::get_IsNumber()
extern void JSONNumber_get_IsNumber_m55E3251DC2B157F6A06A78047C8B7F6A8710CFC1 (void);
// 0x000001D8 SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONNumber::GetEnumerator()
extern void JSONNumber_GetEnumerator_m48371CA028B151ED79247AB9B49B7B120C172F78 (void);
// 0x000001D9 System.String SimpleJSON.JSONNumber::get_Value()
extern void JSONNumber_get_Value_m02C36CBFEEDCE75834055B25A6F0F9845B661B98 (void);
// 0x000001DA System.Void SimpleJSON.JSONNumber::set_Value(System.String)
extern void JSONNumber_set_Value_m437CA8FBE33C4A5C6CA4E2951767974987142A01 (void);
// 0x000001DB System.Double SimpleJSON.JSONNumber::get_AsDouble()
extern void JSONNumber_get_AsDouble_m181BFF83082F0A4A380023EDE5D1EA55B0EC6427 (void);
// 0x000001DC System.Void SimpleJSON.JSONNumber::set_AsDouble(System.Double)
extern void JSONNumber_set_AsDouble_m48E57B49311F9720098069A63A056AF5B44AECBE (void);
// 0x000001DD System.Int64 SimpleJSON.JSONNumber::get_AsLong()
extern void JSONNumber_get_AsLong_m90536472498BCF68E5528450B74FB18631A259CD (void);
// 0x000001DE System.Void SimpleJSON.JSONNumber::set_AsLong(System.Int64)
extern void JSONNumber_set_AsLong_m20401499383A612EAEB52744144B36B7D6ED4A67 (void);
// 0x000001DF System.UInt64 SimpleJSON.JSONNumber::get_AsULong()
extern void JSONNumber_get_AsULong_m349C4A11ADBFD63715DC6CF6D3610E9E3004B1B3 (void);
// 0x000001E0 System.Void SimpleJSON.JSONNumber::set_AsULong(System.UInt64)
extern void JSONNumber_set_AsULong_mC3F7E64D6FF0D26CDD05DC9984C756C0521E78CD (void);
// 0x000001E1 System.Void SimpleJSON.JSONNumber::.ctor(System.Double)
extern void JSONNumber__ctor_mD0AF1324557B9FC7B1D63842D2CA65EC2D1A643A (void);
// 0x000001E2 System.Void SimpleJSON.JSONNumber::.ctor(System.String)
extern void JSONNumber__ctor_m03A803840EF273185C5C63642FAA70FAF9DFE40E (void);
// 0x000001E3 SimpleJSON.JSONNode SimpleJSON.JSONNumber::Clone()
extern void JSONNumber_Clone_m81C53D05C5E0E487036EBE26CE3FDDF79DB88362 (void);
// 0x000001E4 System.Void SimpleJSON.JSONNumber::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONNumber_WriteToStringBuilder_m48A94BA46F195E37F78A7AA167655AE1E870231A (void);
// 0x000001E5 System.Boolean SimpleJSON.JSONNumber::IsNumeric(System.Object)
extern void JSONNumber_IsNumeric_mEA4ED6C1587A4D94F5D6F30B12FA284AB56F11EB (void);
// 0x000001E6 System.Boolean SimpleJSON.JSONNumber::Equals(System.Object)
extern void JSONNumber_Equals_m0D80E8BA86AA78FBB0482F9F053A0BACB65D8D97 (void);
// 0x000001E7 System.Int32 SimpleJSON.JSONNumber::GetHashCode()
extern void JSONNumber_GetHashCode_mB10776EF12594532AB12FA617D1BF18FC53C790C (void);
// 0x000001E8 System.Void SimpleJSON.JSONNumber::Clear()
extern void JSONNumber_Clear_m63292B1A0C3FE42BB3DABEDF12F25ECE633C66AA (void);
// 0x000001E9 SimpleJSON.JSONNodeType SimpleJSON.JSONBool::get_Tag()
extern void JSONBool_get_Tag_m43F4216FE45F0043979093D7BE5FD16C0F462A62 (void);
// 0x000001EA System.Boolean SimpleJSON.JSONBool::get_IsBoolean()
extern void JSONBool_get_IsBoolean_mE70A4F82605CAB1E14A70413FC42EF66ADBAB727 (void);
// 0x000001EB SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONBool::GetEnumerator()
extern void JSONBool_GetEnumerator_mA15666FFF18AADF12912689A521087C98B5B2D1C (void);
// 0x000001EC System.String SimpleJSON.JSONBool::get_Value()
extern void JSONBool_get_Value_m11D2863303B2003F337549581C8A583A6A3B4D74 (void);
// 0x000001ED System.Void SimpleJSON.JSONBool::set_Value(System.String)
extern void JSONBool_set_Value_mA7FB197B21177B947BC3C2940A63724FE532E373 (void);
// 0x000001EE System.Boolean SimpleJSON.JSONBool::get_AsBool()
extern void JSONBool_get_AsBool_mA5617A7BC781E111FBD24A7E53FB34C7DE6F62B6 (void);
// 0x000001EF System.Void SimpleJSON.JSONBool::set_AsBool(System.Boolean)
extern void JSONBool_set_AsBool_mDDD0D1CA7B06DAD37AA13987F46B95C6A1AEF489 (void);
// 0x000001F0 System.Void SimpleJSON.JSONBool::.ctor(System.Boolean)
extern void JSONBool__ctor_m7593DD0B3E9628BA9BB4DE4A0E904BBA6BF151C1 (void);
// 0x000001F1 System.Void SimpleJSON.JSONBool::.ctor(System.String)
extern void JSONBool__ctor_m8E5E6A23827EE2B5DB5E7C86377805ADD8A771B2 (void);
// 0x000001F2 SimpleJSON.JSONNode SimpleJSON.JSONBool::Clone()
extern void JSONBool_Clone_m178E246F0EAC6CC4111AD644E59227A7601C1CD7 (void);
// 0x000001F3 System.Void SimpleJSON.JSONBool::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONBool_WriteToStringBuilder_m7EA9750436CEE42357B6A109B83870F4FC2C403E (void);
// 0x000001F4 System.Boolean SimpleJSON.JSONBool::Equals(System.Object)
extern void JSONBool_Equals_m57A524B4FEBF04285341E4FC42EDEDE7A524F4BB (void);
// 0x000001F5 System.Int32 SimpleJSON.JSONBool::GetHashCode()
extern void JSONBool_GetHashCode_m49A9C417E05DFB795FAE0F38737C7629A3A69A22 (void);
// 0x000001F6 System.Void SimpleJSON.JSONBool::Clear()
extern void JSONBool_Clear_m6016646BC5B21519731150E9F36528CBD753838A (void);
// 0x000001F7 SimpleJSON.JSONNull SimpleJSON.JSONNull::CreateOrGet()
extern void JSONNull_CreateOrGet_m96465EECF543329FFBBC26B552EDB6E43D8DBC90 (void);
// 0x000001F8 System.Void SimpleJSON.JSONNull::.ctor()
extern void JSONNull__ctor_mA4FF0FB3001766DFB7A8D8C23063101AC377F277 (void);
// 0x000001F9 SimpleJSON.JSONNodeType SimpleJSON.JSONNull::get_Tag()
extern void JSONNull_get_Tag_m2A705402C448944F6395AAC88DA5DE7ADF99C4D1 (void);
// 0x000001FA System.Boolean SimpleJSON.JSONNull::get_IsNull()
extern void JSONNull_get_IsNull_m1964A7B7E9C0FD17CDE2E4DFDF6345FE03EBA59B (void);
// 0x000001FB SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONNull::GetEnumerator()
extern void JSONNull_GetEnumerator_m7B4ADD53B56B6663A0832AC611D7B47FA98D1A35 (void);
// 0x000001FC System.String SimpleJSON.JSONNull::get_Value()
extern void JSONNull_get_Value_m76B2E412BD13C31AD68C272CA9AF2E6063F165D3 (void);
// 0x000001FD System.Void SimpleJSON.JSONNull::set_Value(System.String)
extern void JSONNull_set_Value_m713260989D4BA62C8B8CD716893585A1AA88ABE9 (void);
// 0x000001FE System.Boolean SimpleJSON.JSONNull::get_AsBool()
extern void JSONNull_get_AsBool_m5E619608D71A63CBD234CA282B3BB2A7EFF385D5 (void);
// 0x000001FF System.Void SimpleJSON.JSONNull::set_AsBool(System.Boolean)
extern void JSONNull_set_AsBool_m267D1AA7D6003F371DC10DB65EB4E80643BB99F1 (void);
// 0x00000200 SimpleJSON.JSONNode SimpleJSON.JSONNull::Clone()
extern void JSONNull_Clone_m3A730FF65AD2F4141328305059677DBED63A6FF2 (void);
// 0x00000201 System.Boolean SimpleJSON.JSONNull::Equals(System.Object)
extern void JSONNull_Equals_m3063912429EF72D2F9AFAEDF0D0303EB5D60A893 (void);
// 0x00000202 System.Int32 SimpleJSON.JSONNull::GetHashCode()
extern void JSONNull_GetHashCode_m899F43B83E13ABFDE999C3781C45C6C03CC21077 (void);
// 0x00000203 System.Void SimpleJSON.JSONNull::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONNull_WriteToStringBuilder_m66FC8C85A593D6198406753B845BFECA59117B46 (void);
// 0x00000204 System.Void SimpleJSON.JSONNull::.cctor()
extern void JSONNull__cctor_mF5CFD47108FE35E808A8BA6514AA8283BBFD3193 (void);
// 0x00000205 SimpleJSON.JSONNodeType SimpleJSON.JSONLazyCreator::get_Tag()
extern void JSONLazyCreator_get_Tag_m492C25FDFDC35D2EFB17711E1C259976A38BB46E (void);
// 0x00000206 SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONLazyCreator::GetEnumerator()
extern void JSONLazyCreator_GetEnumerator_m6B2038B21158F725FC5B607544FB590A0E313AEC (void);
// 0x00000207 System.Void SimpleJSON.JSONLazyCreator::.ctor(SimpleJSON.JSONNode)
extern void JSONLazyCreator__ctor_m790C925B9AD99D9F574C556397F3C2D868D2F7EE (void);
// 0x00000208 System.Void SimpleJSON.JSONLazyCreator::.ctor(SimpleJSON.JSONNode,System.String)
extern void JSONLazyCreator__ctor_mE9B31AC731EAF8B853DA1671147CD5B9B5C6F10A (void);
// 0x00000209 T SimpleJSON.JSONLazyCreator::Set(T)
// 0x0000020A SimpleJSON.JSONNode SimpleJSON.JSONLazyCreator::get_Item(System.Int32)
extern void JSONLazyCreator_get_Item_m0A09379FDB2D2CB95B781D1E3A03855661A0F446 (void);
// 0x0000020B System.Void SimpleJSON.JSONLazyCreator::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONLazyCreator_set_Item_mE77E182C7C150529FBA7BD013A03F95FDCA25E9E (void);
// 0x0000020C SimpleJSON.JSONNode SimpleJSON.JSONLazyCreator::get_Item(System.String)
extern void JSONLazyCreator_get_Item_m55D3BD8AF905099931604EACC6A907C17AACA753 (void);
// 0x0000020D System.Void SimpleJSON.JSONLazyCreator::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONLazyCreator_set_Item_m8E286CF72FF8622427E69587C30C91BE0D242FCF (void);
// 0x0000020E System.Void SimpleJSON.JSONLazyCreator::Add(SimpleJSON.JSONNode)
extern void JSONLazyCreator_Add_m7393F681C0F1D9F798D3FEAE8C3A37C9F73E0E1B (void);
// 0x0000020F System.Void SimpleJSON.JSONLazyCreator::Add(System.String,SimpleJSON.JSONNode)
extern void JSONLazyCreator_Add_m8762C532A8EBAFF5AE213C3B2A9AC983653C8D37 (void);
// 0x00000210 System.Boolean SimpleJSON.JSONLazyCreator::op_Equality(SimpleJSON.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Equality_mF472937FE1ABFEBCEB8A717561AB198CCB0F75E8 (void);
// 0x00000211 System.Boolean SimpleJSON.JSONLazyCreator::op_Inequality(SimpleJSON.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Inequality_m8C2EB320847B968D52F5E5D96599F734FEEE8ACA (void);
// 0x00000212 System.Boolean SimpleJSON.JSONLazyCreator::Equals(System.Object)
extern void JSONLazyCreator_Equals_m94A17EF99F288D175F1A40DC7B6569643CA84DD9 (void);
// 0x00000213 System.Int32 SimpleJSON.JSONLazyCreator::GetHashCode()
extern void JSONLazyCreator_GetHashCode_m85E38D69ED1F0970E4EE3583196A4E9CD37C9DAF (void);
// 0x00000214 System.Int32 SimpleJSON.JSONLazyCreator::get_AsInt()
extern void JSONLazyCreator_get_AsInt_m8C8C09CC03C6E229A680D004C070734C424DA012 (void);
// 0x00000215 System.Void SimpleJSON.JSONLazyCreator::set_AsInt(System.Int32)
extern void JSONLazyCreator_set_AsInt_m9CEB176958FE16A9213B7828EEAF442F54E70587 (void);
// 0x00000216 System.Single SimpleJSON.JSONLazyCreator::get_AsFloat()
extern void JSONLazyCreator_get_AsFloat_m1EC025D56616385B163C548183783BA8E4FD67E4 (void);
// 0x00000217 System.Void SimpleJSON.JSONLazyCreator::set_AsFloat(System.Single)
extern void JSONLazyCreator_set_AsFloat_mAECCEF9C5598A5EA2204CDD1730D19B073050956 (void);
// 0x00000218 System.Double SimpleJSON.JSONLazyCreator::get_AsDouble()
extern void JSONLazyCreator_get_AsDouble_m019E3ACC054CCE18601A8731E772C86C62C09545 (void);
// 0x00000219 System.Void SimpleJSON.JSONLazyCreator::set_AsDouble(System.Double)
extern void JSONLazyCreator_set_AsDouble_mBFEC4A9E9A946716829F584BE00CFA35D8DBA1E2 (void);
// 0x0000021A System.Int64 SimpleJSON.JSONLazyCreator::get_AsLong()
extern void JSONLazyCreator_get_AsLong_m161CBB667B3FDEA081301BA2A124D453DDCC9857 (void);
// 0x0000021B System.Void SimpleJSON.JSONLazyCreator::set_AsLong(System.Int64)
extern void JSONLazyCreator_set_AsLong_m7B9BDA7A244D499024AD09EC076CE22D8E55B00B (void);
// 0x0000021C System.UInt64 SimpleJSON.JSONLazyCreator::get_AsULong()
extern void JSONLazyCreator_get_AsULong_m66E1A67155C226A193BAB3B3397184FA659427C4 (void);
// 0x0000021D System.Void SimpleJSON.JSONLazyCreator::set_AsULong(System.UInt64)
extern void JSONLazyCreator_set_AsULong_m1C89441A1BD933E6F9B570E5954EAA08B822D6C3 (void);
// 0x0000021E System.Boolean SimpleJSON.JSONLazyCreator::get_AsBool()
extern void JSONLazyCreator_get_AsBool_mE7E32A21C51AFA46EC9BE4F78E96AE20986E7E00 (void);
// 0x0000021F System.Void SimpleJSON.JSONLazyCreator::set_AsBool(System.Boolean)
extern void JSONLazyCreator_set_AsBool_m700796091E7C1940E21834CE4BB1A8FD80017094 (void);
// 0x00000220 SimpleJSON.JSONArray SimpleJSON.JSONLazyCreator::get_AsArray()
extern void JSONLazyCreator_get_AsArray_mF737B0C7548A4C304278366931569BD173D4C2C6 (void);
// 0x00000221 SimpleJSON.JSONObject SimpleJSON.JSONLazyCreator::get_AsObject()
extern void JSONLazyCreator_get_AsObject_m0678D4A1725598B36657E07D1626B8387D3B4C88 (void);
// 0x00000222 System.Void SimpleJSON.JSONLazyCreator::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONLazyCreator_WriteToStringBuilder_m20163F20F4F24E7E82C30AAA1878AE7DE301D0E9 (void);
// 0x00000223 SimpleJSON.JSONNode SimpleJSON.JSON::Parse(System.String)
extern void JSON_Parse_mEAB5874EF687F205543F8469A9FAE7DB3C134421 (void);
static Il2CppMethodPointer s_methodPointers[547] = 
{
	AudioManagerScript_get_Instance_mF84C38805AC2140B48B85F3F771685428EA80C29,
	AudioManagerScript_Awake_m71D69FF1C019AED9716FA8C9FB672A1FE55D46A4,
	AudioManagerScript_Start_mCA8DAB4C8058C91527AA90844292DC065CEF1A63,
	AudioManagerScript_StartGame_m0B09F28E063D9E77698C8D3FB39C356089DAC3DD,
	AudioManagerScript_PlayMainBGM_m0FEC40E0AAD4DC6269FCC37D04A073341C3B302D,
	AudioManagerScript_PlayPowerUpBGM_mDFCDB27A659A55D26186C8B21A8799E5E10D9814,
	AudioManagerScript_StopBGM_mA79AEEEC27D6A3D3550070817EA7522C331B1CDE,
	AudioManagerScript_PlaySFX_m8F546B0F6D46B7F85374EC1CCB7E326FC53DEA0F,
	AudioManagerScript_AdjustVolume_m13990DFC8DA97742FA2E018E78E589E38A61A728,
	AudioManagerScript_IsMute_mE0EBFD6AC34F617593F0C6413EF469136FED19DF,
	AudioManagerScript__ctor_mB7EBDB54102B4DD2ED9186C4F2198DFEA9B1ADDF,
	RegisterUserData__ctor_mC80344269847224DA5072B1EB17627683D189389,
	LoginUserData__ctor_mAD4C8DA646071FFC216DE6EFF402AA77DF95F64B,
	UpdatePasswordData__ctor_m497FBE33C880730D6DA5872C858E6EEBA4254714,
	AuthenticationManagerScript_get_Instance_m143D98B4FF8D71EC28A6BAA3D528768AB50C5C30,
	AuthenticationManagerScript_Awake_mF02810ED29BBA711488E146BDB69CA9B667CC00F,
	AuthenticationManagerScript_Start_m4F950D7A928F5E82340D08BA8C00ECBE718D8A64,
	AuthenticationManagerScript_PlayAsGuest_m310BB1401C420E496E6EC4B77D272E46D8FDE2FF,
	AuthenticationManagerScript_LoginUser_mAB663B37FC1C49389B69928B33F77FFFA0FB1DC2,
	AuthenticationManagerScript_RegisterUser_m1DEF60DE3B6256702FD6DEBE13F4FA3795850BD7,
	AuthenticationManagerScript_UpdateUserPassword_mAB8FAB94F898EC0945BF753FAAA6F74833B1CFD1,
	AuthenticationManagerScript_PostLogin_mEA9EBA2DB1BC45CD94E4998FBE8499C8D62D5ED3,
	AuthenticationManagerScript_InitializeGame_m0680BC7A2C483C7E51CE0CC72EAD2C7C04DEF908,
	AuthenticationManagerScript_PostRegister_m87C6C4318FB74E2BC0464AAD636B548C9C5979CB,
	AuthenticationManagerScript_PostUpdatePassword_mDE1B1EE42E88A7BA7B5307D35BAC7F95F35458F6,
	AuthenticationManagerScript__ctor_m1E1C589C6EDDACCD3DBE10CBB0C894905DFDCF6C,
	U3CPostLoginU3Ed__27__ctor_mEB1BEFE21BAA925B96785C30234697D008F64011,
	U3CPostLoginU3Ed__27_System_IDisposable_Dispose_m2FE2383282696EDB935EEDDBD9834C5AFF8A3DAE,
	U3CPostLoginU3Ed__27_MoveNext_mCC040C0D09EF83758E8D4CCC54FC3F93A27805E9,
	U3CPostLoginU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m58E7651E66624F084279ABF17D4A246276428CFE,
	U3CPostLoginU3Ed__27_System_Collections_IEnumerator_Reset_mADA6D4E611B36BAA2BA3CF7AFA0C6F9B1BAB0B7F,
	U3CPostLoginU3Ed__27_System_Collections_IEnumerator_get_Current_m29813583295C7B6D170E9FCFA005D06633BD3F50,
	U3CInitializeGameU3Ed__28__ctor_m6483DF96F3B8D5010E57BACE7379CD97F6552A8D,
	U3CInitializeGameU3Ed__28_System_IDisposable_Dispose_m66B0054BDB30119B1B18604BDCB93C623E19D178,
	U3CInitializeGameU3Ed__28_MoveNext_mD19D5D71516CAF467A23AF25B71A39728434465E,
	U3CInitializeGameU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7BEC21D79B0B1519085C033FBBE4D4AF59E5B284,
	U3CInitializeGameU3Ed__28_System_Collections_IEnumerator_Reset_m6C14396FDB21283CF4BF45B8EC4EF72848F056D8,
	U3CInitializeGameU3Ed__28_System_Collections_IEnumerator_get_Current_m9FEE2D712034174CD69CE35841120F2210301FED,
	U3CPostRegisterU3Ed__29__ctor_m931F4CA283C7F730F020CB30ADAEB0CD2ADD0F77,
	U3CPostRegisterU3Ed__29_System_IDisposable_Dispose_mB44158C17BA7B6C83846257E355EAAF2E6454C88,
	U3CPostRegisterU3Ed__29_MoveNext_mAD2158B0552AB32BED58502EB21EC6D253AD201D,
	U3CPostRegisterU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6449593DFCD7207A0D1D366835D70272ED696DCE,
	U3CPostRegisterU3Ed__29_System_Collections_IEnumerator_Reset_m25DC991E98C158D77FB873D7478F34797AF3B253,
	U3CPostRegisterU3Ed__29_System_Collections_IEnumerator_get_Current_mCB8DBA443AC25037298A19CBB31BE910ED38F63A,
	U3CPostUpdatePasswordU3Ed__30__ctor_m909F6D547362802555788CF69D5907C4CA4BA188,
	U3CPostUpdatePasswordU3Ed__30_System_IDisposable_Dispose_m89831D42936AAF1DB69688E54ED481E96C1D0844,
	U3CPostUpdatePasswordU3Ed__30_MoveNext_m41ABA5865833A31CFB7DBE8C479D0DCFF878AF22,
	U3CPostUpdatePasswordU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0CB3D36D4861D2E2547B0732E6E75A49BF0D9518,
	U3CPostUpdatePasswordU3Ed__30_System_Collections_IEnumerator_Reset_m39D8102580AB48A2D53BEE3662B43B7035864C14,
	U3CPostUpdatePasswordU3Ed__30_System_Collections_IEnumerator_get_Current_m78D4C232B44854225B661FF119D83E1F104DC408,
	BoundaryScript_Start_mFEB6B58DAFAAF3C7BB2D6F86DE67DCBA9ABC4C3B,
	BoundaryScript_OnTriggerEnter2D_mFD2D0E911CB71C6EFCADD7507CBD3EE883FE740B,
	BoundaryScript_OnTriggerExit2D_m19C6AD58F61A4714DF8572036F130AF29F9ED15A,
	BoundaryScript__ctor_mC7189D2C91CDB69B88B16FC996732B7BEAFFB606,
	CameraScript_Awake_m421586ECC5F1A5029EC370048C8FDC492BA13C65,
	CameraScript_Update_mFD3343EC9B5F59B8D44E7151FA49C7CE88B1936A,
	CameraScript__ctor_m2F555CA91AB185743DD59C5C093A7F6EE339B863,
	ChangeInputScript_Start_mE15210E853535EDEE35639D49D597D622DD9B4CB,
	ChangeInputScript_Update_mC9622E774A1F637A01ED9E6B67618EC1206DC182,
	ChangeInputScript_AssignUINavigation_m6A1F251BE53B3DEB82AD1653E36AA38530B95C84,
	ChangeInputScript__ctor_mE58EFB8844CFE341F0086A05D47F955A7D4A7A1C,
	GameManagerScript_get_Instance_mF64FDD782CB37098D0DB861D1AC3CBE7FCD3115D,
	GameManagerScript_Awake_m8296FABBF41E390DBF0BB4071457A3153854FE01,
	GameManagerScript_StartGame_mA573B61AA3C1CCC72F3BE73F21C03E94DD9C7805,
	GameManagerScript_EndGame_m9F15B54656771FCCDE22769527C1F47DCF427D57,
	GameManagerScript_ProceedToNextLevel_m502AEFA619361B552F5DE7013AA15EBDC8ADBD7A,
	GameManagerScript__ctor_m95960A513907B211C35582552310D03099AB2339,
	NULL,
	GroundScript_Start_mD827FC506EFE4C3BF9A9863368C45393AFA2AAAF,
	GroundScript_OnTriggerEnter2D_mC88505F4A43FF1BB782E0EC2F72431AB80090AE4,
	GroundScript__ctor_m417B8B73D039528B2CA5A35062BFAB97CEC26CF5,
	HTTPRequestHelper_get_Instance_m7CE06EDA42A62C4E5FFBE8515102EA0E3990DE6B,
	HTTPRequestHelper_IsMobileBrowser_m0FC74B25FADCD8A41B3DDE22A22EFA3B2A7F15FD,
	HTTPRequestHelper_openLink_m3FC9E363AA28F6355374FE5700F3FDE2F3AFE3A9,
	HTTPRequestHelper_Awake_m72A3550942537C24D98EDF8D8661E7A6F9243C4B,
	HTTPRequestHelper_GeneratePostCallRequest_mD529C721402C43043FC1C713ECB3C4F6B85CB840,
	HTTPRequestHelper_CheckIfHTTPRunningOnMobile_mD1EF5784E0C3554B68970CC52BD8A76004D31526,
	HTTPRequestHelper_OpenPage_m0F8D16C67A2FD89C7E993E40B724667AF4C5BE88,
	HTTPRequestHelper__ctor_m0084D378615C4BC40B2B5BBC8D3FC1E4626685DB,
	NULL,
	KeyboardClass_focusHandleAction_m4C2FEA3D18106774674F0CD51BE59E18CE3A4F5F,
	KeyboardClass_Start_mB54D10821CC81ABFF646CB111FE5AF2FA46DE503,
	KeyboardClass_ReceiveInputData_mE43EC88640EDE0433E96ABD4DF739C0289E17AEF,
	KeyboardClass_OnSelect_m0B5DBE9AD485B2B5F8CA106DE2DA885FCBE8FBA5,
	KeyboardClass__ctor_m121B5DE9BF5D8774A8EE455719EFF155B13CF2F0,
	ScoreData__ctor_mD8749C727CA2FFBC106EC087E2F5C9B8D94959C1,
	LeaderboardData__ctor_m79824B3B65BE1603FEB490F271C2127D44FEAEFA,
	LeaderboardManagerScript_get_Instance_mA52F4A95BBD5C5570DFDBED8EEC0880CA89AFD92,
	LeaderboardManagerScript_Awake_mC1EC1D1900D13C9B3306E57626710377B7559F11,
	LeaderboardManagerScript_Start_mE2426D550669DC9E564A5008CD902BD60474BC29,
	LeaderboardManagerScript_StartGame_mDDD503DD642AB3708195CB17862B702CA000D5BB,
	LeaderboardManagerScript_UpdateUserScoreToLeaderboard_m49F3A9E1E4F73A02BA54C820042F45A1F134705F,
	LeaderboardManagerScript_GetUserScore_m8756E65C0A74F98E324B876FB5EB196A6ADC795A,
	LeaderboardManagerScript_PostUserScore_m2A579351E7CC3CFE5D7C2F2776852C8CC1AC9447,
	LeaderboardManagerScript_RefreshLeaderboard_m77306CDAE263AAD96AE6C3DCE3CD073A1171C1AE,
	LeaderboardManagerScript_GetLeaderboardScore_mAB14D2288DED2975614F0E520031FC65E44DFF6A,
	LeaderboardManagerScript__ctor_m9A98737963AB5886FFCC53561D044D6FD9BBBB01,
	U3CGetUserScoreU3Ed__12__ctor_m6DF1C76F95FD6F8AF39A06037BED6343E305C2FA,
	U3CGetUserScoreU3Ed__12_System_IDisposable_Dispose_m0B2AEC18400147F12B383DAF905A66A3B8D3DC2A,
	U3CGetUserScoreU3Ed__12_MoveNext_m9DD6297A941BBA0245201A32B612E0336631EB2F,
	U3CGetUserScoreU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1609C0434FBD0F2540E29692CA5145F44797CC82,
	U3CGetUserScoreU3Ed__12_System_Collections_IEnumerator_Reset_m2B00C50A75F503B3E3765CAF36F43001ADF88352,
	U3CGetUserScoreU3Ed__12_System_Collections_IEnumerator_get_Current_m6BBD7CCC844BAD6D354B395F87EF37D6263C3244,
	U3CPostUserScoreU3Ed__13__ctor_m42C6A446F6AB7D73A5DA80DA9AAA9C08741DD4E4,
	U3CPostUserScoreU3Ed__13_System_IDisposable_Dispose_m6791F873B65EA43765AD1F4B6DE6C2F442AF4A75,
	U3CPostUserScoreU3Ed__13_MoveNext_mC1B27DB7543A958120CD99D82134724686A60A67,
	U3CPostUserScoreU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF02B4C4EF83A6A968D93F20EFF5C4D49BDE7C856,
	U3CPostUserScoreU3Ed__13_System_Collections_IEnumerator_Reset_m8C33BD5DDCF2DFB8C0F1322DE16D5BACE21434F1,
	U3CPostUserScoreU3Ed__13_System_Collections_IEnumerator_get_Current_m76FD9530EA41DA3E24A815FAFB2A546BDA2E5398,
	U3CRefreshLeaderboardU3Ed__14__ctor_m3BE38DCD3898B69384135BF8BF3BBECB37CF2BBB,
	U3CRefreshLeaderboardU3Ed__14_System_IDisposable_Dispose_m6DEB89FB0B3983C07F931D6AA157A597A74C4942,
	U3CRefreshLeaderboardU3Ed__14_MoveNext_m40BAFB71DE7800A0CB9FC97152B4FCF20390AB96,
	U3CRefreshLeaderboardU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1C0DA2AEA8CB3577F31398A832DD180B719B4D70,
	U3CRefreshLeaderboardU3Ed__14_System_Collections_IEnumerator_Reset_mE8F2E463A16B08A0DE19F4F2C5E439A6BBA3F4C8,
	U3CRefreshLeaderboardU3Ed__14_System_Collections_IEnumerator_get_Current_mD145646783752DD18CB8862C45B302D896861CFD,
	U3CGetLeaderboardScoreU3Ed__15__ctor_mF4EDA003AED4462A1EF6DDCE4424A4C2EF856FE6,
	U3CGetLeaderboardScoreU3Ed__15_System_IDisposable_Dispose_mA8ABD6A836C443F686F9659B756BD47433198F5C,
	U3CGetLeaderboardScoreU3Ed__15_MoveNext_m72C5829E2163DE1617E0174E04FC40FC0717564A,
	U3CGetLeaderboardScoreU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m668B067B6B3BF7507722752F90FD5DEF954FFB99,
	U3CGetLeaderboardScoreU3Ed__15_System_Collections_IEnumerator_Reset_m450981936121B358A075B602BBA9DB04F71E9548,
	U3CGetLeaderboardScoreU3Ed__15_System_Collections_IEnumerator_get_Current_mF42DAE75A8866E8A9673687025E5FCFCD5ED5D1C,
	LevelTransitionManagerScript_Start_m47636205F4023F58DD08D974A21EB9DB5FC0E541,
	LevelTransitionManagerScript_Update_m31273BC73DE49D2BD5E6DED43BEEBE8C57A3ECCB,
	LevelTransitionManagerScript__ctor_m63749269D84570374F0E1EE90EF9899DA379C240,
	ObstacleColliderScript_Start_m5E40C70E550453F4E6CD99337A65B1AE7A0625BC,
	ObstacleColliderScript_OnTriggerEnter2D_m41B4E7BEB9C6E2628D080709A8A07F0B4DCCA724,
	ObstacleColliderScript__ctor_m111BDCACC86E8D08C565D2D5F7665E24262ED0BF,
	ObstacleCollisionScript_OnTriggerEnter2D_m662BA9D129D5A26B200DEA36AE4C3AC5CAC4DEA9,
	ObstacleCollisionScript__ctor_mD5D61F4C6DF291161540C5B44C8C4DB9DADCE667,
	ObstacleScript_Start_mDD9F1FA599FC21E2B2E521A879887CBF519822F0,
	ObstacleScript_FixedUpdate_m043F033F600C6C981FFFF48B33762A867DEC1CF4,
	ObstacleScript_UpdateMovement_mEEC8D707174AD5EB80A8FF2FBB8BF6740CCEBB0D,
	ObstacleScript_ReconstructObstacle_m400581C4E57CD65A9045EB57DA8CB40AF94896E6,
	ObstacleScript_InitializeObstacle_m2A19A7CE3DC0AEAA52554A38029295A34805A9CA,
	ObstacleScript__ctor_m7F8EE833BA7BB72CE13F43E858BA12E69C2A9929,
	ObstacleSpawner_Start_mBD8A272A8EEAD54E0988D0AE0D7663548DA0C745,
	ObstacleSpawner_PrepopulateObstacles_m35D1C3256AD8CF82564405DB03151FF9DCC8AB7E,
	ObstacleSpawner_HideObstacles_m71005575000323F882FB24AD810FB27448005251,
	ObstacleSpawner_RegenerateObstacles_mE93586B053854CDBD80D95AEFE703ADC06F3857E,
	ObstacleSpawner__ctor_m347D7FB8C7FAEB149F02F9A147EE7E51A28F57ED,
	ParallaxScrollingScript_Start_m196F4A10E8A333276A674CD6E977F17E7629F489,
	ParallaxScrollingScript_FixedUpdate_m28CCAEDF7C66F15788C0076D83EEE04EE16C96A9,
	ParallaxScrollingScript__ctor_mBB5016E34515CC945DFD89E0480F48571982E080,
	PickupScript_Start_m66E57B159A384FD98786299C3F6491839EE9DBA1,
	PickupScript_OnTriggerEnter2D_m9C9FAFC6A648981CB0FDBD94A2452A50E3B31517,
	PickupScript__ctor_m6A7D241A4ED917F6125E78E9ABA819BEF1B28C08,
	Pickup_SpecialScript_Start_mD1DE94500DDB9B1DD1FE09644976555AFBBB15CA,
	Pickup_SpecialScript_OnTriggerEnter2D_mF24197DE1F10CE0CDA59BD5DE03267DE8F711870,
	Pickup_SpecialScript__ctor_mDC73D2E5F8DBAB3E42DF89790B6A252D13E4EB6B,
	PlayerBehavior_Awake_m903075302F0C67B356F133380EDA83FCB714A0C0,
	PlayerBehavior_Start_m0A1F3B0C6099E8205960D3E492BB506E6BAE8EF2,
	PlayerBehavior_FixedUpdate_m534D89C6A5C09E3581800557EA5F859195F78967,
	PlayerBehavior_Update_m2D11A23661FBCC51809DA46B442F4424788BBD7A,
	PlayerBehavior_StartGame_mD929DC0B436D6B9C6BBD129F7F2B20C4DB4A8DD8,
	PlayerBehavior_HandleInput_m28EA7C8587EB1B02362A1FE3BDADC8F8EA41095F,
	PlayerBehavior_ToggleGravity_m3EABDDD05D1796FFD8E0668C502590B34FEE956B,
	PlayerBehavior_IsGravityEnabled_mCA6CA598F2307BCE41AB146DC47D32E3AD2AFE7B,
	PlayerBehavior_ResetToFallingState_m612679EE3801CF7CF2D465DCAE2F725BF76A2360,
	PlayerBehavior_ResetJump_mE473F24CDE47E582BA3DE49AEA232225323C5F1A,
	PlayerBehavior_ResetGravity_m01226E648248720782802B9A56A6E0169362794A,
	PlayerBehavior_UpdateGravityAcceleration_m50D75138B10F716B38ABB9488C7EF0DC7AA836F2,
	PlayerBehavior_UpdateMovement_m92C5873F8E5F005696C991A24D234464C21F93C7,
	PlayerBehavior_RotateBasedOnVelocity_m006E0DE5DC5F7A5095AA177493327AD7B0D4F51A,
	PlayerBehavior_PerformJump_mC677747719CDBAB5B724E4FBD86D2D148EB244ED,
	PlayerBehavior_PerformDeath_mB5027ED6FE256CDBADF206CFB4DCAF24359ED445,
	PlayerBehavior_PerformWinningAction_m905B35B425962798AE958D812EE7965C711AD36C,
	PlayerBehavior_StopJumpTimer_mAFE61DEDE57D69FF756BCE23E17F6434691F8638,
	PlayerBehavior_ResetJumpTimer_m185F6ED4236EEB216DC85888A50C2560FF914D3B,
	PlayerBehavior_EnableGravityOverTime_mD4996D3AB212898BB086648619213689676BDA50,
	PlayerBehavior_UpdateFlyMovement_m5A4CEDBDBD42E791828B0D0198B005C094E59E2C,
	PlayerBehavior_ExecuteFlyAction_m8A406A7AADB16012307E844E2D5B583330707E45,
	PlayerBehavior_RevertFlyAction_m775962B05635245637D37B4F6101A0F57C598D33,
	PlayerBehavior_PlayPickUpAudio_m6BB93858BC23E90FA1B4B1E1B7E4CEA939F27811,
	PlayerBehavior_ChangeAnimationState_m5DE1C31FB8130F879C61DAC5EFA05BDE446DD590,
	PlayerBehavior_CrossFade_ChangeAnimationState_mD446BF74F49BA225E968603D87C39FA01D598CCB,
	PlayerBehavior_SecondFormTransformation_mDFA98DDFD7D3C39FAE702DE13CBBE0CF1DE4913B,
	PlayerBehavior_LockPlayerMovement_mF4A4DFB68D086793C64FC9E8E8F29DDB6BFEC930,
	PlayerBehavior_GetGameStarted_m249CB7DFE875EC4A30C4DA26ABBAE161CCA1A466,
	PlayerBehavior_GetPlayerIsAlive_mC4A534ABE96ACB41F805CF693507ECBFE69A721A,
	PlayerBehavior_GetCurrentJumpState_m59D718BBDAD07005C34891106361D5DA03280EBD,
	PlayerBehavior_AbleToProceedLeaderboardAfterTime_mA4AA9F5151C07EF9F99AFE27CE2F590CF45FDBB5,
	PlayerBehavior_OpenLeaderboardAfterTime_mB2932C80FDD5D9E1299064A2E57DBCEE848E69C7,
	PlayerBehavior__ctor_m85557340768EF11DD81ADAC93770FA77450CF872,
	U3CStopJumpTimerU3Ed__74__ctor_m41E2698C1D7FFA19FB5ED48CDE29E7AAD7CD28B8,
	U3CStopJumpTimerU3Ed__74_System_IDisposable_Dispose_m67AD08EC1B8C83720DAE14A4F7B934CF625176CC,
	U3CStopJumpTimerU3Ed__74_MoveNext_m064E17D5A12CEE924AB579BE877F265CA1DC8DE1,
	U3CStopJumpTimerU3Ed__74_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m081E92295C4CEC6F83563A877AB1DCAF46DC8DA4,
	U3CStopJumpTimerU3Ed__74_System_Collections_IEnumerator_Reset_mD055DB840465665B871A22F89D90E605D7A97096,
	U3CStopJumpTimerU3Ed__74_System_Collections_IEnumerator_get_Current_mD16CC96DB783F8037E654D94D49E6708C51EC914,
	U3CResetJumpTimerU3Ed__75__ctor_mA27CAFE95460E965FDCCC3A1813716299237C8E4,
	U3CResetJumpTimerU3Ed__75_System_IDisposable_Dispose_mD84D349A6DF9DD4BD173257B9AB9BAA218C0ADB4,
	U3CResetJumpTimerU3Ed__75_MoveNext_m868F61272FCD457EC01D1798C1608D5F36AE527A,
	U3CResetJumpTimerU3Ed__75_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7ECDA3855FBBAACCEE6F700AE41B3CDD2EDC486B,
	U3CResetJumpTimerU3Ed__75_System_Collections_IEnumerator_Reset_m9061B620684FA7AA104DB6F03EF4F9EC3096D45F,
	U3CResetJumpTimerU3Ed__75_System_Collections_IEnumerator_get_Current_m978E4CC3F36731396CE35B9C179AE59FD37DC63E,
	U3CEnableGravityOverTimeU3Ed__76__ctor_m2B9DADB5BF8B2D0536C2D164F93FA9867BA796EA,
	U3CEnableGravityOverTimeU3Ed__76_System_IDisposable_Dispose_mA5FDF3F865DA8C4E0905CE6C6025BE69D4070354,
	U3CEnableGravityOverTimeU3Ed__76_MoveNext_mC6E55BDCAA37A0BDD95CBFEDF5611AA25A414658,
	U3CEnableGravityOverTimeU3Ed__76_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m92C0C9DB67D90D2F2C749287610A1B6226FA542C,
	U3CEnableGravityOverTimeU3Ed__76_System_Collections_IEnumerator_Reset_m3932804EE7C65C73D7E8CFC1826D47DCA8C837C2,
	U3CEnableGravityOverTimeU3Ed__76_System_Collections_IEnumerator_get_Current_mAEBADC72DF8EEEFCF94DDE16A776F7D5D8D88794,
	U3CAbleToProceedLeaderboardAfterTimeU3Ed__88__ctor_m40C062923AB126A9E0D45BB3AAA3F17D50DB2530,
	U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_System_IDisposable_Dispose_m373AEAE00494910A9D8D3D849DAA13C2B99048A9,
	U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_MoveNext_m7DC5D8760237ECB2894D187F5810D8B3768B7E97,
	U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1066A678444ADE3726429AD33279DF0180B722D5,
	U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_System_Collections_IEnumerator_Reset_m9119FA446EEE0532BC12BD62858486173A9E51CA,
	U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_System_Collections_IEnumerator_get_Current_m5E07809987A0D316422B77E8F8755D199FA282D7,
	U3COpenLeaderboardAfterTimeU3Ed__89__ctor_m2A4E052679FFFC11969D70AAB464E5B78F3490A2,
	U3COpenLeaderboardAfterTimeU3Ed__89_System_IDisposable_Dispose_mD00875A1A0AE2962BBC8A1E3E19DDF62CBE73CAD,
	U3COpenLeaderboardAfterTimeU3Ed__89_MoveNext_m329DB4B90EB7342841202899B85AAD48BE5CC4EF,
	U3COpenLeaderboardAfterTimeU3Ed__89_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDFD555A751B8003880B730B6AFD29E8A691841F8,
	U3COpenLeaderboardAfterTimeU3Ed__89_System_Collections_IEnumerator_Reset_mC656EA4FBAE012267F228A786F865FA4185A3335,
	U3COpenLeaderboardAfterTimeU3Ed__89_System_Collections_IEnumerator_get_Current_m1C900A1EE59D97FA2DA2A48153B3CE7D14DE2D57,
	ScoreManagerScript_get_Instance_m393A32F09CC80B058D9C17CCA8D8756783E27A37,
	ScoreManagerScript_Awake_m31B40EE0B3C9E853DFDE077E8628A13E451D7FCE,
	ScoreManagerScript_Start_mE9C9CF6F01BF4B494F8726D1014F0399375172AF,
	ScoreManagerScript_Update_m61DCCDF89CB448E6A62126FF862FE507A6000A3E,
	ScoreManagerScript_AddScore_m2A2DCFCE0C7274B40C062161C64F435EF8814F55,
	ScoreManagerScript_RegenerateObstacle_m5BC87374E37350071A1C6973E47051B6F613D117,
	ScoreManagerScript__ctor_mC6D2E5B61E9DFE594F34C983943E01798610ED30,
	U3CRegenerateObstacleU3Ed__19__ctor_m60453B6244F7386B560123E985C11370CBEDECBB,
	U3CRegenerateObstacleU3Ed__19_System_IDisposable_Dispose_mB38500D93A8F554EA8E1909EC0173AE9DD00FCE0,
	U3CRegenerateObstacleU3Ed__19_MoveNext_mB07451936B9CAF55FA75FEED916BA048DB85C098,
	U3CRegenerateObstacleU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m895A09897E7E9C09EE91BEC95D83C82A53DF79E4,
	U3CRegenerateObstacleU3Ed__19_System_Collections_IEnumerator_Reset_m7E60D59453D7C13F7010D83AC8979A2E5DC475B9,
	U3CRegenerateObstacleU3Ed__19_System_Collections_IEnumerator_get_Current_mCA250A006DE45F560B6CE478835B866C20DBE7D6,
	CrossfadeUIScript_Awake_m33237D5647DDC12FCB777BEE4C0D6FE7677AE259,
	CrossfadeUIScript_TriggerTransition_m8C3863B86770A36192134B09B5014F47B72BA67A,
	CrossfadeUIScript_ChangeAnimationState_mA1B07728BBE4498E4EE1ED10D253BF7CC71F4FDC,
	CrossfadeUIScript__ctor_m985700FAEFE8BA20B5459BC8A19008887152868B,
	ExtraValidationCheckBox_Start_m0F102F70D134BE2D33628B9FC0B8A18157BC5CCC,
	ExtraValidationCheckBox_DetectToggleIsSelected_m1EEA2AF6BFC64B72C8158EFB079C181D798FDB45,
	ExtraValidationCheckBox__ctor_mA99A77C5802805A1EAC458E082D3962A2A94B996,
	ExtraValidationDropdown_Start_m48A17259A853C881AEEFF71DE17CFF2574A9877F,
	ExtraValidationDropdown_DetectDropdownIsSelected_mBD9E83A627DC3F4635F1443CDA8512F6D0D74110,
	ExtraValidationDropdown__ctor_mAF11315E4BA56E38A0ED91957BEEF33E33F377CC,
	ExtraValidationInputField_Start_m7EBE4F38E5401D6DF83A57732A182DBA9D92C9D0,
	ExtraValidationInputField_ValidateInputField_m8575A6D2B4AEBF4879243007ACCB4B8F8B250D0B,
	ExtraValidationInputField_DetectInputFieldIsFill_m414E2461D5132D71FD38752E75F42DFE08BD6632,
	ExtraValidationInputField_ValidateNumericNumber_m1CCD9C23BF2FD301A920C31EED8E50ACA28855CE,
	ExtraValidationInputField_ValidateEmail_mD4184B848E1217D438988732C55A5534F4E0B7EB,
	ExtraValidationInputField__ctor_mCB2F80A18E6CD4A613AA004C185990E02BCCAB25,
	NULL,
	NULL,
	LeaderboardDisplayScript_Update_m00FCB0831EBBE4E5D221EF93B6C16C000CDF9376,
	LeaderboardDisplayScript_GenerateLeaderboard_mA93E5C4E016A09F373C65711B972015BAD073E48,
	LeaderboardDisplayScript_CloseLeaderboard_m9B586742A0E3B5AEAEAAFA5F8B3F104EFDE3FD2A,
	LeaderboardDisplayScript_CloseLeaderboardAndRestart_m865B7FCE848AC5B9FE9DE034480AE62814DE92AA,
	LeaderboardDisplayScript__ctor_mDB5183DD7A6E596C1B887ED43E496EE8C830C4B2,
	LoginUIScript_OnLoginButtonClick_m7B579BBAE90F1EE8BCAA633EEF499C875B9DE58E,
	LoginUIScript_LoginButtonPress_m0CE571C7B9164C53A9606738B7BC8FA2482CAA3E,
	LoginUIScript_UIConstruct_m8746B0F4400E0DAF2544FF989E75112FF608EB8A,
	LoginUIScript__ctor_m7FD6FEDE637A93B5D8E243F7A96E23D42B5BF03B,
	U3CLoginButtonPressU3Ed__5__ctor_m935D324B8C35004111454210399F8E62DEE9F2EB,
	U3CLoginButtonPressU3Ed__5_System_IDisposable_Dispose_mBDEB7D0D4575B10290E226D82EBC8EF10CFD99F0,
	U3CLoginButtonPressU3Ed__5_MoveNext_mA392CD45BB03008E84C3BF8F51C594A39405ED47,
	U3CLoginButtonPressU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m82C6CC51A0997A8D3CD01A602CC35F562ACEF118,
	U3CLoginButtonPressU3Ed__5_System_Collections_IEnumerator_Reset_m3024966D9BF1F0E05CE3A175381E492052284959,
	U3CLoginButtonPressU3Ed__5_System_Collections_IEnumerator_get_Current_mAB81582C564DB4C9DD1A7EEB8EA4AF8BCF1A73E0,
	MainTitleUIScript_Start_m9E4B8679626C9C6F9557E1B49F8D25B650B02018,
	MainTitleUIScript_OnPlayAsGuestButtonClick_mA623D5298BBBAD6824538ECC0F814744E6F414B9,
	MainTitleUIScript_OnMuteButtonClick_mF538AD8A57C5D46215B4AEE84D1CB421FC2939C5,
	MainTitleUIScript_OnLoginButtonClick_m4F660564F43EAF95B114F2EDA355E86D606FFC1F,
	MainTitleUIScript__ctor_mFFA7E92D65AEA2AA744E07D96D9257ABC829ABF0,
	RegisterUIScript_Start_m4E3EE5DAAC1F71889B4E62B08054BDC06ADBFBFF,
	RegisterUIScript_OnRegisterButtonClick_mEBC8C696504346FD262A7BA918B08B3FD3919105,
	RegisterUIScript_UIConstruct_mA300E7AE59A5971254B2D0357590E03822F1C4FE,
	RegisterUIScript__ctor_m3E6E86B8303B3834EDAB38BD13FF871A69ABE4AF,
	RegisterUIScript_U3CStartU3Eb__11_0_mA65A23DD36E49ED96B0962932AC2A81EDC81693B,
	ScoreDisplayScript_Start_mFD12910929721600204A1C4608CC4B59C746CA12,
	ScoreDisplayScript_Update_m5CAB913D5E6B99EB03180AC78FA29F4D181B67AF,
	ScoreDisplayScript__ctor_mF034AA41D7D3AC4F193C889E003594989B3DA665,
	UpdatePasswordUIScript_Start_mAC4437267B2216497D2E6A3A4509EFA8FE6394BD,
	UpdatePasswordUIScript_OnUpdatePasswordButtonClick_m13F93CAD3D1CC2D456FD1DCADD762EB003FD1ED9,
	UpdatePasswordUIScript_UIConstruct_m2FAA6588DEE6C0895E0A40CEED25800F6C1030F0,
	UpdatePasswordUIScript__ctor_m20FBDE893E685A769FFD4DC905A3F49E5E91A3BA,
	UpdatePasswordUIScript_U3CStartU3Eb__6_0_m29B697067F37BA21498266921EF007CB013D8DF3,
	WinningScreenFadeUIScript_Awake_mDEF9D76F9CBF5CDB0E0252571E48AB24CE558C87,
	WinningScreenFadeUIScript_FadeInWinningScreen_m259FB62586373BF0631F6DE5F8C0460EBB1F8935,
	WinningScreenFadeUIScript_ChangeAnimationState_m8D0954583A63AE5C45FBC735B8542DFD558DC3F6,
	WinningScreenFadeUIScript__ctor_m20E7A110CB2F7060F0325B76603BF2596145326C,
	WinningCollisionScript_OnTriggerEnter2D_m73350BFE073676C9078E26633F4DE50DE66F8B02,
	WinningCollisionScript__ctor_m791F6D742A94595C28A0B627C057E4156F59F394,
	NULL,
	JSONNode_get_Item_m9B4DBE85D7742250EF918F4450E3CE020AF7C807,
	JSONNode_set_Item_m62DFE58028E55800BF80EFE6E0CE4CDE6DF00CA7,
	JSONNode_get_Item_mB39E17FF10DD6C0D91FCF1EF485A73F434B34F1D,
	JSONNode_set_Item_m9F8D28C00CA46F8C0BD5E43500AEAFC3A1999E07,
	JSONNode_get_Value_m8F31142225AAE4DDEC7ACB74B7FFB30A6DD672F1,
	JSONNode_set_Value_mA046548FC6B7DCBDA4964DB05303EBA0CF2A29CA,
	JSONNode_get_Count_m6FD5676FECC6B26A6384D8294C213C4B032EA269,
	JSONNode_get_IsNumber_m682272D381670DCFA66BD9EE3C0CE3CB161E3AAD,
	JSONNode_get_IsString_mE0D6737C47977364E9F1620CEA98454EAFAEB196,
	JSONNode_get_IsBoolean_mA575C54AD05577A9F45E0F55355C0B40F60BFA24,
	JSONNode_get_IsNull_m85D1B73ABCCE95314768A4E690D941B82D08CD58,
	JSONNode_get_IsArray_m54719E46991451D5C8750D8265D0C6F91EEE97D7,
	JSONNode_get_IsObject_mC9E7B94CB90443629157D7C2AB5ED83510DD0BD1,
	JSONNode_get_Inline_m6791C62A74ACCEA90470A7EC177FA3F12F744F09,
	JSONNode_set_Inline_m5C5C9E182CE521A831EBD2D708568297601AD835,
	JSONNode_Add_m10F03DC3FD017B22B831388D39FF735D5AFE96A4,
	JSONNode_Add_m7DCFC7266181DE46B90ECC7C5DD7D5B7618F9DE2,
	JSONNode_Remove_m49D627BAE273E902C8EB8345BDD7B33E945C2E47,
	JSONNode_Remove_m3B3259E69D33121E4974FE1ACFAEBED4B034C053,
	JSONNode_Remove_mD1FE270ABF87BCD7D09AAA22286AB3C315BAA0EA,
	JSONNode_Clear_m706F6EBA5C07C64C78FADE96D642EA3A108C1978,
	JSONNode_Clone_mB8EAA2AC7D9354F0BB2BFE8154199D2B5787E5EB,
	JSONNode_get_Children_mB9A68101166B1C5176311D2EC1ABCC832248FDFF,
	JSONNode_get_DeepChildren_m3E3CE542A4856D0BD557B7E121B4CAA999C8110C,
	JSONNode_HasKey_m844625544C6ACE8B18B84D836357CF0CB83D4109,
	JSONNode_GetValueOrDefault_m995F3AFE1A8C9A1F6AA41CB6EB3C9D1514BBCD2E,
	JSONNode_ToString_m9E23A359C8B7209DEED6B81DA27C8D0619F9CE98,
	JSONNode_ToString_m9B8D0351B9B14D65ABF1090E6D07E0C69DFB84C3,
	NULL,
	NULL,
	JSONNode_get_Linq_m568F8D242CB58C62C9D8230A0C1F684D70573268,
	JSONNode_get_Keys_m6C1FC7F6C35656362327B486F0426273234584E2,
	JSONNode_get_Values_m1097622D87EFAC54681A9A9796E563DA142D7677,
	JSONNode_get_AsDouble_m0DE7D5E7712FB59B32C1C570EBBF85D597B98656,
	JSONNode_set_AsDouble_mBA8F81DF080D29E60DDE8235663BAFFA688736D5,
	JSONNode_get_AsInt_m743FC99FEF2DA80A8C6CB71137A5FFD28084E0FA,
	JSONNode_set_AsInt_m7CA6313AD31E9E08FEB0B6F8B92AD9A3882D9B75,
	JSONNode_get_AsFloat_m4B2A24C67F4FBF872DEA6360719D854AE1AD1FBB,
	JSONNode_set_AsFloat_m624BDD6CAF17D1709BACFDF3554F2AE11FDD22D1,
	JSONNode_get_AsBool_m15160B79EBEA51E7A2C7C7A23B3540A60434B36D,
	JSONNode_set_AsBool_m0878AF783E25077E17850DD1B4522B17FF08770F,
	JSONNode_get_AsLong_mE7DA8DC8A512CA74C7B372E9FF9CAE347E4BA855,
	JSONNode_set_AsLong_m24D91AED5E8EF3285A82FEDDE9D7D151DBEDAFA6,
	JSONNode_get_AsULong_mEEA4E92743A00ECBEE45404E05CA6C0D082389A3,
	JSONNode_set_AsULong_mE9F8E62D253807F5BE0B2768D4CFB9AA3E232054,
	JSONNode_get_AsArray_m10801C5609C0C024480B49DFA03A4FB16A4E6829,
	JSONNode_get_AsObject_mDE74F42234B130BA44AD17DF9FFC64A2D8DFCD46,
	JSONNode_op_Implicit_m3938223B519495895A5B4E53D60789BB2D4620F2,
	JSONNode_op_Implicit_m110AF33FB2CEF68FF905E93F656AF02222554668,
	JSONNode_op_Implicit_m66EA9B62BBFD5A306C2B95703443F1960338BA01,
	JSONNode_op_Implicit_m98778303F0AD762A7C4FE928D82352A1F8276EBE,
	JSONNode_op_Implicit_m3780006769998A253BE0F8068E68BD88AC692112,
	JSONNode_op_Implicit_mA90C42C0D957F5F284857AE91907D69477331166,
	JSONNode_op_Implicit_mDAC018F58F31AB6333A9852BD0B038152063B3F2,
	JSONNode_op_Implicit_m10A6B0627DE9BC247B0B39554FEA1CD37CA7988C,
	JSONNode_op_Implicit_m45DA7291150C2AD5FF800B3E6BFF747D59D571DC,
	JSONNode_op_Implicit_mA5840D22D4BEDED7AC00F6F5A36938B4C919BFD3,
	JSONNode_op_Implicit_mCDBF45400C2B893A51A9E5ACD26D479C5B942C82,
	JSONNode_op_Implicit_m4A1F576E9A7CB07C3BECEFF936EC66C01E8FF884,
	JSONNode_op_Implicit_mAF077674B97BAF8FD4EB941A63B84D26F6E08EDC,
	JSONNode_op_Implicit_mE4078D38F919268986EF017D4A9EA55FD8CBA826,
	JSONNode_op_Implicit_m92A46C2D66615E97C21041238C09386EDCF0A266,
	JSONNode_op_Equality_mA320F56360F44724C465167318C8FC38F0DCA38F,
	JSONNode_op_Inequality_m00AE17CECA317ECC98958F027D0545C831DA24FE,
	JSONNode_Equals_mC1598F5265D88ECAA3040C537A28FC2DC878BBFF,
	JSONNode_GetHashCode_mF900BB7E50D965906CDBC7210B9097CDE6B89834,
	JSONNode_get_EscapeBuilder_m87ACD2EE75FD74E9E7B67BBB66FCDB0643749628,
	JSONNode_Escape_m5C3A578A1281224A7BDF595470FA706CBC597A68,
	JSONNode_ParseElement_mB69AF0C51A3AD8AF384A016599AEAEEF978533E3,
	JSONNode_Parse_m747C7ACB3E8F42CB32651EAB1506047F02BF4076,
	JSONNode__ctor_mF0692814A0795056AE052B05EF63D6B216B5619E,
	JSONNode__cctor_m0F95CA9754AE155CEC873DEFB28A83E8AAC25990,
	Enumerator_get_IsValid_m078841FA11B23FAA03C8C0F0F5CFE1C7A33CC267_AdjustorThunk,
	Enumerator__ctor_m0A9EDBF3F33AA5FAE87C204FA885D965D16E8A44_AdjustorThunk,
	Enumerator__ctor_mDE6D3B06DF10240F0C637131119C8B09CE9EC4DC_AdjustorThunk,
	Enumerator_get_Current_m6C700A8C86DAB32F9FE845C3BE1E35D17757C65C_AdjustorThunk,
	Enumerator_MoveNext_m095925BE3D881DF1E015997DBEAFE0851722E53E_AdjustorThunk,
	ValueEnumerator__ctor_m1FF9C4191C706E48C3F9A191D0CE02F9BECDCE45_AdjustorThunk,
	ValueEnumerator__ctor_m8143FFB5AD9AB3936DF1A59E3492C2CF81793AF5_AdjustorThunk,
	ValueEnumerator__ctor_mE92051B3948C6A45D4F9265EA6D8329D70649D06_AdjustorThunk,
	ValueEnumerator_get_Current_mAB38FFFD4C1293C61EE73C825D85B7258B61961C_AdjustorThunk,
	ValueEnumerator_MoveNext_mE68E69AFB9D6FA2B41F55B066F3295490EED5589_AdjustorThunk,
	ValueEnumerator_GetEnumerator_mF841522782073D8D5F9782A37B2487BEA63E5E09_AdjustorThunk,
	KeyEnumerator__ctor_m6E59374D6FF2E994DFB9BFCA3759CC335CA48D23_AdjustorThunk,
	KeyEnumerator__ctor_mB67A74FB33429333A208FA269F011122C1AB6395_AdjustorThunk,
	KeyEnumerator__ctor_m5F83F3F252DE00DB6AC2AE59FD501F921C56F224_AdjustorThunk,
	KeyEnumerator_get_Current_m2F8F06987AA5A1600CFA852F7510BF01A3F4DEB4_AdjustorThunk,
	KeyEnumerator_MoveNext_mFF5FB4ECB623B58733E9B85846A296853462F2F4_AdjustorThunk,
	KeyEnumerator_GetEnumerator_m4534071ECBA3E7165BB0DC642EA9E827A29F5A87_AdjustorThunk,
	LinqEnumerator__ctor_m711F6A5F82CC6093E5245A51BFEF95B0355E2FAC,
	LinqEnumerator_get_Current_m7F293F41FED328A040E5BC71C7F4560C3B8C295C,
	LinqEnumerator_System_Collections_IEnumerator_get_Current_m36203F845AA95DEB756D145E03469EF077B79BF1,
	LinqEnumerator_MoveNext_m547BA7FD679ED14C3E698763EBEC58333194B9EE,
	LinqEnumerator_Dispose_mF5270BF1DEAB954E8FC65435DF6D6FD5DBE1C21F,
	LinqEnumerator_GetEnumerator_m356D1E4E1EC2D215962F677F3C6AE538E3CF9A20,
	LinqEnumerator_Reset_mCC3E0E49D5343E81C63A2B4A28A7268F628A2522,
	LinqEnumerator_System_Collections_IEnumerable_GetEnumerator_m713D8655544E9FC2911762CD17D215B5763AE735,
	U3Cget_ChildrenU3Ed__43__ctor_m1E3DEE85215E302020E599B5D498D5F257B841E5,
	U3Cget_ChildrenU3Ed__43_System_IDisposable_Dispose_m3F00470D6E61DBE9CF64CD523D38F5092505403A,
	U3Cget_ChildrenU3Ed__43_MoveNext_m44C527BF6FC399EF5A6818F21D2DE6EE987A3514,
	U3Cget_ChildrenU3Ed__43_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m2E015F7A52E8208F5DBDD306CA59D6DDA7C5E14B,
	U3Cget_ChildrenU3Ed__43_System_Collections_IEnumerator_Reset_m651CEB1CC2F1607A9BC74E77C5B74A593E064F08,
	U3Cget_ChildrenU3Ed__43_System_Collections_IEnumerator_get_Current_m10EB5342637BBF2E59C8EC9A660D591135EB6ACA,
	U3Cget_ChildrenU3Ed__43_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m3C96B2C39F4888749B03B276C3525C5F6F65F689,
	U3Cget_ChildrenU3Ed__43_System_Collections_IEnumerable_GetEnumerator_m348BC5D347B12C3670753E9B49E7A85F98FBD028,
	U3Cget_DeepChildrenU3Ed__45__ctor_mAEF820B12BA0E5ACD7AEAF398E13D86229F33824,
	U3Cget_DeepChildrenU3Ed__45_System_IDisposable_Dispose_m402090148B49E68DBBB71155A79B32A9D5EC0B28,
	U3Cget_DeepChildrenU3Ed__45_MoveNext_m7D0B0392EC628E124531B872D453C5627BE36C16,
	U3Cget_DeepChildrenU3Ed__45_U3CU3Em__Finally1_m7087BAFAFAED9C487312FEFB3224EF8C5AD8C816,
	U3Cget_DeepChildrenU3Ed__45_U3CU3Em__Finally2_m81B9787080A73A39DD00CC34745F1439DA51099A,
	U3Cget_DeepChildrenU3Ed__45_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m6F553695AE45AFE3DB3CB3CB041EBA0C989BD418,
	U3Cget_DeepChildrenU3Ed__45_System_Collections_IEnumerator_Reset_m0A483B8285BDDCD8AAE7A06B530E0B8AD49AFBB7,
	U3Cget_DeepChildrenU3Ed__45_System_Collections_IEnumerator_get_Current_m3405A7516644ED2988C02CD58955E86EF6F4A9C0,
	U3Cget_DeepChildrenU3Ed__45_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m8EC445D3F9C890F98F5EFE538F031CEFD61B518A,
	U3Cget_DeepChildrenU3Ed__45_System_Collections_IEnumerable_GetEnumerator_mDD5EC1B27E01615A2CC852679694910EF826BE7A,
	JSONArray_get_Inline_mCCA0FAE052593C420DF7266D9030F7CBCFF5D43A,
	JSONArray_set_Inline_mCDD35E23A81DC4DF48DB54FA164B2D05CA8CDD5C,
	JSONArray_get_Tag_mA6475F4E3B27AE262DFF53A510E5DD9CA9A82212,
	JSONArray_get_IsArray_m015BCD163D49042D8827D8B5880EAC8720B93DDD,
	JSONArray_GetEnumerator_m107028482740A363BC3AAC4348F729F49094B8D7,
	JSONArray_get_Item_mB56A68792D63083DAE88CF576C4E5C9C6D0E1AEC,
	JSONArray_set_Item_m8D70249BB8D717AD878AFE87F9F86345CD5DE9EF,
	JSONArray_get_Item_m5E864AF954B22AD5841E8CEB58AF68DED45C4C8B,
	JSONArray_set_Item_mD6DB253A66DC2D34179D9773A1ADF25A11FE7DDE,
	JSONArray_get_Count_m8649900F00FBEDB004834FB9D814F848E6C7D72B,
	JSONArray_Add_m14182F062E171457670DB45BE811A46A78B2D685,
	JSONArray_Remove_m65505E642765169F35A9F21760C7EDC39407B9F9,
	JSONArray_Remove_mA79C09C43B22EE2B4965C1D8B3794BAE1957037E,
	JSONArray_Clear_m34F9A0A6EA42F82C69B96EC8651181EC221B8329,
	JSONArray_Clone_m61DBE2A5A6231C2F3FD32D1EB44A1693CB6CD172,
	JSONArray_get_Children_m56D8A696E09F5EEFF1FF9F8BE06011CE5EAFA197,
	JSONArray_WriteToStringBuilder_m64C54C1D36684A7CE5EAB86E3350AE4B811D40C9,
	JSONArray__ctor_m2B6B15254CC3578F4B78A9E5CA656C4D25119512,
	U3Cget_ChildrenU3Ed__24__ctor_mC6643FC9306ABFE30E20A00C08D9738D533DB279,
	U3Cget_ChildrenU3Ed__24_System_IDisposable_Dispose_mDDD5192FEDBCC636E7301392862E8CC8315F155E,
	U3Cget_ChildrenU3Ed__24_MoveNext_mD59D17AA8CF3F462A6A65C89885A61EC5F03C409,
	U3Cget_ChildrenU3Ed__24_U3CU3Em__Finally1_mFEFCD0DE4E0476CA2C2B2897D7BA097B1928B83A,
	U3Cget_ChildrenU3Ed__24_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m6143E2C2D943E8398F986B0FD76F13B995D2BE15,
	U3Cget_ChildrenU3Ed__24_System_Collections_IEnumerator_Reset_m838D58EEF0F67271515CB056BAA7E7467754D04A,
	U3Cget_ChildrenU3Ed__24_System_Collections_IEnumerator_get_Current_m2B6EE2073CADA240627B5A1419ED1DE249E657E8,
	U3Cget_ChildrenU3Ed__24_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mF97261A04403E4BFC8518DB4F85163C057934C26,
	U3Cget_ChildrenU3Ed__24_System_Collections_IEnumerable_GetEnumerator_m29A61F8B9157EB14CF35EFC4495AF8D7CBDB7C39,
	JSONObject_get_Inline_mEBB1EA4AB1EE32D80F2DB4C5C4240DD13593750A,
	JSONObject_set_Inline_mDDC84E63241E060414987E3B8301F7F1AB2967B4,
	JSONObject_get_Tag_m0F8454A3B161D9954B2C1B49945DE0E9041907B6,
	JSONObject_get_IsObject_m6E6B9273D50B23643A6DFBD17FBEA6BD6143E12C,
	JSONObject_GetEnumerator_mC0AAAB9C27211FED4A7DA97BB1D2DEA1777CABD6,
	JSONObject_get_Item_mF7EF352F2BA035D7EFC5902F2DEB12168E586CBD,
	JSONObject_set_Item_m84B17410261AD3D4C06D551BB169126C42331DD8,
	JSONObject_get_Item_m28A01147850D0D538204316C35A6E1A823602C0E,
	JSONObject_set_Item_m17A462CA68BCC6AB9A077BD2D5354C2CD9BAA448,
	JSONObject_get_Count_m5271E2DB59F0A774A71E1313A43FC47D209DE4BD,
	JSONObject_Add_m89A24748B1E3A3BC627C355AD3F946A5541D0D47,
	JSONObject_Remove_m20C910052474109A5D0B8C63DA98648296C1E846,
	JSONObject_Remove_m6A66604EDD3CBED40F44D85BAD15342B133A2B21,
	JSONObject_Remove_m2000AEFD68838D8F04D250DDFF4E5B2B52D2661A,
	JSONObject_Clear_m5332361D04331BEB78ADA3F299212004967EAE05,
	JSONObject_Clone_m61BB490DF0D0A7D29453CFB5EBA356865176978A,
	JSONObject_HasKey_m818D6C5A3544D891213DC9620E4E8F281FB446E3,
	JSONObject_GetValueOrDefault_m0C55823832259A1F2E55088F8204DEAFA1AB03C3,
	JSONObject_get_Children_mDC37B11CC04E072B9E1DFE56A3B176A1E22F8E6D,
	JSONObject_WriteToStringBuilder_m8B9F8C4C58F878A4992C39D21E75CF735DB65F61,
	JSONObject__ctor_m159A8A4A6AD38E7E4786F8EA8FFB9948DF520F92,
	U3CU3Ec__DisplayClass21_0__ctor_mC0EC999DE1ACB8335F31D6D652C0379FF2D065A6,
	U3CU3Ec__DisplayClass21_0_U3CRemoveU3Eb__0_m4DAF41E69ADC8210F10A0E9353204C4C9BB593F0,
	U3Cget_ChildrenU3Ed__27__ctor_m8B8957E246F78B1184815B8C085890B41340711D,
	U3Cget_ChildrenU3Ed__27_System_IDisposable_Dispose_m9FF83D38681D2D54D43AC8A1B50B1A3C8CBFC2FD,
	U3Cget_ChildrenU3Ed__27_MoveNext_m5A6D6FE80E1EBD6AA95570CDC2DEFDF6AC948AF8,
	U3Cget_ChildrenU3Ed__27_U3CU3Em__Finally1_m95B7E9611A12888F1C6F07750B36EDCD06FF0A2D,
	U3Cget_ChildrenU3Ed__27_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mC4ACE0C684380E7DDC8D7EDE0D76749E2B2EBA5F,
	U3Cget_ChildrenU3Ed__27_System_Collections_IEnumerator_Reset_m8F4944630D5FBA4286ED06697CD6932FB73C76EC,
	U3Cget_ChildrenU3Ed__27_System_Collections_IEnumerator_get_Current_mECECB942275EF47371EA737AC48C607909027DA3,
	U3Cget_ChildrenU3Ed__27_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mBA4BBCF2F261D002F57438B8F03BDF263810D942,
	U3Cget_ChildrenU3Ed__27_System_Collections_IEnumerable_GetEnumerator_m0EC44BA64BA97F3BA0655F7809FEC0EFE289C8F0,
	JSONString_get_Tag_m8CD40E0F8FEA6E68B02D661527AAB26E581AD925,
	JSONString_get_IsString_mE62F3299F2BDCFA5704FEFBB6D6174C7D18EFEA0,
	JSONString_GetEnumerator_m2DB5AD3DA3FA00B24522A9083E0EC92952D28C14,
	JSONString_get_Value_m0A72F580BA1EBD98A7592810069BDF215BC737F0,
	JSONString_set_Value_m9B7FCFDF82ED801A434AC208F52582A2B0DF2164,
	JSONString__ctor_mE09DCD65C60DE37D1F0F79BD6902E464EE1960A3,
	JSONString_Clone_m1B348BFEED419701F25E81BCB6F804B6D72B131D,
	JSONString_WriteToStringBuilder_mB338AAA2644A34B03F6DBE6620F8370C64D15627,
	JSONString_Equals_m9709B63863E6B7E1000EF9E0318B2C499D325B92,
	JSONString_GetHashCode_m573FB034DD33C5A300F358A318A0BCA761E35AB1,
	JSONString_Clear_mEABC1369E1751A8B6ACCF8F69DB736B8EDB35446,
	JSONNumber_get_Tag_mC3886860EBAEBED98DF4B58A2E5D2DA1C86D0DF0,
	JSONNumber_get_IsNumber_m55E3251DC2B157F6A06A78047C8B7F6A8710CFC1,
	JSONNumber_GetEnumerator_m48371CA028B151ED79247AB9B49B7B120C172F78,
	JSONNumber_get_Value_m02C36CBFEEDCE75834055B25A6F0F9845B661B98,
	JSONNumber_set_Value_m437CA8FBE33C4A5C6CA4E2951767974987142A01,
	JSONNumber_get_AsDouble_m181BFF83082F0A4A380023EDE5D1EA55B0EC6427,
	JSONNumber_set_AsDouble_m48E57B49311F9720098069A63A056AF5B44AECBE,
	JSONNumber_get_AsLong_m90536472498BCF68E5528450B74FB18631A259CD,
	JSONNumber_set_AsLong_m20401499383A612EAEB52744144B36B7D6ED4A67,
	JSONNumber_get_AsULong_m349C4A11ADBFD63715DC6CF6D3610E9E3004B1B3,
	JSONNumber_set_AsULong_mC3F7E64D6FF0D26CDD05DC9984C756C0521E78CD,
	JSONNumber__ctor_mD0AF1324557B9FC7B1D63842D2CA65EC2D1A643A,
	JSONNumber__ctor_m03A803840EF273185C5C63642FAA70FAF9DFE40E,
	JSONNumber_Clone_m81C53D05C5E0E487036EBE26CE3FDDF79DB88362,
	JSONNumber_WriteToStringBuilder_m48A94BA46F195E37F78A7AA167655AE1E870231A,
	JSONNumber_IsNumeric_mEA4ED6C1587A4D94F5D6F30B12FA284AB56F11EB,
	JSONNumber_Equals_m0D80E8BA86AA78FBB0482F9F053A0BACB65D8D97,
	JSONNumber_GetHashCode_mB10776EF12594532AB12FA617D1BF18FC53C790C,
	JSONNumber_Clear_m63292B1A0C3FE42BB3DABEDF12F25ECE633C66AA,
	JSONBool_get_Tag_m43F4216FE45F0043979093D7BE5FD16C0F462A62,
	JSONBool_get_IsBoolean_mE70A4F82605CAB1E14A70413FC42EF66ADBAB727,
	JSONBool_GetEnumerator_mA15666FFF18AADF12912689A521087C98B5B2D1C,
	JSONBool_get_Value_m11D2863303B2003F337549581C8A583A6A3B4D74,
	JSONBool_set_Value_mA7FB197B21177B947BC3C2940A63724FE532E373,
	JSONBool_get_AsBool_mA5617A7BC781E111FBD24A7E53FB34C7DE6F62B6,
	JSONBool_set_AsBool_mDDD0D1CA7B06DAD37AA13987F46B95C6A1AEF489,
	JSONBool__ctor_m7593DD0B3E9628BA9BB4DE4A0E904BBA6BF151C1,
	JSONBool__ctor_m8E5E6A23827EE2B5DB5E7C86377805ADD8A771B2,
	JSONBool_Clone_m178E246F0EAC6CC4111AD644E59227A7601C1CD7,
	JSONBool_WriteToStringBuilder_m7EA9750436CEE42357B6A109B83870F4FC2C403E,
	JSONBool_Equals_m57A524B4FEBF04285341E4FC42EDEDE7A524F4BB,
	JSONBool_GetHashCode_m49A9C417E05DFB795FAE0F38737C7629A3A69A22,
	JSONBool_Clear_m6016646BC5B21519731150E9F36528CBD753838A,
	JSONNull_CreateOrGet_m96465EECF543329FFBBC26B552EDB6E43D8DBC90,
	JSONNull__ctor_mA4FF0FB3001766DFB7A8D8C23063101AC377F277,
	JSONNull_get_Tag_m2A705402C448944F6395AAC88DA5DE7ADF99C4D1,
	JSONNull_get_IsNull_m1964A7B7E9C0FD17CDE2E4DFDF6345FE03EBA59B,
	JSONNull_GetEnumerator_m7B4ADD53B56B6663A0832AC611D7B47FA98D1A35,
	JSONNull_get_Value_m76B2E412BD13C31AD68C272CA9AF2E6063F165D3,
	JSONNull_set_Value_m713260989D4BA62C8B8CD716893585A1AA88ABE9,
	JSONNull_get_AsBool_m5E619608D71A63CBD234CA282B3BB2A7EFF385D5,
	JSONNull_set_AsBool_m267D1AA7D6003F371DC10DB65EB4E80643BB99F1,
	JSONNull_Clone_m3A730FF65AD2F4141328305059677DBED63A6FF2,
	JSONNull_Equals_m3063912429EF72D2F9AFAEDF0D0303EB5D60A893,
	JSONNull_GetHashCode_m899F43B83E13ABFDE999C3781C45C6C03CC21077,
	JSONNull_WriteToStringBuilder_m66FC8C85A593D6198406753B845BFECA59117B46,
	JSONNull__cctor_mF5CFD47108FE35E808A8BA6514AA8283BBFD3193,
	JSONLazyCreator_get_Tag_m492C25FDFDC35D2EFB17711E1C259976A38BB46E,
	JSONLazyCreator_GetEnumerator_m6B2038B21158F725FC5B607544FB590A0E313AEC,
	JSONLazyCreator__ctor_m790C925B9AD99D9F574C556397F3C2D868D2F7EE,
	JSONLazyCreator__ctor_mE9B31AC731EAF8B853DA1671147CD5B9B5C6F10A,
	NULL,
	JSONLazyCreator_get_Item_m0A09379FDB2D2CB95B781D1E3A03855661A0F446,
	JSONLazyCreator_set_Item_mE77E182C7C150529FBA7BD013A03F95FDCA25E9E,
	JSONLazyCreator_get_Item_m55D3BD8AF905099931604EACC6A907C17AACA753,
	JSONLazyCreator_set_Item_m8E286CF72FF8622427E69587C30C91BE0D242FCF,
	JSONLazyCreator_Add_m7393F681C0F1D9F798D3FEAE8C3A37C9F73E0E1B,
	JSONLazyCreator_Add_m8762C532A8EBAFF5AE213C3B2A9AC983653C8D37,
	JSONLazyCreator_op_Equality_mF472937FE1ABFEBCEB8A717561AB198CCB0F75E8,
	JSONLazyCreator_op_Inequality_m8C2EB320847B968D52F5E5D96599F734FEEE8ACA,
	JSONLazyCreator_Equals_m94A17EF99F288D175F1A40DC7B6569643CA84DD9,
	JSONLazyCreator_GetHashCode_m85E38D69ED1F0970E4EE3583196A4E9CD37C9DAF,
	JSONLazyCreator_get_AsInt_m8C8C09CC03C6E229A680D004C070734C424DA012,
	JSONLazyCreator_set_AsInt_m9CEB176958FE16A9213B7828EEAF442F54E70587,
	JSONLazyCreator_get_AsFloat_m1EC025D56616385B163C548183783BA8E4FD67E4,
	JSONLazyCreator_set_AsFloat_mAECCEF9C5598A5EA2204CDD1730D19B073050956,
	JSONLazyCreator_get_AsDouble_m019E3ACC054CCE18601A8731E772C86C62C09545,
	JSONLazyCreator_set_AsDouble_mBFEC4A9E9A946716829F584BE00CFA35D8DBA1E2,
	JSONLazyCreator_get_AsLong_m161CBB667B3FDEA081301BA2A124D453DDCC9857,
	JSONLazyCreator_set_AsLong_m7B9BDA7A244D499024AD09EC076CE22D8E55B00B,
	JSONLazyCreator_get_AsULong_m66E1A67155C226A193BAB3B3397184FA659427C4,
	JSONLazyCreator_set_AsULong_m1C89441A1BD933E6F9B570E5954EAA08B822D6C3,
	JSONLazyCreator_get_AsBool_mE7E32A21C51AFA46EC9BE4F78E96AE20986E7E00,
	JSONLazyCreator_set_AsBool_m700796091E7C1940E21834CE4BB1A8FD80017094,
	JSONLazyCreator_get_AsArray_mF737B0C7548A4C304278366931569BD173D4C2C6,
	JSONLazyCreator_get_AsObject_m0678D4A1725598B36657E07D1626B8387D3B4C88,
	JSONLazyCreator_WriteToStringBuilder_m20163F20F4F24E7E82C30AAA1878AE7DE301D0E9,
	JSON_Parse_mEAB5874EF687F205543F8469A9FAE7DB3C134421,
};
static const int32_t s_InvokerIndices[547] = 
{
	1988,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1006,
	673,
	1164,
	1176,
	1176,
	1176,
	1176,
	1988,
	1176,
	1176,
	1176,
	657,
	32,
	276,
	807,
	809,
	807,
	807,
	1176,
	997,
	1176,
	1164,
	1145,
	1176,
	1145,
	997,
	1176,
	1164,
	1145,
	1176,
	1145,
	997,
	1176,
	1164,
	1145,
	1176,
	1145,
	997,
	1176,
	1164,
	1145,
	1176,
	1145,
	1176,
	1006,
	1006,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1988,
	1176,
	1176,
	1176,
	1176,
	1176,
	-1,
	1176,
	1006,
	1176,
	1988,
	1996,
	1966,
	1176,
	501,
	1164,
	1006,
	1176,
	1176,
	1275,
	1176,
	1006,
	1006,
	1176,
	1176,
	1176,
	1988,
	1176,
	1176,
	1176,
	1176,
	804,
	495,
	809,
	1145,
	1176,
	997,
	1176,
	1164,
	1145,
	1176,
	1145,
	997,
	1176,
	1164,
	1145,
	1176,
	1145,
	997,
	1176,
	1164,
	1145,
	1176,
	1145,
	997,
	1176,
	1164,
	1145,
	1176,
	1145,
	1176,
	1176,
	1176,
	1176,
	1006,
	1176,
	1006,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1006,
	1176,
	1176,
	1006,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1022,
	1164,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	809,
	809,
	809,
	1176,
	1176,
	1176,
	1006,
	997,
	997,
	1176,
	1022,
	1164,
	1164,
	1134,
	809,
	809,
	1176,
	997,
	1176,
	1164,
	1145,
	1176,
	1145,
	997,
	1176,
	1164,
	1145,
	1176,
	1145,
	997,
	1176,
	1164,
	1145,
	1176,
	1145,
	997,
	1176,
	1164,
	1145,
	1176,
	1145,
	997,
	1176,
	1164,
	1145,
	1176,
	1145,
	1988,
	1176,
	1176,
	1176,
	1022,
	809,
	1176,
	997,
	1176,
	1164,
	1145,
	1176,
	1145,
	1176,
	1176,
	997,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	888,
	888,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	809,
	1176,
	1176,
	997,
	1176,
	1164,
	1145,
	1176,
	1145,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	1176,
	997,
	1176,
	1006,
	1176,
	1134,
	804,
	614,
	807,
	657,
	1145,
	1006,
	1134,
	1164,
	1164,
	1164,
	1164,
	1164,
	1164,
	1164,
	1022,
	657,
	1006,
	807,
	804,
	807,
	1176,
	1145,
	1145,
	1145,
	888,
	501,
	1145,
	804,
	263,
	1184,
	1145,
	1185,
	1186,
	1129,
	991,
	1134,
	997,
	1166,
	1024,
	1164,
	1022,
	1135,
	998,
	1135,
	998,
	1145,
	1145,
	1909,
	1909,
	1903,
	1853,
	1915,
	1944,
	1906,
	1875,
	1907,
	1884,
	1907,
	1884,
	1914,
	1931,
	1896,
	1756,
	1756,
	888,
	1134,
	1988,
	1909,
	1707,
	1909,
	1176,
	2003,
	1164,
	963,
	964,
	1106,
	1164,
	963,
	964,
	1040,
	1145,
	1164,
	1186,
	963,
	964,
	1040,
	1145,
	1164,
	1185,
	1006,
	1106,
	1145,
	1164,
	1176,
	1145,
	1176,
	1145,
	997,
	1176,
	1164,
	1145,
	1176,
	1145,
	1145,
	1145,
	997,
	1176,
	1164,
	1176,
	1176,
	1145,
	1176,
	1145,
	1145,
	1145,
	1164,
	1022,
	1134,
	1164,
	1184,
	804,
	614,
	807,
	657,
	1134,
	657,
	804,
	807,
	1176,
	1145,
	1145,
	263,
	1176,
	997,
	1176,
	1164,
	1176,
	1145,
	1176,
	1145,
	1145,
	1145,
	1164,
	1022,
	1134,
	1164,
	1184,
	807,
	657,
	804,
	614,
	1134,
	657,
	807,
	804,
	807,
	1176,
	1145,
	888,
	501,
	1145,
	263,
	1176,
	1176,
	835,
	997,
	1176,
	1164,
	1176,
	1145,
	1176,
	1145,
	1145,
	1145,
	1134,
	1164,
	1184,
	1145,
	1006,
	1006,
	1145,
	263,
	888,
	1134,
	1176,
	1134,
	1164,
	1184,
	1145,
	1006,
	1129,
	991,
	1135,
	998,
	1135,
	998,
	991,
	1006,
	1145,
	263,
	1931,
	888,
	1134,
	1176,
	1134,
	1164,
	1184,
	1145,
	1006,
	1164,
	1022,
	1022,
	1006,
	1145,
	263,
	888,
	1134,
	1176,
	1988,
	1176,
	1134,
	1164,
	1184,
	1145,
	1006,
	1164,
	1022,
	1145,
	888,
	1134,
	263,
	2003,
	1134,
	1184,
	1006,
	657,
	-1,
	804,
	614,
	807,
	657,
	1006,
	657,
	1756,
	1756,
	888,
	1134,
	1134,
	997,
	1166,
	1024,
	1129,
	991,
	1135,
	998,
	1135,
	998,
	1164,
	1022,
	1145,
	1145,
	263,
	1909,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x06000044, { 0, 4 } },
	{ 0x06000209, { 4, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[5] = 
{
	{ (Il2CppRGCTXDataType)2, 994 },
	{ (Il2CppRGCTXDataType)3, 3053 },
	{ (Il2CppRGCTXDataType)3, 6119 },
	{ (Il2CppRGCTXDataType)3, 3054 },
	{ (Il2CppRGCTXDataType)2, 98 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	547,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	5,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
