﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// System.ThreadStaticAttribute
struct ThreadStaticAttribute_tD3A8F4870EC5B163383AB888C364217A38134F14;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_t28ED70B25F5DBD96AF5C3BA74FEC3A12D0953248_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CEnableGravityOverTimeU3Ed__76_tDBABDCD94E90FACD6D355E83A8FAF6B2804CFD91_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetLeaderboardScoreU3Ed__15_tF0279DBD48BBB5A3CF3999D94F644F10D3E108B9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetUserScoreU3Ed__12_t1AD429FCDB69D638CEC571707DA1437C42386A42_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInitializeGameU3Ed__28_t3864133EE3A1AB00700344DB45586C9589FE8B4D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoginButtonPressU3Ed__5_tF4E5EACBB34656F5D3F398A46275BCC283CC40DE_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COpenLeaderboardAfterTimeU3Ed__89_tC2825CC655F64FABC9CDD8EA139885CBDBF4796F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CPostLoginU3Ed__27_t6A3ADDE3A4F0974F15BFA86356BBE7FAAD4AC023_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CPostRegisterU3Ed__29_tF6B2D4FEE90A4B1B6D060B83633526CFA58BD9B6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CPostUpdatePasswordU3Ed__30_t7DCF75414574AF890E0F9EABCD630A2447121000_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CPostUserScoreU3Ed__13_t1350BA35CEFFD7F394F524484677BDAB1E6C7C9C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRefreshLeaderboardU3Ed__14_tBBA14020E901DACD81905E997A91E48B1EB99043_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRegenerateObstacleU3Ed__19_t39EA583832813CE94425E82627389AB062684519_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CResetJumpTimerU3Ed__75_tD3DA8E1652DAD9E4C83045BF2AA5F5B2F26DB94E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStopJumpTimerU3Ed__74_t2769B2E0AC0323FFEF1FD5744C16C240C26670D0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3Cget_ChildrenU3Ed__24_t73B664F1768B6C6781512427D4D8E87F350AA835_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3Cget_ChildrenU3Ed__27_t39D6D21A5227AF625B1601F0038D844C23D789BC_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3Cget_ChildrenU3Ed__43_tF127CA438370481A61170450D7243D4984DC0504_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3Cget_DeepChildrenU3Ed__45_tFFAE4AD76D933309177202F51E61F95E756BBBC2_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct  Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct  CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct  CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct  DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Reflection.DefaultMemberAttribute
struct  DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.DefaultMemberAttribute::m_memberName
	String_t* ___m_memberName_0;

public:
	inline static int32_t get_offset_of_m_memberName_0() { return static_cast<int32_t>(offsetof(DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5, ___m_memberName_0)); }
	inline String_t* get_m_memberName_0() const { return ___m_memberName_0; }
	inline String_t** get_address_of_m_memberName_0() { return &___m_memberName_0; }
	inline void set_m_memberName_0(String_t* value)
	{
		___m_memberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_memberName_0), (void*)value);
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct  RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct  SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct  StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.ThreadStaticAttribute
struct  ThreadStaticAttribute_tD3A8F4870EC5B163383AB888C364217A38134F14  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct  BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct  IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct  DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct  DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void System.Reflection.DefaultMemberAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7 (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * __this, String_t* ___memberName0, const RuntimeMethod* method);
// System.Void System.ThreadStaticAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThreadStaticAttribute__ctor_m2F60E2FA27DEC1E9FE581440EF3445F3B5E7F16A (ThreadStaticAttribute_tD3A8F4870EC5B163383AB888C364217A38134F14 * __this, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void AuthenticationManagerScript_t0DC27D6231187368FA31C2F93A5D478B6B5ED4FB_CustomAttributesCacheGenerator__registerUserData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AuthenticationManagerScript_t0DC27D6231187368FA31C2F93A5D478B6B5ED4FB_CustomAttributesCacheGenerator__loginUserData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AuthenticationManagerScript_t0DC27D6231187368FA31C2F93A5D478B6B5ED4FB_CustomAttributesCacheGenerator__updatePasswordData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AuthenticationManagerScript_t0DC27D6231187368FA31C2F93A5D478B6B5ED4FB_CustomAttributesCacheGenerator_AuthenticationManagerScript_PostLogin_mEA9EBA2DB1BC45CD94E4998FBE8499C8D62D5ED3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPostLoginU3Ed__27_t6A3ADDE3A4F0974F15BFA86356BBE7FAAD4AC023_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CPostLoginU3Ed__27_t6A3ADDE3A4F0974F15BFA86356BBE7FAAD4AC023_0_0_0_var), NULL);
	}
}
static void AuthenticationManagerScript_t0DC27D6231187368FA31C2F93A5D478B6B5ED4FB_CustomAttributesCacheGenerator_AuthenticationManagerScript_InitializeGame_m0680BC7A2C483C7E51CE0CC72EAD2C7C04DEF908(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInitializeGameU3Ed__28_t3864133EE3A1AB00700344DB45586C9589FE8B4D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CInitializeGameU3Ed__28_t3864133EE3A1AB00700344DB45586C9589FE8B4D_0_0_0_var), NULL);
	}
}
static void AuthenticationManagerScript_t0DC27D6231187368FA31C2F93A5D478B6B5ED4FB_CustomAttributesCacheGenerator_AuthenticationManagerScript_PostRegister_m87C6C4318FB74E2BC0464AAD636B548C9C5979CB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPostRegisterU3Ed__29_tF6B2D4FEE90A4B1B6D060B83633526CFA58BD9B6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CPostRegisterU3Ed__29_tF6B2D4FEE90A4B1B6D060B83633526CFA58BD9B6_0_0_0_var), NULL);
	}
}
static void AuthenticationManagerScript_t0DC27D6231187368FA31C2F93A5D478B6B5ED4FB_CustomAttributesCacheGenerator_AuthenticationManagerScript_PostUpdatePassword_mDE1B1EE42E88A7BA7B5307D35BAC7F95F35458F6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPostUpdatePasswordU3Ed__30_t7DCF75414574AF890E0F9EABCD630A2447121000_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CPostUpdatePasswordU3Ed__30_t7DCF75414574AF890E0F9EABCD630A2447121000_0_0_0_var), NULL);
	}
}
static void U3CPostLoginU3Ed__27_t6A3ADDE3A4F0974F15BFA86356BBE7FAAD4AC023_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPostLoginU3Ed__27_t6A3ADDE3A4F0974F15BFA86356BBE7FAAD4AC023_CustomAttributesCacheGenerator_U3CPostLoginU3Ed__27__ctor_mEB1BEFE21BAA925B96785C30234697D008F64011(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPostLoginU3Ed__27_t6A3ADDE3A4F0974F15BFA86356BBE7FAAD4AC023_CustomAttributesCacheGenerator_U3CPostLoginU3Ed__27_System_IDisposable_Dispose_m2FE2383282696EDB935EEDDBD9834C5AFF8A3DAE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPostLoginU3Ed__27_t6A3ADDE3A4F0974F15BFA86356BBE7FAAD4AC023_CustomAttributesCacheGenerator_U3CPostLoginU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m58E7651E66624F084279ABF17D4A246276428CFE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPostLoginU3Ed__27_t6A3ADDE3A4F0974F15BFA86356BBE7FAAD4AC023_CustomAttributesCacheGenerator_U3CPostLoginU3Ed__27_System_Collections_IEnumerator_Reset_mADA6D4E611B36BAA2BA3CF7AFA0C6F9B1BAB0B7F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPostLoginU3Ed__27_t6A3ADDE3A4F0974F15BFA86356BBE7FAAD4AC023_CustomAttributesCacheGenerator_U3CPostLoginU3Ed__27_System_Collections_IEnumerator_get_Current_m29813583295C7B6D170E9FCFA005D06633BD3F50(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeGameU3Ed__28_t3864133EE3A1AB00700344DB45586C9589FE8B4D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInitializeGameU3Ed__28_t3864133EE3A1AB00700344DB45586C9589FE8B4D_CustomAttributesCacheGenerator_U3CInitializeGameU3Ed__28__ctor_m6483DF96F3B8D5010E57BACE7379CD97F6552A8D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeGameU3Ed__28_t3864133EE3A1AB00700344DB45586C9589FE8B4D_CustomAttributesCacheGenerator_U3CInitializeGameU3Ed__28_System_IDisposable_Dispose_m66B0054BDB30119B1B18604BDCB93C623E19D178(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeGameU3Ed__28_t3864133EE3A1AB00700344DB45586C9589FE8B4D_CustomAttributesCacheGenerator_U3CInitializeGameU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7BEC21D79B0B1519085C033FBBE4D4AF59E5B284(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeGameU3Ed__28_t3864133EE3A1AB00700344DB45586C9589FE8B4D_CustomAttributesCacheGenerator_U3CInitializeGameU3Ed__28_System_Collections_IEnumerator_Reset_m6C14396FDB21283CF4BF45B8EC4EF72848F056D8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeGameU3Ed__28_t3864133EE3A1AB00700344DB45586C9589FE8B4D_CustomAttributesCacheGenerator_U3CInitializeGameU3Ed__28_System_Collections_IEnumerator_get_Current_m9FEE2D712034174CD69CE35841120F2210301FED(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPostRegisterU3Ed__29_tF6B2D4FEE90A4B1B6D060B83633526CFA58BD9B6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPostRegisterU3Ed__29_tF6B2D4FEE90A4B1B6D060B83633526CFA58BD9B6_CustomAttributesCacheGenerator_U3CPostRegisterU3Ed__29__ctor_m931F4CA283C7F730F020CB30ADAEB0CD2ADD0F77(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPostRegisterU3Ed__29_tF6B2D4FEE90A4B1B6D060B83633526CFA58BD9B6_CustomAttributesCacheGenerator_U3CPostRegisterU3Ed__29_System_IDisposable_Dispose_mB44158C17BA7B6C83846257E355EAAF2E6454C88(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPostRegisterU3Ed__29_tF6B2D4FEE90A4B1B6D060B83633526CFA58BD9B6_CustomAttributesCacheGenerator_U3CPostRegisterU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6449593DFCD7207A0D1D366835D70272ED696DCE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPostRegisterU3Ed__29_tF6B2D4FEE90A4B1B6D060B83633526CFA58BD9B6_CustomAttributesCacheGenerator_U3CPostRegisterU3Ed__29_System_Collections_IEnumerator_Reset_m25DC991E98C158D77FB873D7478F34797AF3B253(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPostRegisterU3Ed__29_tF6B2D4FEE90A4B1B6D060B83633526CFA58BD9B6_CustomAttributesCacheGenerator_U3CPostRegisterU3Ed__29_System_Collections_IEnumerator_get_Current_mCB8DBA443AC25037298A19CBB31BE910ED38F63A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPostUpdatePasswordU3Ed__30_t7DCF75414574AF890E0F9EABCD630A2447121000_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPostUpdatePasswordU3Ed__30_t7DCF75414574AF890E0F9EABCD630A2447121000_CustomAttributesCacheGenerator_U3CPostUpdatePasswordU3Ed__30__ctor_m909F6D547362802555788CF69D5907C4CA4BA188(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPostUpdatePasswordU3Ed__30_t7DCF75414574AF890E0F9EABCD630A2447121000_CustomAttributesCacheGenerator_U3CPostUpdatePasswordU3Ed__30_System_IDisposable_Dispose_m89831D42936AAF1DB69688E54ED481E96C1D0844(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPostUpdatePasswordU3Ed__30_t7DCF75414574AF890E0F9EABCD630A2447121000_CustomAttributesCacheGenerator_U3CPostUpdatePasswordU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0CB3D36D4861D2E2547B0732E6E75A49BF0D9518(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPostUpdatePasswordU3Ed__30_t7DCF75414574AF890E0F9EABCD630A2447121000_CustomAttributesCacheGenerator_U3CPostUpdatePasswordU3Ed__30_System_Collections_IEnumerator_Reset_m39D8102580AB48A2D53BEE3662B43B7035864C14(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPostUpdatePasswordU3Ed__30_t7DCF75414574AF890E0F9EABCD630A2447121000_CustomAttributesCacheGenerator_U3CPostUpdatePasswordU3Ed__30_System_Collections_IEnumerator_get_Current_m78D4C232B44854225B661FF119D83E1F104DC408(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void LeaderboardManagerScript_t622C79C46163EB074DE56D5FDCA50C39B3C7BB10_CustomAttributesCacheGenerator__scoreData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeaderboardManagerScript_t622C79C46163EB074DE56D5FDCA50C39B3C7BB10_CustomAttributesCacheGenerator_LeaderboardManagerScript_GetUserScore_m8756E65C0A74F98E324B876FB5EB196A6ADC795A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetUserScoreU3Ed__12_t1AD429FCDB69D638CEC571707DA1437C42386A42_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetUserScoreU3Ed__12_t1AD429FCDB69D638CEC571707DA1437C42386A42_0_0_0_var), NULL);
	}
}
static void LeaderboardManagerScript_t622C79C46163EB074DE56D5FDCA50C39B3C7BB10_CustomAttributesCacheGenerator_LeaderboardManagerScript_PostUserScore_m2A579351E7CC3CFE5D7C2F2776852C8CC1AC9447(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPostUserScoreU3Ed__13_t1350BA35CEFFD7F394F524484677BDAB1E6C7C9C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CPostUserScoreU3Ed__13_t1350BA35CEFFD7F394F524484677BDAB1E6C7C9C_0_0_0_var), NULL);
	}
}
static void LeaderboardManagerScript_t622C79C46163EB074DE56D5FDCA50C39B3C7BB10_CustomAttributesCacheGenerator_LeaderboardManagerScript_RefreshLeaderboard_m77306CDAE263AAD96AE6C3DCE3CD073A1171C1AE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRefreshLeaderboardU3Ed__14_tBBA14020E901DACD81905E997A91E48B1EB99043_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRefreshLeaderboardU3Ed__14_tBBA14020E901DACD81905E997A91E48B1EB99043_0_0_0_var), NULL);
	}
}
static void LeaderboardManagerScript_t622C79C46163EB074DE56D5FDCA50C39B3C7BB10_CustomAttributesCacheGenerator_LeaderboardManagerScript_GetLeaderboardScore_mAB14D2288DED2975614F0E520031FC65E44DFF6A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetLeaderboardScoreU3Ed__15_tF0279DBD48BBB5A3CF3999D94F644F10D3E108B9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetLeaderboardScoreU3Ed__15_tF0279DBD48BBB5A3CF3999D94F644F10D3E108B9_0_0_0_var), NULL);
	}
}
static void U3CGetUserScoreU3Ed__12_t1AD429FCDB69D638CEC571707DA1437C42386A42_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetUserScoreU3Ed__12_t1AD429FCDB69D638CEC571707DA1437C42386A42_CustomAttributesCacheGenerator_U3CGetUserScoreU3Ed__12__ctor_m6DF1C76F95FD6F8AF39A06037BED6343E305C2FA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetUserScoreU3Ed__12_t1AD429FCDB69D638CEC571707DA1437C42386A42_CustomAttributesCacheGenerator_U3CGetUserScoreU3Ed__12_System_IDisposable_Dispose_m0B2AEC18400147F12B383DAF905A66A3B8D3DC2A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetUserScoreU3Ed__12_t1AD429FCDB69D638CEC571707DA1437C42386A42_CustomAttributesCacheGenerator_U3CGetUserScoreU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1609C0434FBD0F2540E29692CA5145F44797CC82(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetUserScoreU3Ed__12_t1AD429FCDB69D638CEC571707DA1437C42386A42_CustomAttributesCacheGenerator_U3CGetUserScoreU3Ed__12_System_Collections_IEnumerator_Reset_m2B00C50A75F503B3E3765CAF36F43001ADF88352(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetUserScoreU3Ed__12_t1AD429FCDB69D638CEC571707DA1437C42386A42_CustomAttributesCacheGenerator_U3CGetUserScoreU3Ed__12_System_Collections_IEnumerator_get_Current_m6BBD7CCC844BAD6D354B395F87EF37D6263C3244(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPostUserScoreU3Ed__13_t1350BA35CEFFD7F394F524484677BDAB1E6C7C9C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPostUserScoreU3Ed__13_t1350BA35CEFFD7F394F524484677BDAB1E6C7C9C_CustomAttributesCacheGenerator_U3CPostUserScoreU3Ed__13__ctor_m42C6A446F6AB7D73A5DA80DA9AAA9C08741DD4E4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPostUserScoreU3Ed__13_t1350BA35CEFFD7F394F524484677BDAB1E6C7C9C_CustomAttributesCacheGenerator_U3CPostUserScoreU3Ed__13_System_IDisposable_Dispose_m6791F873B65EA43765AD1F4B6DE6C2F442AF4A75(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPostUserScoreU3Ed__13_t1350BA35CEFFD7F394F524484677BDAB1E6C7C9C_CustomAttributesCacheGenerator_U3CPostUserScoreU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF02B4C4EF83A6A968D93F20EFF5C4D49BDE7C856(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPostUserScoreU3Ed__13_t1350BA35CEFFD7F394F524484677BDAB1E6C7C9C_CustomAttributesCacheGenerator_U3CPostUserScoreU3Ed__13_System_Collections_IEnumerator_Reset_m8C33BD5DDCF2DFB8C0F1322DE16D5BACE21434F1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPostUserScoreU3Ed__13_t1350BA35CEFFD7F394F524484677BDAB1E6C7C9C_CustomAttributesCacheGenerator_U3CPostUserScoreU3Ed__13_System_Collections_IEnumerator_get_Current_m76FD9530EA41DA3E24A815FAFB2A546BDA2E5398(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRefreshLeaderboardU3Ed__14_tBBA14020E901DACD81905E997A91E48B1EB99043_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRefreshLeaderboardU3Ed__14_tBBA14020E901DACD81905E997A91E48B1EB99043_CustomAttributesCacheGenerator_U3CRefreshLeaderboardU3Ed__14__ctor_m3BE38DCD3898B69384135BF8BF3BBECB37CF2BBB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRefreshLeaderboardU3Ed__14_tBBA14020E901DACD81905E997A91E48B1EB99043_CustomAttributesCacheGenerator_U3CRefreshLeaderboardU3Ed__14_System_IDisposable_Dispose_m6DEB89FB0B3983C07F931D6AA157A597A74C4942(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRefreshLeaderboardU3Ed__14_tBBA14020E901DACD81905E997A91E48B1EB99043_CustomAttributesCacheGenerator_U3CRefreshLeaderboardU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1C0DA2AEA8CB3577F31398A832DD180B719B4D70(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRefreshLeaderboardU3Ed__14_tBBA14020E901DACD81905E997A91E48B1EB99043_CustomAttributesCacheGenerator_U3CRefreshLeaderboardU3Ed__14_System_Collections_IEnumerator_Reset_mE8F2E463A16B08A0DE19F4F2C5E439A6BBA3F4C8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRefreshLeaderboardU3Ed__14_tBBA14020E901DACD81905E997A91E48B1EB99043_CustomAttributesCacheGenerator_U3CRefreshLeaderboardU3Ed__14_System_Collections_IEnumerator_get_Current_mD145646783752DD18CB8862C45B302D896861CFD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetLeaderboardScoreU3Ed__15_tF0279DBD48BBB5A3CF3999D94F644F10D3E108B9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetLeaderboardScoreU3Ed__15_tF0279DBD48BBB5A3CF3999D94F644F10D3E108B9_CustomAttributesCacheGenerator_U3CGetLeaderboardScoreU3Ed__15__ctor_mF4EDA003AED4462A1EF6DDCE4424A4C2EF856FE6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetLeaderboardScoreU3Ed__15_tF0279DBD48BBB5A3CF3999D94F644F10D3E108B9_CustomAttributesCacheGenerator_U3CGetLeaderboardScoreU3Ed__15_System_IDisposable_Dispose_mA8ABD6A836C443F686F9659B756BD47433198F5C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetLeaderboardScoreU3Ed__15_tF0279DBD48BBB5A3CF3999D94F644F10D3E108B9_CustomAttributesCacheGenerator_U3CGetLeaderboardScoreU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m668B067B6B3BF7507722752F90FD5DEF954FFB99(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetLeaderboardScoreU3Ed__15_tF0279DBD48BBB5A3CF3999D94F644F10D3E108B9_CustomAttributesCacheGenerator_U3CGetLeaderboardScoreU3Ed__15_System_Collections_IEnumerator_Reset_m450981936121B358A075B602BBA9DB04F71E9548(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetLeaderboardScoreU3Ed__15_tF0279DBD48BBB5A3CF3999D94F644F10D3E108B9_CustomAttributesCacheGenerator_U3CGetLeaderboardScoreU3Ed__15_System_Collections_IEnumerator_get_Current_mF42DAE75A8866E8A9673687025E5FCFCD5ED5D1C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PlayerBehavior_t9DE38691D7AF23D88A278979D97CA3EA92E0EF86_CustomAttributesCacheGenerator_PlayerBehavior_StopJumpTimer_mAFE61DEDE57D69FF756BCE23E17F6434691F8638(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStopJumpTimerU3Ed__74_t2769B2E0AC0323FFEF1FD5744C16C240C26670D0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStopJumpTimerU3Ed__74_t2769B2E0AC0323FFEF1FD5744C16C240C26670D0_0_0_0_var), NULL);
	}
}
static void PlayerBehavior_t9DE38691D7AF23D88A278979D97CA3EA92E0EF86_CustomAttributesCacheGenerator_PlayerBehavior_ResetJumpTimer_m185F6ED4236EEB216DC85888A50C2560FF914D3B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CResetJumpTimerU3Ed__75_tD3DA8E1652DAD9E4C83045BF2AA5F5B2F26DB94E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CResetJumpTimerU3Ed__75_tD3DA8E1652DAD9E4C83045BF2AA5F5B2F26DB94E_0_0_0_var), NULL);
	}
}
static void PlayerBehavior_t9DE38691D7AF23D88A278979D97CA3EA92E0EF86_CustomAttributesCacheGenerator_PlayerBehavior_EnableGravityOverTime_mD4996D3AB212898BB086648619213689676BDA50(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CEnableGravityOverTimeU3Ed__76_tDBABDCD94E90FACD6D355E83A8FAF6B2804CFD91_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CEnableGravityOverTimeU3Ed__76_tDBABDCD94E90FACD6D355E83A8FAF6B2804CFD91_0_0_0_var), NULL);
	}
}
static void PlayerBehavior_t9DE38691D7AF23D88A278979D97CA3EA92E0EF86_CustomAttributesCacheGenerator_PlayerBehavior_AbleToProceedLeaderboardAfterTime_mA4AA9F5151C07EF9F99AFE27CE2F590CF45FDBB5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_t28ED70B25F5DBD96AF5C3BA74FEC3A12D0953248_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_t28ED70B25F5DBD96AF5C3BA74FEC3A12D0953248_0_0_0_var), NULL);
	}
}
static void PlayerBehavior_t9DE38691D7AF23D88A278979D97CA3EA92E0EF86_CustomAttributesCacheGenerator_PlayerBehavior_OpenLeaderboardAfterTime_mB2932C80FDD5D9E1299064A2E57DBCEE848E69C7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COpenLeaderboardAfterTimeU3Ed__89_tC2825CC655F64FABC9CDD8EA139885CBDBF4796F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3COpenLeaderboardAfterTimeU3Ed__89_tC2825CC655F64FABC9CDD8EA139885CBDBF4796F_0_0_0_var), NULL);
	}
}
static void U3CStopJumpTimerU3Ed__74_t2769B2E0AC0323FFEF1FD5744C16C240C26670D0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStopJumpTimerU3Ed__74_t2769B2E0AC0323FFEF1FD5744C16C240C26670D0_CustomAttributesCacheGenerator_U3CStopJumpTimerU3Ed__74__ctor_m41E2698C1D7FFA19FB5ED48CDE29E7AAD7CD28B8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStopJumpTimerU3Ed__74_t2769B2E0AC0323FFEF1FD5744C16C240C26670D0_CustomAttributesCacheGenerator_U3CStopJumpTimerU3Ed__74_System_IDisposable_Dispose_m67AD08EC1B8C83720DAE14A4F7B934CF625176CC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStopJumpTimerU3Ed__74_t2769B2E0AC0323FFEF1FD5744C16C240C26670D0_CustomAttributesCacheGenerator_U3CStopJumpTimerU3Ed__74_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m081E92295C4CEC6F83563A877AB1DCAF46DC8DA4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStopJumpTimerU3Ed__74_t2769B2E0AC0323FFEF1FD5744C16C240C26670D0_CustomAttributesCacheGenerator_U3CStopJumpTimerU3Ed__74_System_Collections_IEnumerator_Reset_mD055DB840465665B871A22F89D90E605D7A97096(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStopJumpTimerU3Ed__74_t2769B2E0AC0323FFEF1FD5744C16C240C26670D0_CustomAttributesCacheGenerator_U3CStopJumpTimerU3Ed__74_System_Collections_IEnumerator_get_Current_mD16CC96DB783F8037E654D94D49E6708C51EC914(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResetJumpTimerU3Ed__75_tD3DA8E1652DAD9E4C83045BF2AA5F5B2F26DB94E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CResetJumpTimerU3Ed__75_tD3DA8E1652DAD9E4C83045BF2AA5F5B2F26DB94E_CustomAttributesCacheGenerator_U3CResetJumpTimerU3Ed__75__ctor_mA27CAFE95460E965FDCCC3A1813716299237C8E4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResetJumpTimerU3Ed__75_tD3DA8E1652DAD9E4C83045BF2AA5F5B2F26DB94E_CustomAttributesCacheGenerator_U3CResetJumpTimerU3Ed__75_System_IDisposable_Dispose_mD84D349A6DF9DD4BD173257B9AB9BAA218C0ADB4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResetJumpTimerU3Ed__75_tD3DA8E1652DAD9E4C83045BF2AA5F5B2F26DB94E_CustomAttributesCacheGenerator_U3CResetJumpTimerU3Ed__75_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7ECDA3855FBBAACCEE6F700AE41B3CDD2EDC486B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResetJumpTimerU3Ed__75_tD3DA8E1652DAD9E4C83045BF2AA5F5B2F26DB94E_CustomAttributesCacheGenerator_U3CResetJumpTimerU3Ed__75_System_Collections_IEnumerator_Reset_m9061B620684FA7AA104DB6F03EF4F9EC3096D45F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResetJumpTimerU3Ed__75_tD3DA8E1652DAD9E4C83045BF2AA5F5B2F26DB94E_CustomAttributesCacheGenerator_U3CResetJumpTimerU3Ed__75_System_Collections_IEnumerator_get_Current_m978E4CC3F36731396CE35B9C179AE59FD37DC63E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnableGravityOverTimeU3Ed__76_tDBABDCD94E90FACD6D355E83A8FAF6B2804CFD91_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CEnableGravityOverTimeU3Ed__76_tDBABDCD94E90FACD6D355E83A8FAF6B2804CFD91_CustomAttributesCacheGenerator_U3CEnableGravityOverTimeU3Ed__76__ctor_m2B9DADB5BF8B2D0536C2D164F93FA9867BA796EA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnableGravityOverTimeU3Ed__76_tDBABDCD94E90FACD6D355E83A8FAF6B2804CFD91_CustomAttributesCacheGenerator_U3CEnableGravityOverTimeU3Ed__76_System_IDisposable_Dispose_mA5FDF3F865DA8C4E0905CE6C6025BE69D4070354(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnableGravityOverTimeU3Ed__76_tDBABDCD94E90FACD6D355E83A8FAF6B2804CFD91_CustomAttributesCacheGenerator_U3CEnableGravityOverTimeU3Ed__76_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m92C0C9DB67D90D2F2C749287610A1B6226FA542C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnableGravityOverTimeU3Ed__76_tDBABDCD94E90FACD6D355E83A8FAF6B2804CFD91_CustomAttributesCacheGenerator_U3CEnableGravityOverTimeU3Ed__76_System_Collections_IEnumerator_Reset_m3932804EE7C65C73D7E8CFC1826D47DCA8C837C2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnableGravityOverTimeU3Ed__76_tDBABDCD94E90FACD6D355E83A8FAF6B2804CFD91_CustomAttributesCacheGenerator_U3CEnableGravityOverTimeU3Ed__76_System_Collections_IEnumerator_get_Current_mAEBADC72DF8EEEFCF94DDE16A776F7D5D8D88794(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_t28ED70B25F5DBD96AF5C3BA74FEC3A12D0953248_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_t28ED70B25F5DBD96AF5C3BA74FEC3A12D0953248_CustomAttributesCacheGenerator_U3CAbleToProceedLeaderboardAfterTimeU3Ed__88__ctor_m40C062923AB126A9E0D45BB3AAA3F17D50DB2530(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_t28ED70B25F5DBD96AF5C3BA74FEC3A12D0953248_CustomAttributesCacheGenerator_U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_System_IDisposable_Dispose_m373AEAE00494910A9D8D3D849DAA13C2B99048A9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_t28ED70B25F5DBD96AF5C3BA74FEC3A12D0953248_CustomAttributesCacheGenerator_U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1066A678444ADE3726429AD33279DF0180B722D5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_t28ED70B25F5DBD96AF5C3BA74FEC3A12D0953248_CustomAttributesCacheGenerator_U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_System_Collections_IEnumerator_Reset_m9119FA446EEE0532BC12BD62858486173A9E51CA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_t28ED70B25F5DBD96AF5C3BA74FEC3A12D0953248_CustomAttributesCacheGenerator_U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_System_Collections_IEnumerator_get_Current_m5E07809987A0D316422B77E8F8755D199FA282D7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COpenLeaderboardAfterTimeU3Ed__89_tC2825CC655F64FABC9CDD8EA139885CBDBF4796F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COpenLeaderboardAfterTimeU3Ed__89_tC2825CC655F64FABC9CDD8EA139885CBDBF4796F_CustomAttributesCacheGenerator_U3COpenLeaderboardAfterTimeU3Ed__89__ctor_m2A4E052679FFFC11969D70AAB464E5B78F3490A2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COpenLeaderboardAfterTimeU3Ed__89_tC2825CC655F64FABC9CDD8EA139885CBDBF4796F_CustomAttributesCacheGenerator_U3COpenLeaderboardAfterTimeU3Ed__89_System_IDisposable_Dispose_mD00875A1A0AE2962BBC8A1E3E19DDF62CBE73CAD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COpenLeaderboardAfterTimeU3Ed__89_tC2825CC655F64FABC9CDD8EA139885CBDBF4796F_CustomAttributesCacheGenerator_U3COpenLeaderboardAfterTimeU3Ed__89_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDFD555A751B8003880B730B6AFD29E8A691841F8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COpenLeaderboardAfterTimeU3Ed__89_tC2825CC655F64FABC9CDD8EA139885CBDBF4796F_CustomAttributesCacheGenerator_U3COpenLeaderboardAfterTimeU3Ed__89_System_Collections_IEnumerator_Reset_mC656EA4FBAE012267F228A786F865FA4185A3335(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COpenLeaderboardAfterTimeU3Ed__89_tC2825CC655F64FABC9CDD8EA139885CBDBF4796F_CustomAttributesCacheGenerator_U3COpenLeaderboardAfterTimeU3Ed__89_System_Collections_IEnumerator_get_Current_m1C900A1EE59D97FA2DA2A48153B3CE7D14DE2D57(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ScoreManagerScript_tC5BCE3E4E48F43B3125D1A9D6A5FD79E79A4A506_CustomAttributesCacheGenerator_ScoreManagerScript_RegenerateObstacle_m5BC87374E37350071A1C6973E47051B6F613D117(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRegenerateObstacleU3Ed__19_t39EA583832813CE94425E82627389AB062684519_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRegenerateObstacleU3Ed__19_t39EA583832813CE94425E82627389AB062684519_0_0_0_var), NULL);
	}
}
static void U3CRegenerateObstacleU3Ed__19_t39EA583832813CE94425E82627389AB062684519_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRegenerateObstacleU3Ed__19_t39EA583832813CE94425E82627389AB062684519_CustomAttributesCacheGenerator_U3CRegenerateObstacleU3Ed__19__ctor_m60453B6244F7386B560123E985C11370CBEDECBB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRegenerateObstacleU3Ed__19_t39EA583832813CE94425E82627389AB062684519_CustomAttributesCacheGenerator_U3CRegenerateObstacleU3Ed__19_System_IDisposable_Dispose_mB38500D93A8F554EA8E1909EC0173AE9DD00FCE0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRegenerateObstacleU3Ed__19_t39EA583832813CE94425E82627389AB062684519_CustomAttributesCacheGenerator_U3CRegenerateObstacleU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m895A09897E7E9C09EE91BEC95D83C82A53DF79E4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRegenerateObstacleU3Ed__19_t39EA583832813CE94425E82627389AB062684519_CustomAttributesCacheGenerator_U3CRegenerateObstacleU3Ed__19_System_Collections_IEnumerator_Reset_m7E60D59453D7C13F7010D83AC8979A2E5DC475B9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRegenerateObstacleU3Ed__19_t39EA583832813CE94425E82627389AB062684519_CustomAttributesCacheGenerator_U3CRegenerateObstacleU3Ed__19_System_Collections_IEnumerator_get_Current_mCA250A006DE45F560B6CE478835B866C20DBE7D6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void LoginUIScript_t030E5A2DFD6F6DC1DC5C99CCB178135E124E7F31_CustomAttributesCacheGenerator_LoginUIScript_LoginButtonPress_m0CE571C7B9164C53A9606738B7BC8FA2482CAA3E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoginButtonPressU3Ed__5_tF4E5EACBB34656F5D3F398A46275BCC283CC40DE_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoginButtonPressU3Ed__5_tF4E5EACBB34656F5D3F398A46275BCC283CC40DE_0_0_0_var), NULL);
	}
}
static void U3CLoginButtonPressU3Ed__5_tF4E5EACBB34656F5D3F398A46275BCC283CC40DE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoginButtonPressU3Ed__5_tF4E5EACBB34656F5D3F398A46275BCC283CC40DE_CustomAttributesCacheGenerator_U3CLoginButtonPressU3Ed__5__ctor_m935D324B8C35004111454210399F8E62DEE9F2EB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoginButtonPressU3Ed__5_tF4E5EACBB34656F5D3F398A46275BCC283CC40DE_CustomAttributesCacheGenerator_U3CLoginButtonPressU3Ed__5_System_IDisposable_Dispose_mBDEB7D0D4575B10290E226D82EBC8EF10CFD99F0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoginButtonPressU3Ed__5_tF4E5EACBB34656F5D3F398A46275BCC283CC40DE_CustomAttributesCacheGenerator_U3CLoginButtonPressU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m82C6CC51A0997A8D3CD01A602CC35F562ACEF118(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoginButtonPressU3Ed__5_tF4E5EACBB34656F5D3F398A46275BCC283CC40DE_CustomAttributesCacheGenerator_U3CLoginButtonPressU3Ed__5_System_Collections_IEnumerator_Reset_m3024966D9BF1F0E05CE3A175381E492052284959(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoginButtonPressU3Ed__5_tF4E5EACBB34656F5D3F398A46275BCC283CC40DE_CustomAttributesCacheGenerator_U3CLoginButtonPressU3Ed__5_System_Collections_IEnumerator_get_Current_mAB81582C564DB4C9DD1A7EEB8EA4AF8BCF1A73E0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void RegisterUIScript_tD044E84DB9D96298A96CD5D1492DAE81B2544C39_CustomAttributesCacheGenerator_RegisterUIScript_U3CStartU3Eb__11_0_mA65A23DD36E49ED96B0962932AC2A81EDC81693B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UpdatePasswordUIScript_t8CA6FD51A09849C85599830D10EADB5892E51363_CustomAttributesCacheGenerator_UpdatePasswordUIScript_U3CStartU3Eb__6_0_m29B697067F37BA21498266921EF007CB013D8DF3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JSONNode_tDEACFC45BBAB7B7189263CF1BA7D4B7FE179132A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void JSONNode_tDEACFC45BBAB7B7189263CF1BA7D4B7FE179132A_CustomAttributesCacheGenerator_m_EscapeBuilder(CustomAttributesCache* cache)
{
	{
		ThreadStaticAttribute_tD3A8F4870EC5B163383AB888C364217A38134F14 * tmp = (ThreadStaticAttribute_tD3A8F4870EC5B163383AB888C364217A38134F14 *)cache->attributes[0];
		ThreadStaticAttribute__ctor_m2F60E2FA27DEC1E9FE581440EF3445F3B5E7F16A(tmp, NULL);
	}
}
static void JSONNode_tDEACFC45BBAB7B7189263CF1BA7D4B7FE179132A_CustomAttributesCacheGenerator_JSONNode_get_Children_mB9A68101166B1C5176311D2EC1ABCC832248FDFF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3Cget_ChildrenU3Ed__43_tF127CA438370481A61170450D7243D4984DC0504_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3Cget_ChildrenU3Ed__43_tF127CA438370481A61170450D7243D4984DC0504_0_0_0_var), NULL);
	}
}
static void JSONNode_tDEACFC45BBAB7B7189263CF1BA7D4B7FE179132A_CustomAttributesCacheGenerator_JSONNode_get_DeepChildren_m3E3CE542A4856D0BD557B7E121B4CAA999C8110C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3Cget_DeepChildrenU3Ed__45_tFFAE4AD76D933309177202F51E61F95E756BBBC2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3Cget_DeepChildrenU3Ed__45_tFFAE4AD76D933309177202F51E61F95E756BBBC2_0_0_0_var), NULL);
	}
}
static void U3Cget_ChildrenU3Ed__43_tF127CA438370481A61170450D7243D4984DC0504_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__43_tF127CA438370481A61170450D7243D4984DC0504_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__43__ctor_m1E3DEE85215E302020E599B5D498D5F257B841E5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__43_tF127CA438370481A61170450D7243D4984DC0504_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__43_System_IDisposable_Dispose_m3F00470D6E61DBE9CF64CD523D38F5092505403A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__43_tF127CA438370481A61170450D7243D4984DC0504_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__43_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m2E015F7A52E8208F5DBDD306CA59D6DDA7C5E14B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__43_tF127CA438370481A61170450D7243D4984DC0504_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__43_System_Collections_IEnumerator_Reset_m651CEB1CC2F1607A9BC74E77C5B74A593E064F08(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__43_tF127CA438370481A61170450D7243D4984DC0504_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__43_System_Collections_IEnumerator_get_Current_m10EB5342637BBF2E59C8EC9A660D591135EB6ACA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__43_tF127CA438370481A61170450D7243D4984DC0504_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__43_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m3C96B2C39F4888749B03B276C3525C5F6F65F689(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__43_tF127CA438370481A61170450D7243D4984DC0504_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__43_System_Collections_IEnumerable_GetEnumerator_m348BC5D347B12C3670753E9B49E7A85F98FBD028(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_DeepChildrenU3Ed__45_tFFAE4AD76D933309177202F51E61F95E756BBBC2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3Cget_DeepChildrenU3Ed__45_tFFAE4AD76D933309177202F51E61F95E756BBBC2_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__45__ctor_mAEF820B12BA0E5ACD7AEAF398E13D86229F33824(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_DeepChildrenU3Ed__45_tFFAE4AD76D933309177202F51E61F95E756BBBC2_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__45_System_IDisposable_Dispose_m402090148B49E68DBBB71155A79B32A9D5EC0B28(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_DeepChildrenU3Ed__45_tFFAE4AD76D933309177202F51E61F95E756BBBC2_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__45_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m6F553695AE45AFE3DB3CB3CB041EBA0C989BD418(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_DeepChildrenU3Ed__45_tFFAE4AD76D933309177202F51E61F95E756BBBC2_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__45_System_Collections_IEnumerator_Reset_m0A483B8285BDDCD8AAE7A06B530E0B8AD49AFBB7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_DeepChildrenU3Ed__45_tFFAE4AD76D933309177202F51E61F95E756BBBC2_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__45_System_Collections_IEnumerator_get_Current_m3405A7516644ED2988C02CD58955E86EF6F4A9C0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_DeepChildrenU3Ed__45_tFFAE4AD76D933309177202F51E61F95E756BBBC2_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__45_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m8EC445D3F9C890F98F5EFE538F031CEFD61B518A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_DeepChildrenU3Ed__45_tFFAE4AD76D933309177202F51E61F95E756BBBC2_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__45_System_Collections_IEnumerable_GetEnumerator_mDD5EC1B27E01615A2CC852679694910EF826BE7A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void JSONArray_t478FF5B6520D2CAB00FA50A61F808A3E09F928A0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void JSONArray_t478FF5B6520D2CAB00FA50A61F808A3E09F928A0_CustomAttributesCacheGenerator_JSONArray_get_Children_m56D8A696E09F5EEFF1FF9F8BE06011CE5EAFA197(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3Cget_ChildrenU3Ed__24_t73B664F1768B6C6781512427D4D8E87F350AA835_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3Cget_ChildrenU3Ed__24_t73B664F1768B6C6781512427D4D8E87F350AA835_0_0_0_var), NULL);
	}
}
static void U3Cget_ChildrenU3Ed__24_t73B664F1768B6C6781512427D4D8E87F350AA835_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__24_t73B664F1768B6C6781512427D4D8E87F350AA835_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__24__ctor_mC6643FC9306ABFE30E20A00C08D9738D533DB279(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__24_t73B664F1768B6C6781512427D4D8E87F350AA835_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__24_System_IDisposable_Dispose_mDDD5192FEDBCC636E7301392862E8CC8315F155E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__24_t73B664F1768B6C6781512427D4D8E87F350AA835_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__24_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m6143E2C2D943E8398F986B0FD76F13B995D2BE15(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__24_t73B664F1768B6C6781512427D4D8E87F350AA835_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__24_System_Collections_IEnumerator_Reset_m838D58EEF0F67271515CB056BAA7E7467754D04A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__24_t73B664F1768B6C6781512427D4D8E87F350AA835_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__24_System_Collections_IEnumerator_get_Current_m2B6EE2073CADA240627B5A1419ED1DE249E657E8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__24_t73B664F1768B6C6781512427D4D8E87F350AA835_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__24_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mF97261A04403E4BFC8518DB4F85163C057934C26(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__24_t73B664F1768B6C6781512427D4D8E87F350AA835_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__24_System_Collections_IEnumerable_GetEnumerator_m29A61F8B9157EB14CF35EFC4495AF8D7CBDB7C39(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void JSONObject_t401528CAA28B4B8AAA03BBD681A13864E62CE865_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void JSONObject_t401528CAA28B4B8AAA03BBD681A13864E62CE865_CustomAttributesCacheGenerator_JSONObject_get_Children_mDC37B11CC04E072B9E1DFE56A3B176A1E22F8E6D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3Cget_ChildrenU3Ed__27_t39D6D21A5227AF625B1601F0038D844C23D789BC_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3Cget_ChildrenU3Ed__27_t39D6D21A5227AF625B1601F0038D844C23D789BC_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass21_0_t373BF0D6FE8FC7A0C471A9DE43255479FD1A6F0C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__27_t39D6D21A5227AF625B1601F0038D844C23D789BC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__27_t39D6D21A5227AF625B1601F0038D844C23D789BC_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__27__ctor_m8B8957E246F78B1184815B8C085890B41340711D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__27_t39D6D21A5227AF625B1601F0038D844C23D789BC_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__27_System_IDisposable_Dispose_m9FF83D38681D2D54D43AC8A1B50B1A3C8CBFC2FD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__27_t39D6D21A5227AF625B1601F0038D844C23D789BC_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__27_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mC4ACE0C684380E7DDC8D7EDE0D76749E2B2EBA5F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__27_t39D6D21A5227AF625B1601F0038D844C23D789BC_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__27_System_Collections_IEnumerator_Reset_m8F4944630D5FBA4286ED06697CD6932FB73C76EC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__27_t39D6D21A5227AF625B1601F0038D844C23D789BC_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__27_System_Collections_IEnumerator_get_Current_mECECB942275EF47371EA737AC48C607909027DA3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__27_t39D6D21A5227AF625B1601F0038D844C23D789BC_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__27_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mBA4BBCF2F261D002F57438B8F03BDF263810D942(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__27_t39D6D21A5227AF625B1601F0038D844C23D789BC_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__27_System_Collections_IEnumerable_GetEnumerator_m0EC44BA64BA97F3BA0655F7809FEC0EFE289C8F0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void JSONLazyCreator_t296BB4494EC996EF3B55A2F0A82DEA5E35C9D85A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[154] = 
{
	U3CPostLoginU3Ed__27_t6A3ADDE3A4F0974F15BFA86356BBE7FAAD4AC023_CustomAttributesCacheGenerator,
	U3CInitializeGameU3Ed__28_t3864133EE3A1AB00700344DB45586C9589FE8B4D_CustomAttributesCacheGenerator,
	U3CPostRegisterU3Ed__29_tF6B2D4FEE90A4B1B6D060B83633526CFA58BD9B6_CustomAttributesCacheGenerator,
	U3CPostUpdatePasswordU3Ed__30_t7DCF75414574AF890E0F9EABCD630A2447121000_CustomAttributesCacheGenerator,
	U3CGetUserScoreU3Ed__12_t1AD429FCDB69D638CEC571707DA1437C42386A42_CustomAttributesCacheGenerator,
	U3CPostUserScoreU3Ed__13_t1350BA35CEFFD7F394F524484677BDAB1E6C7C9C_CustomAttributesCacheGenerator,
	U3CRefreshLeaderboardU3Ed__14_tBBA14020E901DACD81905E997A91E48B1EB99043_CustomAttributesCacheGenerator,
	U3CGetLeaderboardScoreU3Ed__15_tF0279DBD48BBB5A3CF3999D94F644F10D3E108B9_CustomAttributesCacheGenerator,
	U3CStopJumpTimerU3Ed__74_t2769B2E0AC0323FFEF1FD5744C16C240C26670D0_CustomAttributesCacheGenerator,
	U3CResetJumpTimerU3Ed__75_tD3DA8E1652DAD9E4C83045BF2AA5F5B2F26DB94E_CustomAttributesCacheGenerator,
	U3CEnableGravityOverTimeU3Ed__76_tDBABDCD94E90FACD6D355E83A8FAF6B2804CFD91_CustomAttributesCacheGenerator,
	U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_t28ED70B25F5DBD96AF5C3BA74FEC3A12D0953248_CustomAttributesCacheGenerator,
	U3COpenLeaderboardAfterTimeU3Ed__89_tC2825CC655F64FABC9CDD8EA139885CBDBF4796F_CustomAttributesCacheGenerator,
	U3CRegenerateObstacleU3Ed__19_t39EA583832813CE94425E82627389AB062684519_CustomAttributesCacheGenerator,
	U3CLoginButtonPressU3Ed__5_tF4E5EACBB34656F5D3F398A46275BCC283CC40DE_CustomAttributesCacheGenerator,
	JSONNode_tDEACFC45BBAB7B7189263CF1BA7D4B7FE179132A_CustomAttributesCacheGenerator,
	U3Cget_ChildrenU3Ed__43_tF127CA438370481A61170450D7243D4984DC0504_CustomAttributesCacheGenerator,
	U3Cget_DeepChildrenU3Ed__45_tFFAE4AD76D933309177202F51E61F95E756BBBC2_CustomAttributesCacheGenerator,
	JSONArray_t478FF5B6520D2CAB00FA50A61F808A3E09F928A0_CustomAttributesCacheGenerator,
	U3Cget_ChildrenU3Ed__24_t73B664F1768B6C6781512427D4D8E87F350AA835_CustomAttributesCacheGenerator,
	JSONObject_t401528CAA28B4B8AAA03BBD681A13864E62CE865_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass21_0_t373BF0D6FE8FC7A0C471A9DE43255479FD1A6F0C_CustomAttributesCacheGenerator,
	U3Cget_ChildrenU3Ed__27_t39D6D21A5227AF625B1601F0038D844C23D789BC_CustomAttributesCacheGenerator,
	JSONLazyCreator_t296BB4494EC996EF3B55A2F0A82DEA5E35C9D85A_CustomAttributesCacheGenerator,
	AuthenticationManagerScript_t0DC27D6231187368FA31C2F93A5D478B6B5ED4FB_CustomAttributesCacheGenerator__registerUserData,
	AuthenticationManagerScript_t0DC27D6231187368FA31C2F93A5D478B6B5ED4FB_CustomAttributesCacheGenerator__loginUserData,
	AuthenticationManagerScript_t0DC27D6231187368FA31C2F93A5D478B6B5ED4FB_CustomAttributesCacheGenerator__updatePasswordData,
	LeaderboardManagerScript_t622C79C46163EB074DE56D5FDCA50C39B3C7BB10_CustomAttributesCacheGenerator__scoreData,
	JSONNode_tDEACFC45BBAB7B7189263CF1BA7D4B7FE179132A_CustomAttributesCacheGenerator_m_EscapeBuilder,
	AuthenticationManagerScript_t0DC27D6231187368FA31C2F93A5D478B6B5ED4FB_CustomAttributesCacheGenerator_AuthenticationManagerScript_PostLogin_mEA9EBA2DB1BC45CD94E4998FBE8499C8D62D5ED3,
	AuthenticationManagerScript_t0DC27D6231187368FA31C2F93A5D478B6B5ED4FB_CustomAttributesCacheGenerator_AuthenticationManagerScript_InitializeGame_m0680BC7A2C483C7E51CE0CC72EAD2C7C04DEF908,
	AuthenticationManagerScript_t0DC27D6231187368FA31C2F93A5D478B6B5ED4FB_CustomAttributesCacheGenerator_AuthenticationManagerScript_PostRegister_m87C6C4318FB74E2BC0464AAD636B548C9C5979CB,
	AuthenticationManagerScript_t0DC27D6231187368FA31C2F93A5D478B6B5ED4FB_CustomAttributesCacheGenerator_AuthenticationManagerScript_PostUpdatePassword_mDE1B1EE42E88A7BA7B5307D35BAC7F95F35458F6,
	U3CPostLoginU3Ed__27_t6A3ADDE3A4F0974F15BFA86356BBE7FAAD4AC023_CustomAttributesCacheGenerator_U3CPostLoginU3Ed__27__ctor_mEB1BEFE21BAA925B96785C30234697D008F64011,
	U3CPostLoginU3Ed__27_t6A3ADDE3A4F0974F15BFA86356BBE7FAAD4AC023_CustomAttributesCacheGenerator_U3CPostLoginU3Ed__27_System_IDisposable_Dispose_m2FE2383282696EDB935EEDDBD9834C5AFF8A3DAE,
	U3CPostLoginU3Ed__27_t6A3ADDE3A4F0974F15BFA86356BBE7FAAD4AC023_CustomAttributesCacheGenerator_U3CPostLoginU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m58E7651E66624F084279ABF17D4A246276428CFE,
	U3CPostLoginU3Ed__27_t6A3ADDE3A4F0974F15BFA86356BBE7FAAD4AC023_CustomAttributesCacheGenerator_U3CPostLoginU3Ed__27_System_Collections_IEnumerator_Reset_mADA6D4E611B36BAA2BA3CF7AFA0C6F9B1BAB0B7F,
	U3CPostLoginU3Ed__27_t6A3ADDE3A4F0974F15BFA86356BBE7FAAD4AC023_CustomAttributesCacheGenerator_U3CPostLoginU3Ed__27_System_Collections_IEnumerator_get_Current_m29813583295C7B6D170E9FCFA005D06633BD3F50,
	U3CInitializeGameU3Ed__28_t3864133EE3A1AB00700344DB45586C9589FE8B4D_CustomAttributesCacheGenerator_U3CInitializeGameU3Ed__28__ctor_m6483DF96F3B8D5010E57BACE7379CD97F6552A8D,
	U3CInitializeGameU3Ed__28_t3864133EE3A1AB00700344DB45586C9589FE8B4D_CustomAttributesCacheGenerator_U3CInitializeGameU3Ed__28_System_IDisposable_Dispose_m66B0054BDB30119B1B18604BDCB93C623E19D178,
	U3CInitializeGameU3Ed__28_t3864133EE3A1AB00700344DB45586C9589FE8B4D_CustomAttributesCacheGenerator_U3CInitializeGameU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7BEC21D79B0B1519085C033FBBE4D4AF59E5B284,
	U3CInitializeGameU3Ed__28_t3864133EE3A1AB00700344DB45586C9589FE8B4D_CustomAttributesCacheGenerator_U3CInitializeGameU3Ed__28_System_Collections_IEnumerator_Reset_m6C14396FDB21283CF4BF45B8EC4EF72848F056D8,
	U3CInitializeGameU3Ed__28_t3864133EE3A1AB00700344DB45586C9589FE8B4D_CustomAttributesCacheGenerator_U3CInitializeGameU3Ed__28_System_Collections_IEnumerator_get_Current_m9FEE2D712034174CD69CE35841120F2210301FED,
	U3CPostRegisterU3Ed__29_tF6B2D4FEE90A4B1B6D060B83633526CFA58BD9B6_CustomAttributesCacheGenerator_U3CPostRegisterU3Ed__29__ctor_m931F4CA283C7F730F020CB30ADAEB0CD2ADD0F77,
	U3CPostRegisterU3Ed__29_tF6B2D4FEE90A4B1B6D060B83633526CFA58BD9B6_CustomAttributesCacheGenerator_U3CPostRegisterU3Ed__29_System_IDisposable_Dispose_mB44158C17BA7B6C83846257E355EAAF2E6454C88,
	U3CPostRegisterU3Ed__29_tF6B2D4FEE90A4B1B6D060B83633526CFA58BD9B6_CustomAttributesCacheGenerator_U3CPostRegisterU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6449593DFCD7207A0D1D366835D70272ED696DCE,
	U3CPostRegisterU3Ed__29_tF6B2D4FEE90A4B1B6D060B83633526CFA58BD9B6_CustomAttributesCacheGenerator_U3CPostRegisterU3Ed__29_System_Collections_IEnumerator_Reset_m25DC991E98C158D77FB873D7478F34797AF3B253,
	U3CPostRegisterU3Ed__29_tF6B2D4FEE90A4B1B6D060B83633526CFA58BD9B6_CustomAttributesCacheGenerator_U3CPostRegisterU3Ed__29_System_Collections_IEnumerator_get_Current_mCB8DBA443AC25037298A19CBB31BE910ED38F63A,
	U3CPostUpdatePasswordU3Ed__30_t7DCF75414574AF890E0F9EABCD630A2447121000_CustomAttributesCacheGenerator_U3CPostUpdatePasswordU3Ed__30__ctor_m909F6D547362802555788CF69D5907C4CA4BA188,
	U3CPostUpdatePasswordU3Ed__30_t7DCF75414574AF890E0F9EABCD630A2447121000_CustomAttributesCacheGenerator_U3CPostUpdatePasswordU3Ed__30_System_IDisposable_Dispose_m89831D42936AAF1DB69688E54ED481E96C1D0844,
	U3CPostUpdatePasswordU3Ed__30_t7DCF75414574AF890E0F9EABCD630A2447121000_CustomAttributesCacheGenerator_U3CPostUpdatePasswordU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0CB3D36D4861D2E2547B0732E6E75A49BF0D9518,
	U3CPostUpdatePasswordU3Ed__30_t7DCF75414574AF890E0F9EABCD630A2447121000_CustomAttributesCacheGenerator_U3CPostUpdatePasswordU3Ed__30_System_Collections_IEnumerator_Reset_m39D8102580AB48A2D53BEE3662B43B7035864C14,
	U3CPostUpdatePasswordU3Ed__30_t7DCF75414574AF890E0F9EABCD630A2447121000_CustomAttributesCacheGenerator_U3CPostUpdatePasswordU3Ed__30_System_Collections_IEnumerator_get_Current_m78D4C232B44854225B661FF119D83E1F104DC408,
	LeaderboardManagerScript_t622C79C46163EB074DE56D5FDCA50C39B3C7BB10_CustomAttributesCacheGenerator_LeaderboardManagerScript_GetUserScore_m8756E65C0A74F98E324B876FB5EB196A6ADC795A,
	LeaderboardManagerScript_t622C79C46163EB074DE56D5FDCA50C39B3C7BB10_CustomAttributesCacheGenerator_LeaderboardManagerScript_PostUserScore_m2A579351E7CC3CFE5D7C2F2776852C8CC1AC9447,
	LeaderboardManagerScript_t622C79C46163EB074DE56D5FDCA50C39B3C7BB10_CustomAttributesCacheGenerator_LeaderboardManagerScript_RefreshLeaderboard_m77306CDAE263AAD96AE6C3DCE3CD073A1171C1AE,
	LeaderboardManagerScript_t622C79C46163EB074DE56D5FDCA50C39B3C7BB10_CustomAttributesCacheGenerator_LeaderboardManagerScript_GetLeaderboardScore_mAB14D2288DED2975614F0E520031FC65E44DFF6A,
	U3CGetUserScoreU3Ed__12_t1AD429FCDB69D638CEC571707DA1437C42386A42_CustomAttributesCacheGenerator_U3CGetUserScoreU3Ed__12__ctor_m6DF1C76F95FD6F8AF39A06037BED6343E305C2FA,
	U3CGetUserScoreU3Ed__12_t1AD429FCDB69D638CEC571707DA1437C42386A42_CustomAttributesCacheGenerator_U3CGetUserScoreU3Ed__12_System_IDisposable_Dispose_m0B2AEC18400147F12B383DAF905A66A3B8D3DC2A,
	U3CGetUserScoreU3Ed__12_t1AD429FCDB69D638CEC571707DA1437C42386A42_CustomAttributesCacheGenerator_U3CGetUserScoreU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1609C0434FBD0F2540E29692CA5145F44797CC82,
	U3CGetUserScoreU3Ed__12_t1AD429FCDB69D638CEC571707DA1437C42386A42_CustomAttributesCacheGenerator_U3CGetUserScoreU3Ed__12_System_Collections_IEnumerator_Reset_m2B00C50A75F503B3E3765CAF36F43001ADF88352,
	U3CGetUserScoreU3Ed__12_t1AD429FCDB69D638CEC571707DA1437C42386A42_CustomAttributesCacheGenerator_U3CGetUserScoreU3Ed__12_System_Collections_IEnumerator_get_Current_m6BBD7CCC844BAD6D354B395F87EF37D6263C3244,
	U3CPostUserScoreU3Ed__13_t1350BA35CEFFD7F394F524484677BDAB1E6C7C9C_CustomAttributesCacheGenerator_U3CPostUserScoreU3Ed__13__ctor_m42C6A446F6AB7D73A5DA80DA9AAA9C08741DD4E4,
	U3CPostUserScoreU3Ed__13_t1350BA35CEFFD7F394F524484677BDAB1E6C7C9C_CustomAttributesCacheGenerator_U3CPostUserScoreU3Ed__13_System_IDisposable_Dispose_m6791F873B65EA43765AD1F4B6DE6C2F442AF4A75,
	U3CPostUserScoreU3Ed__13_t1350BA35CEFFD7F394F524484677BDAB1E6C7C9C_CustomAttributesCacheGenerator_U3CPostUserScoreU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF02B4C4EF83A6A968D93F20EFF5C4D49BDE7C856,
	U3CPostUserScoreU3Ed__13_t1350BA35CEFFD7F394F524484677BDAB1E6C7C9C_CustomAttributesCacheGenerator_U3CPostUserScoreU3Ed__13_System_Collections_IEnumerator_Reset_m8C33BD5DDCF2DFB8C0F1322DE16D5BACE21434F1,
	U3CPostUserScoreU3Ed__13_t1350BA35CEFFD7F394F524484677BDAB1E6C7C9C_CustomAttributesCacheGenerator_U3CPostUserScoreU3Ed__13_System_Collections_IEnumerator_get_Current_m76FD9530EA41DA3E24A815FAFB2A546BDA2E5398,
	U3CRefreshLeaderboardU3Ed__14_tBBA14020E901DACD81905E997A91E48B1EB99043_CustomAttributesCacheGenerator_U3CRefreshLeaderboardU3Ed__14__ctor_m3BE38DCD3898B69384135BF8BF3BBECB37CF2BBB,
	U3CRefreshLeaderboardU3Ed__14_tBBA14020E901DACD81905E997A91E48B1EB99043_CustomAttributesCacheGenerator_U3CRefreshLeaderboardU3Ed__14_System_IDisposable_Dispose_m6DEB89FB0B3983C07F931D6AA157A597A74C4942,
	U3CRefreshLeaderboardU3Ed__14_tBBA14020E901DACD81905E997A91E48B1EB99043_CustomAttributesCacheGenerator_U3CRefreshLeaderboardU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1C0DA2AEA8CB3577F31398A832DD180B719B4D70,
	U3CRefreshLeaderboardU3Ed__14_tBBA14020E901DACD81905E997A91E48B1EB99043_CustomAttributesCacheGenerator_U3CRefreshLeaderboardU3Ed__14_System_Collections_IEnumerator_Reset_mE8F2E463A16B08A0DE19F4F2C5E439A6BBA3F4C8,
	U3CRefreshLeaderboardU3Ed__14_tBBA14020E901DACD81905E997A91E48B1EB99043_CustomAttributesCacheGenerator_U3CRefreshLeaderboardU3Ed__14_System_Collections_IEnumerator_get_Current_mD145646783752DD18CB8862C45B302D896861CFD,
	U3CGetLeaderboardScoreU3Ed__15_tF0279DBD48BBB5A3CF3999D94F644F10D3E108B9_CustomAttributesCacheGenerator_U3CGetLeaderboardScoreU3Ed__15__ctor_mF4EDA003AED4462A1EF6DDCE4424A4C2EF856FE6,
	U3CGetLeaderboardScoreU3Ed__15_tF0279DBD48BBB5A3CF3999D94F644F10D3E108B9_CustomAttributesCacheGenerator_U3CGetLeaderboardScoreU3Ed__15_System_IDisposable_Dispose_mA8ABD6A836C443F686F9659B756BD47433198F5C,
	U3CGetLeaderboardScoreU3Ed__15_tF0279DBD48BBB5A3CF3999D94F644F10D3E108B9_CustomAttributesCacheGenerator_U3CGetLeaderboardScoreU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m668B067B6B3BF7507722752F90FD5DEF954FFB99,
	U3CGetLeaderboardScoreU3Ed__15_tF0279DBD48BBB5A3CF3999D94F644F10D3E108B9_CustomAttributesCacheGenerator_U3CGetLeaderboardScoreU3Ed__15_System_Collections_IEnumerator_Reset_m450981936121B358A075B602BBA9DB04F71E9548,
	U3CGetLeaderboardScoreU3Ed__15_tF0279DBD48BBB5A3CF3999D94F644F10D3E108B9_CustomAttributesCacheGenerator_U3CGetLeaderboardScoreU3Ed__15_System_Collections_IEnumerator_get_Current_mF42DAE75A8866E8A9673687025E5FCFCD5ED5D1C,
	PlayerBehavior_t9DE38691D7AF23D88A278979D97CA3EA92E0EF86_CustomAttributesCacheGenerator_PlayerBehavior_StopJumpTimer_mAFE61DEDE57D69FF756BCE23E17F6434691F8638,
	PlayerBehavior_t9DE38691D7AF23D88A278979D97CA3EA92E0EF86_CustomAttributesCacheGenerator_PlayerBehavior_ResetJumpTimer_m185F6ED4236EEB216DC85888A50C2560FF914D3B,
	PlayerBehavior_t9DE38691D7AF23D88A278979D97CA3EA92E0EF86_CustomAttributesCacheGenerator_PlayerBehavior_EnableGravityOverTime_mD4996D3AB212898BB086648619213689676BDA50,
	PlayerBehavior_t9DE38691D7AF23D88A278979D97CA3EA92E0EF86_CustomAttributesCacheGenerator_PlayerBehavior_AbleToProceedLeaderboardAfterTime_mA4AA9F5151C07EF9F99AFE27CE2F590CF45FDBB5,
	PlayerBehavior_t9DE38691D7AF23D88A278979D97CA3EA92E0EF86_CustomAttributesCacheGenerator_PlayerBehavior_OpenLeaderboardAfterTime_mB2932C80FDD5D9E1299064A2E57DBCEE848E69C7,
	U3CStopJumpTimerU3Ed__74_t2769B2E0AC0323FFEF1FD5744C16C240C26670D0_CustomAttributesCacheGenerator_U3CStopJumpTimerU3Ed__74__ctor_m41E2698C1D7FFA19FB5ED48CDE29E7AAD7CD28B8,
	U3CStopJumpTimerU3Ed__74_t2769B2E0AC0323FFEF1FD5744C16C240C26670D0_CustomAttributesCacheGenerator_U3CStopJumpTimerU3Ed__74_System_IDisposable_Dispose_m67AD08EC1B8C83720DAE14A4F7B934CF625176CC,
	U3CStopJumpTimerU3Ed__74_t2769B2E0AC0323FFEF1FD5744C16C240C26670D0_CustomAttributesCacheGenerator_U3CStopJumpTimerU3Ed__74_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m081E92295C4CEC6F83563A877AB1DCAF46DC8DA4,
	U3CStopJumpTimerU3Ed__74_t2769B2E0AC0323FFEF1FD5744C16C240C26670D0_CustomAttributesCacheGenerator_U3CStopJumpTimerU3Ed__74_System_Collections_IEnumerator_Reset_mD055DB840465665B871A22F89D90E605D7A97096,
	U3CStopJumpTimerU3Ed__74_t2769B2E0AC0323FFEF1FD5744C16C240C26670D0_CustomAttributesCacheGenerator_U3CStopJumpTimerU3Ed__74_System_Collections_IEnumerator_get_Current_mD16CC96DB783F8037E654D94D49E6708C51EC914,
	U3CResetJumpTimerU3Ed__75_tD3DA8E1652DAD9E4C83045BF2AA5F5B2F26DB94E_CustomAttributesCacheGenerator_U3CResetJumpTimerU3Ed__75__ctor_mA27CAFE95460E965FDCCC3A1813716299237C8E4,
	U3CResetJumpTimerU3Ed__75_tD3DA8E1652DAD9E4C83045BF2AA5F5B2F26DB94E_CustomAttributesCacheGenerator_U3CResetJumpTimerU3Ed__75_System_IDisposable_Dispose_mD84D349A6DF9DD4BD173257B9AB9BAA218C0ADB4,
	U3CResetJumpTimerU3Ed__75_tD3DA8E1652DAD9E4C83045BF2AA5F5B2F26DB94E_CustomAttributesCacheGenerator_U3CResetJumpTimerU3Ed__75_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7ECDA3855FBBAACCEE6F700AE41B3CDD2EDC486B,
	U3CResetJumpTimerU3Ed__75_tD3DA8E1652DAD9E4C83045BF2AA5F5B2F26DB94E_CustomAttributesCacheGenerator_U3CResetJumpTimerU3Ed__75_System_Collections_IEnumerator_Reset_m9061B620684FA7AA104DB6F03EF4F9EC3096D45F,
	U3CResetJumpTimerU3Ed__75_tD3DA8E1652DAD9E4C83045BF2AA5F5B2F26DB94E_CustomAttributesCacheGenerator_U3CResetJumpTimerU3Ed__75_System_Collections_IEnumerator_get_Current_m978E4CC3F36731396CE35B9C179AE59FD37DC63E,
	U3CEnableGravityOverTimeU3Ed__76_tDBABDCD94E90FACD6D355E83A8FAF6B2804CFD91_CustomAttributesCacheGenerator_U3CEnableGravityOverTimeU3Ed__76__ctor_m2B9DADB5BF8B2D0536C2D164F93FA9867BA796EA,
	U3CEnableGravityOverTimeU3Ed__76_tDBABDCD94E90FACD6D355E83A8FAF6B2804CFD91_CustomAttributesCacheGenerator_U3CEnableGravityOverTimeU3Ed__76_System_IDisposable_Dispose_mA5FDF3F865DA8C4E0905CE6C6025BE69D4070354,
	U3CEnableGravityOverTimeU3Ed__76_tDBABDCD94E90FACD6D355E83A8FAF6B2804CFD91_CustomAttributesCacheGenerator_U3CEnableGravityOverTimeU3Ed__76_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m92C0C9DB67D90D2F2C749287610A1B6226FA542C,
	U3CEnableGravityOverTimeU3Ed__76_tDBABDCD94E90FACD6D355E83A8FAF6B2804CFD91_CustomAttributesCacheGenerator_U3CEnableGravityOverTimeU3Ed__76_System_Collections_IEnumerator_Reset_m3932804EE7C65C73D7E8CFC1826D47DCA8C837C2,
	U3CEnableGravityOverTimeU3Ed__76_tDBABDCD94E90FACD6D355E83A8FAF6B2804CFD91_CustomAttributesCacheGenerator_U3CEnableGravityOverTimeU3Ed__76_System_Collections_IEnumerator_get_Current_mAEBADC72DF8EEEFCF94DDE16A776F7D5D8D88794,
	U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_t28ED70B25F5DBD96AF5C3BA74FEC3A12D0953248_CustomAttributesCacheGenerator_U3CAbleToProceedLeaderboardAfterTimeU3Ed__88__ctor_m40C062923AB126A9E0D45BB3AAA3F17D50DB2530,
	U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_t28ED70B25F5DBD96AF5C3BA74FEC3A12D0953248_CustomAttributesCacheGenerator_U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_System_IDisposable_Dispose_m373AEAE00494910A9D8D3D849DAA13C2B99048A9,
	U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_t28ED70B25F5DBD96AF5C3BA74FEC3A12D0953248_CustomAttributesCacheGenerator_U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1066A678444ADE3726429AD33279DF0180B722D5,
	U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_t28ED70B25F5DBD96AF5C3BA74FEC3A12D0953248_CustomAttributesCacheGenerator_U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_System_Collections_IEnumerator_Reset_m9119FA446EEE0532BC12BD62858486173A9E51CA,
	U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_t28ED70B25F5DBD96AF5C3BA74FEC3A12D0953248_CustomAttributesCacheGenerator_U3CAbleToProceedLeaderboardAfterTimeU3Ed__88_System_Collections_IEnumerator_get_Current_m5E07809987A0D316422B77E8F8755D199FA282D7,
	U3COpenLeaderboardAfterTimeU3Ed__89_tC2825CC655F64FABC9CDD8EA139885CBDBF4796F_CustomAttributesCacheGenerator_U3COpenLeaderboardAfterTimeU3Ed__89__ctor_m2A4E052679FFFC11969D70AAB464E5B78F3490A2,
	U3COpenLeaderboardAfterTimeU3Ed__89_tC2825CC655F64FABC9CDD8EA139885CBDBF4796F_CustomAttributesCacheGenerator_U3COpenLeaderboardAfterTimeU3Ed__89_System_IDisposable_Dispose_mD00875A1A0AE2962BBC8A1E3E19DDF62CBE73CAD,
	U3COpenLeaderboardAfterTimeU3Ed__89_tC2825CC655F64FABC9CDD8EA139885CBDBF4796F_CustomAttributesCacheGenerator_U3COpenLeaderboardAfterTimeU3Ed__89_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDFD555A751B8003880B730B6AFD29E8A691841F8,
	U3COpenLeaderboardAfterTimeU3Ed__89_tC2825CC655F64FABC9CDD8EA139885CBDBF4796F_CustomAttributesCacheGenerator_U3COpenLeaderboardAfterTimeU3Ed__89_System_Collections_IEnumerator_Reset_mC656EA4FBAE012267F228A786F865FA4185A3335,
	U3COpenLeaderboardAfterTimeU3Ed__89_tC2825CC655F64FABC9CDD8EA139885CBDBF4796F_CustomAttributesCacheGenerator_U3COpenLeaderboardAfterTimeU3Ed__89_System_Collections_IEnumerator_get_Current_m1C900A1EE59D97FA2DA2A48153B3CE7D14DE2D57,
	ScoreManagerScript_tC5BCE3E4E48F43B3125D1A9D6A5FD79E79A4A506_CustomAttributesCacheGenerator_ScoreManagerScript_RegenerateObstacle_m5BC87374E37350071A1C6973E47051B6F613D117,
	U3CRegenerateObstacleU3Ed__19_t39EA583832813CE94425E82627389AB062684519_CustomAttributesCacheGenerator_U3CRegenerateObstacleU3Ed__19__ctor_m60453B6244F7386B560123E985C11370CBEDECBB,
	U3CRegenerateObstacleU3Ed__19_t39EA583832813CE94425E82627389AB062684519_CustomAttributesCacheGenerator_U3CRegenerateObstacleU3Ed__19_System_IDisposable_Dispose_mB38500D93A8F554EA8E1909EC0173AE9DD00FCE0,
	U3CRegenerateObstacleU3Ed__19_t39EA583832813CE94425E82627389AB062684519_CustomAttributesCacheGenerator_U3CRegenerateObstacleU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m895A09897E7E9C09EE91BEC95D83C82A53DF79E4,
	U3CRegenerateObstacleU3Ed__19_t39EA583832813CE94425E82627389AB062684519_CustomAttributesCacheGenerator_U3CRegenerateObstacleU3Ed__19_System_Collections_IEnumerator_Reset_m7E60D59453D7C13F7010D83AC8979A2E5DC475B9,
	U3CRegenerateObstacleU3Ed__19_t39EA583832813CE94425E82627389AB062684519_CustomAttributesCacheGenerator_U3CRegenerateObstacleU3Ed__19_System_Collections_IEnumerator_get_Current_mCA250A006DE45F560B6CE478835B866C20DBE7D6,
	LoginUIScript_t030E5A2DFD6F6DC1DC5C99CCB178135E124E7F31_CustomAttributesCacheGenerator_LoginUIScript_LoginButtonPress_m0CE571C7B9164C53A9606738B7BC8FA2482CAA3E,
	U3CLoginButtonPressU3Ed__5_tF4E5EACBB34656F5D3F398A46275BCC283CC40DE_CustomAttributesCacheGenerator_U3CLoginButtonPressU3Ed__5__ctor_m935D324B8C35004111454210399F8E62DEE9F2EB,
	U3CLoginButtonPressU3Ed__5_tF4E5EACBB34656F5D3F398A46275BCC283CC40DE_CustomAttributesCacheGenerator_U3CLoginButtonPressU3Ed__5_System_IDisposable_Dispose_mBDEB7D0D4575B10290E226D82EBC8EF10CFD99F0,
	U3CLoginButtonPressU3Ed__5_tF4E5EACBB34656F5D3F398A46275BCC283CC40DE_CustomAttributesCacheGenerator_U3CLoginButtonPressU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m82C6CC51A0997A8D3CD01A602CC35F562ACEF118,
	U3CLoginButtonPressU3Ed__5_tF4E5EACBB34656F5D3F398A46275BCC283CC40DE_CustomAttributesCacheGenerator_U3CLoginButtonPressU3Ed__5_System_Collections_IEnumerator_Reset_m3024966D9BF1F0E05CE3A175381E492052284959,
	U3CLoginButtonPressU3Ed__5_tF4E5EACBB34656F5D3F398A46275BCC283CC40DE_CustomAttributesCacheGenerator_U3CLoginButtonPressU3Ed__5_System_Collections_IEnumerator_get_Current_mAB81582C564DB4C9DD1A7EEB8EA4AF8BCF1A73E0,
	RegisterUIScript_tD044E84DB9D96298A96CD5D1492DAE81B2544C39_CustomAttributesCacheGenerator_RegisterUIScript_U3CStartU3Eb__11_0_mA65A23DD36E49ED96B0962932AC2A81EDC81693B,
	UpdatePasswordUIScript_t8CA6FD51A09849C85599830D10EADB5892E51363_CustomAttributesCacheGenerator_UpdatePasswordUIScript_U3CStartU3Eb__6_0_m29B697067F37BA21498266921EF007CB013D8DF3,
	JSONNode_tDEACFC45BBAB7B7189263CF1BA7D4B7FE179132A_CustomAttributesCacheGenerator_JSONNode_get_Children_mB9A68101166B1C5176311D2EC1ABCC832248FDFF,
	JSONNode_tDEACFC45BBAB7B7189263CF1BA7D4B7FE179132A_CustomAttributesCacheGenerator_JSONNode_get_DeepChildren_m3E3CE542A4856D0BD557B7E121B4CAA999C8110C,
	U3Cget_ChildrenU3Ed__43_tF127CA438370481A61170450D7243D4984DC0504_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__43__ctor_m1E3DEE85215E302020E599B5D498D5F257B841E5,
	U3Cget_ChildrenU3Ed__43_tF127CA438370481A61170450D7243D4984DC0504_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__43_System_IDisposable_Dispose_m3F00470D6E61DBE9CF64CD523D38F5092505403A,
	U3Cget_ChildrenU3Ed__43_tF127CA438370481A61170450D7243D4984DC0504_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__43_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m2E015F7A52E8208F5DBDD306CA59D6DDA7C5E14B,
	U3Cget_ChildrenU3Ed__43_tF127CA438370481A61170450D7243D4984DC0504_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__43_System_Collections_IEnumerator_Reset_m651CEB1CC2F1607A9BC74E77C5B74A593E064F08,
	U3Cget_ChildrenU3Ed__43_tF127CA438370481A61170450D7243D4984DC0504_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__43_System_Collections_IEnumerator_get_Current_m10EB5342637BBF2E59C8EC9A660D591135EB6ACA,
	U3Cget_ChildrenU3Ed__43_tF127CA438370481A61170450D7243D4984DC0504_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__43_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m3C96B2C39F4888749B03B276C3525C5F6F65F689,
	U3Cget_ChildrenU3Ed__43_tF127CA438370481A61170450D7243D4984DC0504_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__43_System_Collections_IEnumerable_GetEnumerator_m348BC5D347B12C3670753E9B49E7A85F98FBD028,
	U3Cget_DeepChildrenU3Ed__45_tFFAE4AD76D933309177202F51E61F95E756BBBC2_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__45__ctor_mAEF820B12BA0E5ACD7AEAF398E13D86229F33824,
	U3Cget_DeepChildrenU3Ed__45_tFFAE4AD76D933309177202F51E61F95E756BBBC2_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__45_System_IDisposable_Dispose_m402090148B49E68DBBB71155A79B32A9D5EC0B28,
	U3Cget_DeepChildrenU3Ed__45_tFFAE4AD76D933309177202F51E61F95E756BBBC2_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__45_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m6F553695AE45AFE3DB3CB3CB041EBA0C989BD418,
	U3Cget_DeepChildrenU3Ed__45_tFFAE4AD76D933309177202F51E61F95E756BBBC2_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__45_System_Collections_IEnumerator_Reset_m0A483B8285BDDCD8AAE7A06B530E0B8AD49AFBB7,
	U3Cget_DeepChildrenU3Ed__45_tFFAE4AD76D933309177202F51E61F95E756BBBC2_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__45_System_Collections_IEnumerator_get_Current_m3405A7516644ED2988C02CD58955E86EF6F4A9C0,
	U3Cget_DeepChildrenU3Ed__45_tFFAE4AD76D933309177202F51E61F95E756BBBC2_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__45_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m8EC445D3F9C890F98F5EFE538F031CEFD61B518A,
	U3Cget_DeepChildrenU3Ed__45_tFFAE4AD76D933309177202F51E61F95E756BBBC2_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__45_System_Collections_IEnumerable_GetEnumerator_mDD5EC1B27E01615A2CC852679694910EF826BE7A,
	JSONArray_t478FF5B6520D2CAB00FA50A61F808A3E09F928A0_CustomAttributesCacheGenerator_JSONArray_get_Children_m56D8A696E09F5EEFF1FF9F8BE06011CE5EAFA197,
	U3Cget_ChildrenU3Ed__24_t73B664F1768B6C6781512427D4D8E87F350AA835_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__24__ctor_mC6643FC9306ABFE30E20A00C08D9738D533DB279,
	U3Cget_ChildrenU3Ed__24_t73B664F1768B6C6781512427D4D8E87F350AA835_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__24_System_IDisposable_Dispose_mDDD5192FEDBCC636E7301392862E8CC8315F155E,
	U3Cget_ChildrenU3Ed__24_t73B664F1768B6C6781512427D4D8E87F350AA835_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__24_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m6143E2C2D943E8398F986B0FD76F13B995D2BE15,
	U3Cget_ChildrenU3Ed__24_t73B664F1768B6C6781512427D4D8E87F350AA835_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__24_System_Collections_IEnumerator_Reset_m838D58EEF0F67271515CB056BAA7E7467754D04A,
	U3Cget_ChildrenU3Ed__24_t73B664F1768B6C6781512427D4D8E87F350AA835_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__24_System_Collections_IEnumerator_get_Current_m2B6EE2073CADA240627B5A1419ED1DE249E657E8,
	U3Cget_ChildrenU3Ed__24_t73B664F1768B6C6781512427D4D8E87F350AA835_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__24_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mF97261A04403E4BFC8518DB4F85163C057934C26,
	U3Cget_ChildrenU3Ed__24_t73B664F1768B6C6781512427D4D8E87F350AA835_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__24_System_Collections_IEnumerable_GetEnumerator_m29A61F8B9157EB14CF35EFC4495AF8D7CBDB7C39,
	JSONObject_t401528CAA28B4B8AAA03BBD681A13864E62CE865_CustomAttributesCacheGenerator_JSONObject_get_Children_mDC37B11CC04E072B9E1DFE56A3B176A1E22F8E6D,
	U3Cget_ChildrenU3Ed__27_t39D6D21A5227AF625B1601F0038D844C23D789BC_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__27__ctor_m8B8957E246F78B1184815B8C085890B41340711D,
	U3Cget_ChildrenU3Ed__27_t39D6D21A5227AF625B1601F0038D844C23D789BC_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__27_System_IDisposable_Dispose_m9FF83D38681D2D54D43AC8A1B50B1A3C8CBFC2FD,
	U3Cget_ChildrenU3Ed__27_t39D6D21A5227AF625B1601F0038D844C23D789BC_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__27_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mC4ACE0C684380E7DDC8D7EDE0D76749E2B2EBA5F,
	U3Cget_ChildrenU3Ed__27_t39D6D21A5227AF625B1601F0038D844C23D789BC_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__27_System_Collections_IEnumerator_Reset_m8F4944630D5FBA4286ED06697CD6932FB73C76EC,
	U3Cget_ChildrenU3Ed__27_t39D6D21A5227AF625B1601F0038D844C23D789BC_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__27_System_Collections_IEnumerator_get_Current_mECECB942275EF47371EA737AC48C607909027DA3,
	U3Cget_ChildrenU3Ed__27_t39D6D21A5227AF625B1601F0038D844C23D789BC_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__27_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mBA4BBCF2F261D002F57438B8F03BDF263810D942,
	U3Cget_ChildrenU3Ed__27_t39D6D21A5227AF625B1601F0038D844C23D789BC_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__27_System_Collections_IEnumerable_GetEnumerator_m0EC44BA64BA97F3BA0655F7809FEC0EFE289C8F0,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
